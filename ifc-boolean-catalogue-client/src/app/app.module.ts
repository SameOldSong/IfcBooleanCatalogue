import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import {
  MatIconModule, MatButtonModule, MatCardModule, MatFormFieldModule, MatGridListModule, MatInputModule, MatListModule, MatSelectModule, MatSidenavModule, MatTableModule, 
  MatToolbarModule
} from '@angular/material';

import { HttpClientModule } from '@angular/common/http';
import { FileSelectDirective, FileDropDirective } from 'ng2-file-upload';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ReactiveFormsModule } from '@angular/forms';


import { BooleanToIfcComponent } from './boolean-to-ifc/boolean-to-ifc.component';
import { IfcToBooleanComponent } from './ifc-to-boolean/ifc-to-boolean.component';
import { BooleanToCatalogueComponent } from './boolean-to-catalogue/boolean-to-catalogue.component';
import { IfcToCatalogueComponent } from './ifc-to-catalogue/ifc-to-catalogue.component';
import { HeaderComponent } from './header/header.component';
import { TestserviceComponent } from './testservice/testservice.component';
import { CatalogueGenerationComponent } from './catalogue-generation/catalogue-generation.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { CatalogueSearchComponent } from './catalogue-search/catalogue-search.component'; 


@NgModule({
  declarations: [
    AppComponent,
    BooleanToIfcComponent,
    BooleanToCatalogueComponent,
    FileSelectDirective,
    FileDropDirective,
    HeaderComponent,
    IfcToBooleanComponent,
    IfcToCatalogueComponent,
    TestserviceComponent,
    CatalogueGenerationComponent,
    CatalogueSearchComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    FlexLayoutModule,
    HttpClientModule,
    MatButtonModule, MatCardModule, MatGridListModule ,  MatIconModule, MatPaginatorModule, MatSidenavModule, MatToolbarModule, MatTableModule, MatSelectModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
