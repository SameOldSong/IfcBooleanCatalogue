import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IfcToBooleanComponent } from './ifc-to-boolean.component';

describe('IfcToBooleanComponent', () => {
  let component: IfcToBooleanComponent;
  let fixture: ComponentFixture<IfcToBooleanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IfcToBooleanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IfcToBooleanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
