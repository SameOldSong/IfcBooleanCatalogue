import { Component, OnInit, ViewChild } from '@angular/core';
import { Property } from '../model/property';
import { HttpClient, HttpEvent, HttpEventType, HttpHeaders, HttpRequest, HttpResponse } from "@angular/common/http";
import { Value } from '../model/value';
import { Dependency } from '../model/dependency';
import { DirectiveOwnerAndPlayerBuilderIndex } from '@angular/core/src/render3/interfaces/styling';

@Component({
  selector: 'app-catalogue-generation',
  templateUrl: './catalogue-generation.component.html',
  styleUrls: ['./catalogue-generation.component.css']
})
export class CatalogueGenerationComponent implements OnInit {
  showCatalogueTable: boolean;
  properties: Array<Property> = [];
  products: Array<string> = [];
  
  constructor(private http: HttpClient) { }
  ngOnInit() {
  }

  /// DOWNLOAD COMPLETE CATALOGUE FROM SERVER
  showCatalogue() {


    this.getCatalogueDataFromServer().subscribe(res => {
      let response = res.body;
      var json = JSON.parse(JSON.stringify(response));
      console.log("RECEIVED ");
      console.log(JSON.stringify(response));
      for (var product of Object.keys(json)) {
        this.products.push(product);
        var listOfProperties = json[product];

        for (var propertyObject of listOfProperties) {
          var property = new Property(propertyObject['name'], product, propertyObject['unitName']);
          for (var valueObject of propertyObject['values']) {
            var dependencies = this.initDependencies(valueObject);
            var propertyValue = new Value(property, valueObject['text'], this.initDependencies(valueObject));
            property.addValue(propertyValue);
            property.setIndex(Number(valueObject['indexInArtNum']))
          }
          console.log("INDEX " + property.index);
          property.initSelectableValues();
          this.properties.push(property);
        }

      }
      this.properties.sort(function(a : Property, b: Property) {
        return a.index - b.index;
      });
      this.showCatalogueTable = true;
    });
  }

   
  initDependencies(valueObject: any): Dependency[] {
    var dependencies = new Array<Dependency>();

    for (var dependencyObject of valueObject['dependencies']) {

      //initialize trigger of Dependency
      var trigger = new Map<string, string>();
      var propertyName = Object.keys(dependencyObject['trigger'])[0];
      trigger.set(propertyName, dependencyObject['trigger'][propertyName]);

      //initialize dependents of Dependency
      var dependents = new Map<string, string[]>();
      var dependentObject = dependencyObject['dependents'];

      for (var dependentKey of Object.keys(dependentObject)) {
        var dependentValues = new Array<string>();
        for (var dependentValue of dependentObject[dependentKey])
          dependentValues.push(dependentValue);
        dependents.set(dependentKey, dependentValues);
      }

      //add new Dependency
      dependencies.push(new Dependency(trigger, dependents));
    }

    return dependencies;
  }

  getDependencies(dependencies: Array<Dependency>) {
    var depObjects = new Array<Dependency>();

    for (var dependency of dependencies) {
      var trigger = dependency['trigger'];
      var dependents = dependency['dependents'];

    }

    return depObjects;



  }

  getCatalogueDataFromServer() {
    let headers = new HttpHeaders();
    headers = headers.append('Accept', 'application/json; charset=utf-8');

    return this.http.get('/booleanCatalogue', {
      headers: headers,
      observe: 'response',
      responseType: 'json'
    });
  }
}
