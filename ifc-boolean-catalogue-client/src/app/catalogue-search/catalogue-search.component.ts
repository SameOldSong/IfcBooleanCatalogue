import { Component, OnInit, Input, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { Property } from '../model/property'
import { Dependency } from '../model/dependency'
import { MatPaginator } from '@angular/material/paginator';
import { Value } from '../model/value';
import { TableRow } from '../model/table-row';
import { HttpClient, HttpEvent, HttpEventType, HttpHeaders, HttpRequest, HttpResponse, HttpParams } from "@angular/common/http";
import { BehaviorSubject } from 'rxjs';

@Component({
	selector: 'app-catalogue-search',
	templateUrl: './catalogue-search.component.html',
	styleUrls: ['./catalogue-search.component.css']
})
export class CatalogueSearchComponent implements OnInit {

	firstSearch: boolean = true;
	showSearchResults: boolean;

	@Input() properties: Array<Property>;
	products: Array<string>;
	displayedColumns: string[] = [];

	dataSource;

	tableRows: Array<TableRow>;
	selectedValues: Map<string, string> = new Map();
	@ViewChild(MatPaginator) paginator: MatPaginator;

	constructor(private http: HttpClient, private changeDetectorRefs: ChangeDetectorRef) { }

	ngOnInit() {
		this.tableRows = new Array<TableRow>();
		this.products = new Array<string>();
		this.properties.forEach((property: Property) => {
			if (!this.products.includes(property.product))
				this.products.push(property.product);
		})

	}

	/**************SEARCHING ***************/

	search() {
		this.parseSearchResponseFromServer();
	}
	parseSearchResponseFromServer() {
		var newRows = new Array<TableRow>();
		this.getSearchResultFromServer().subscribe(response => {

			var json = JSON.parse(JSON.stringify(response));
			console.log("RECEIVED ");
			console.log(JSON.stringify(response));
			var listOfTableRowIndexes = Object.keys(json);

			//collect row objects
			for (var index of listOfTableRowIndexes) {
				console.log("indexes");
				var tableRowObject = json[index];
				var tableRow = new TableRow();
				for (var property of Object.keys(tableRowObject)) {
					console.log(tableRowObject[property]);
					tableRow.addColumnValue(property, tableRowObject[property]);
				}
				newRows.push(tableRow);
			}

			//column names
			this.displayedColumns = new Array<string>();

			var firstRow = json[0];
			for (var propertyName of Object.keys(tableRowObject)) {
				var prop = this.findProperty(propertyName);
				if (prop !== undefined && prop !== null) {
					console.log("Found " + prop.name)
					this.displayedColumns.splice(prop.index, 0, propertyName);
				} else {
					console.log("NOT Found " + propertyName)
					this.displayedColumns.splice(Object.keys.length, 0, propertyName);
				}
			}



			//rows
			this.tableRows.length = 0;
			newRows.forEach((row: TableRow) => {
				this.tableRows.push(row);
			});

			this.dataSource = new MatTableDataSource(this.tableRows);
			setTimeout(() => this.dataSource.paginator = this.paginator);
			this.showSearchResults = true;
			this.changeDetectorRefs.detectChanges();
		});
	}
	getSearchResultFromServer() {
		let headers = new HttpHeaders();
		headers = headers.append('Accept', 'application/json; charset=utf-8');
		this.printSelectedValues();
		var dataForServer = new Array<Object>();
		let selectedProduct = (<HTMLSelectElement>document.getElementById('productDropdown')).value;
		dataForServer.push(selectedProduct, Array.from(this.selectedValues.entries()));
		return this.http.post('/searchRequest', dataForServer, { headers: headers });
	}

	/**************SELECTING ***************/

	// On selecting value in dropdown
	onEditClick(selectedValue: string, propertyName: string) {
		//store previously selected value
		if (this.selectedValues.get(propertyName) !== null)
			var previousValue = this.findValue(this.findProperty(propertyName), this.selectedValues.get(propertyName));

		//find Value object for the currently selected value string
		var valueObject = this.findValue(this.findProperty(propertyName), selectedValue);
		if (selectedValue === "All ...") {
			this.selectedValues.delete(propertyName);
			if (this.nothingSelected())
				this.resetAllDropdownListsToDefault();
			else
				this.changeDependentPropertiesAfterValueDeselection(previousValue);
		}
		else {
			this.selectedValues.set(propertyName, selectedValue);
			this.changeDependentPropertiesAfterValueSelection(valueObject);
		}
		this.disableDropdownsWithNoneValue();
	}
	disableDropdownsWithNoneValue() {
		this.properties.forEach((property: Property) => {
			if (property.onlyNoneSelectable())
				property.setDisabled(true);
			else
				property.setDisabled(false);
		})
	}
	//if any value other than "All..." was selected in any dropdown
	changeDependentPropertiesAfterValueSelection(valueObject: Value) {
		valueObject.resetRemovedValues();
		var dependency = this.getDependencyMatchingAllSelectedValues();
		if (dependency !== null)
			this.setDependentDropdownLists(dependency);
		else
			this.changeDropdownListsUsingDependenciesOfSelectedValue(valueObject);
	}

	//if "All ..." was selected in any dropdown - add values back to dropdowns, 
	//i.e. the values that were previously removed as a result of selecting the now deselected value
	changeDependentPropertiesAfterValueDeselection(deselectedValue: Value) {
		if (deselectedValue !== null) {
			deselectedValue.removedValues.forEach((values: string[], propertyName: string) => {
				var propertyObject = this.findProperty(propertyName);
				propertyObject.selectableValues.push.apply(values);
			});

			deselectedValue.resetRemovedValues();
		}
	}
	//resets each dropdown to contain ALL possible values
	resetAllDropdownListsToDefault() {
		for (var property of this.properties)
			property.initSelectableValues();
	}

	//returns true if all dropdowns contain "All ..."
	nothingSelected() {
		return this.selectedValues.values.length === 0;
	}

	//returns dependency whose trigger consists of all currently selected values
	//one-to-one match!
	getDependencyMatchingAllSelectedValues(): Dependency {
		//take the first value in the selected values
		//it doesn't matter which, because we are looking for dependency matching ALL of the selected values
		var propertyWithSelectedValue = Array.from(this.selectedValues.keys())[0];
		var selectedValueObject = this.findValue(this.findProperty(propertyWithSelectedValue), this.selectedValues.get(propertyWithSelectedValue));
		for (var dependency of selectedValueObject.dependencies)
			if (this.exactlyTheseTriggersSelected(dependency))
				return dependency;
		return null;
	}

	//changes content of dropdowns according to the dependency
	setDependentDropdownLists(dependency: Dependency) {
		dependency.dependents.forEach((values: string[], key: string) => {
			var property = this.findProperty(key);
			property.setSelectableValues(values);

			//if new list of allowed dropdown values does not include the currently selected value - remove this value
			var curSelectedVal = this.selectedValues[property.name];
			if (curSelectedVal !== undefined) {
				if (!property.selectableValues.includes(curSelectedVal)) {
					this.findValue(property, curSelectedVal).resetRemovedValues();
					this.selectedValues.delete(property.name);
				}
			}
		});
	}

	//only dependencies of the Value are taken into account
	changeDropdownListsUsingDependenciesOfSelectedValue(value: Value) {
		//collect all values that are allowed according to the dependencies
		var newDependentValues = new Map<string, string[]>();
		for (var dependency of value.dependencies)
			if (this.selectedValuesIncludeDependencyTrigger(Array.from(dependency.trigger.values()))) {
				this.addToDependentValues(dependency.dependents, newDependentValues);
				console.log("ADDED to NEW DEPENDENT VALUES");
				this.printMapWithList(newDependentValues);

			} else {
				console.log("did not add to NEW DEPENDENT VALUES");
				dependency.printTriggeringValues();
			}
		//remove from each dropdown all values that are not in the "allowed collection"
		newDependentValues.forEach((values: string[], propertyName: string) => {
			console.log("Property " + propertyName);
			values.forEach((val: string) => {
				console.log(val);
			})
			var propertyObject = this.findProperty(propertyName);
			var newSelectableValues = new Array<string>();
			for (var selectableValue of propertyObject.values) {
				console.log("SELECTABLE " + selectableValue.textValue);
				if (values.includes(selectableValue.textValue) && !this.removedByOtherProperty(value.property.name, selectableValue)) {
					console.log("Added selectable value " + selectableValue.textValue);
					newSelectableValues.push(selectableValue.textValue);
					if (propertyObject.selectableValues.includes(selectableValue.textValue))
						value.addRemovedValue(selectableValue);
				}
			}
			propertyObject.setSelectableValues(newSelectableValues);
		});

	}
	printMapWithList(map: Map<string, string[]>) {
		map.forEach((value: string[], key: string) => {
			console.log("KEY " + key);
			value.forEach((val: string) => {
				console.log("VALUE: " + val);
			});
		});
	}
	//returns true if the specified value was previously removed as a result of selecting a value of another property
	//the specified property is ignored during this check
	removedByOtherProperty(propertyToIgnore: string, value: Value): boolean {
		this.selectedValues.forEach((val: string, key: string) => {
			if (key !== propertyToIgnore) {
				var valueObject = this.findValue(this.findProperty(key), val);
				if (valueObject.removedValue(value))
					return true;
			}
		});

		return false;
	}

	//returns true if currently selected values include all values in the trigger
	selectedValuesIncludeDependencyTrigger(trigger: string[]): boolean {
		for (var value of trigger)
			if (!Array.from(this.selectedValues.values()).includes(value))
				return false;
		return true;
	}
	addToDependentValues(newDependent: Map<string, string[]>, existingDependents: Map<String, String[]>) {
		for (var key of Array.from(newDependent.keys()))
			if (existingDependents.has(key)) {
				for (var value of newDependent.get(key))
					if (!existingDependents.get(key).includes(value))
						existingDependents.get(key).push(value);
			} else
				existingDependents.set(key, newDependent.get(key));
	}
	//returns true if currently selected values exactly match values in the trigger 
	exactlyTheseTriggersSelected(dependency: Dependency) {
		if (dependency.triggeringValues.length !== this.selectedValues.size)
			return false;

		for (var trigger of dependency.triggeringValues) {

			if (!Array.from(this.selectedValues.values()).includes(trigger))
				return false;
		}
		return true;
	}
	printSelectedValues() {
		this.selectedValues.forEach((value: string, key: string) => {
			console.log("In property " + key + " currently selected " + value);
		})
	}

	//returns Value object for the specified property and value name
	findValue(property: Property, value: string) {
		for (var propValue of property.values)
			if (propValue.textValue === value)
				return propValue;
		return null;
	}

	//returns Property object for the specified property name
	findProperty(propertyName: string): Property {
		for (var property of this.properties) {
			if (property.name === propertyName)
				return property;
		}
		return null;
	}

}
