import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {  FileUploader, FileSelectDirective, FileItem  } from 'ng2-file-upload/ng2-file-upload';
import {HttpClient, HttpEvent, HttpEventType, HttpHeaders, HttpRequest, HttpResponse} from "@angular/common/http";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Observable} from "rxjs";
import * as fileSaver from 'file-saver';

@Component({
  selector: 'app-boolean-to-catalogue',
  templateUrl: './boolean-to-catalogue.component.html',
  styleUrls: ['./boolean-to-catalogue.component.css']
})
export class BooleanToCatalogueComponent implements OnInit{
  uploadForm: FormGroup;
  title: string = 'Boolean IFC Catalogue';
  fileUploaded: boolean;
  currentFileUpload: File;
  fileSelected: boolean;
  progress: { percentage: number } = { percentage: 0 };
  fileUploadId;
 
  selectedFileName : string;

 

  constructor(private formBuilder: FormBuilder, private http: HttpClient ) { }
  ngOnInit() {
    this.uploadForm = this.formBuilder.group({
      document: [null, null],
      type:  [null, null]
    });
    this.uploader.onAfterAddingFile = (fileItem: FileItem) => this.onAfterAddingFile(fileItem)

  }
  
  

  ///SELECT FILE /////////////
  @ViewChild('fileInput') fileInput: ElementRef;
  fileClicked() {
    this.selectedFileName = "";
    this.fileUploaded = false;
    this.currentFileUpload = null;
    this.fileInput.nativeElement.click(); 

  }

  ngAfterViewInit() {
    this.uploader.onAfterAddingFile = (item => {
    this.fileInput.nativeElement.value = '';
    });
  }

  onAfterAddingFile(fileItem: FileItem) {
    let latestFileIndexInQueue : number = this.uploader.queue.length - 1;
    let latestFile = this.uploader.queue[latestFileIndexInQueue]
    this.uploader.queue = []; 
    this.uploader.queue.push(latestFile);
    this.fileSelected = true;
    this.selectedFileName = this.uploader.queue[latestFileIndexInQueue]._file.name;
  }

  ///// UPLOAD FILE FOR CONVERSION/////////
  public uploader:FileUploader = new FileUploader({
    isHTML5: true
  });
  
  uploadToServer(){
    //get latest selected in the queue
    let latestFileIndexInQueue : number = this.uploader.queue.length - 1;
    let fileItem = this.uploader.queue[latestFileIndexInQueue]._file;

    //uploading, observing progress and getting response
    this.currentFileUpload = this.uploader.queue[latestFileIndexInQueue]._file;
    this.progress.percentage = 0;
    this.sendFileToServer(fileItem).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress.percentage = Math.round(100 * event.loaded / event.total);
      } else if (event instanceof HttpResponse) {
        this.fileUploaded = true;
        this.fileUploadId = event.body;
      }
    }) 
  }

  sendFileToServer(file: File): Observable<HttpEvent<{}>> {
    let formdata: FormData = new FormData();
    formdata.append('file', file);
    const req = new HttpRequest('POST', '/uploadBooleanToCatalogue', formdata, {
      reportProgress: true,
      responseType: 'text'
    });

    return this.http.request(req);
  }

  

 

  
  
}
