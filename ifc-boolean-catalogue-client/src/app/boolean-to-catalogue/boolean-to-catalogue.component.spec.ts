import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BooleanToCatalogueComponent } from './boolean-to-catalogue.component';

describe('BooleanToCatalogueComponent', () => {
  let component: BooleanToCatalogueComponent;
  let fixture: ComponentFixture<BooleanToCatalogueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BooleanToCatalogueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BooleanToCatalogueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
