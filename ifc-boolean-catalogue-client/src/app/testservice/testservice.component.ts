import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
	selector: 'app-testservice',
	templateUrl: './testservice.component.html',
	styleUrls: ['./testservice.component.css']
})


export class TestserviceComponent implements OnInit {
	greeting;
	constructor(private apiService: ApiService) { }

	ngOnInit() {
		this.apiService.getGreeting().subscribe((data)=>{
			console.log(data);
			this.greeting = data;
		});
	}
}
