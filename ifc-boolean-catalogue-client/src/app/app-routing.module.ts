import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BooleanToIfcComponent } from './boolean-to-ifc/boolean-to-ifc.component';
import { BooleanToCatalogueComponent } from './boolean-to-catalogue/boolean-to-catalogue.component';
import { IfcToCatalogueComponent } from './ifc-to-catalogue/ifc-to-catalogue.component';

import { IfcToBooleanComponent } from './ifc-to-boolean/ifc-to-boolean.component'; 

import { TestserviceComponent } from './testservice/testservice.component'; 

const routes: Routes = [ { path: 'boolean-to-ifc', component: BooleanToIfcComponent },
  { path: 'ifc-to-boolean', component: IfcToBooleanComponent },
  { path: 'boolean-to-catalogue', component: BooleanToCatalogueComponent },
  { path: 'ifc-to-catalogue', component: IfcToCatalogueComponent },
  { path: '', redirectTo: '/boolean-to-ifc', pathMatch: 'full' },
   { path: 'test', component: TestserviceComponent }

  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
