import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IfcToCatalogueComponent } from './ifc-to-catalogue.component';

describe('IfcToCatalogueComponent', () => {
  let component: IfcToCatalogueComponent;
  let fixture: ComponentFixture<IfcToCatalogueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IfcToCatalogueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IfcToCatalogueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
