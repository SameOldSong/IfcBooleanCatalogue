import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {  FileUploader, FileSelectDirective, FileItem  } from 'ng2-file-upload/ng2-file-upload';
import {HttpClient, HttpEvent, HttpEventType, HttpHeaders, HttpRequest, HttpResponse} from "@angular/common/http";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Observable} from "rxjs";
import * as fileSaver from 'file-saver';

@Component({
  selector: 'app-boolean-to-ifc',
  templateUrl: './boolean-to-ifc.component.html',
  styleUrls: ['./boolean-to-ifc.component.css']
})
export class BooleanToIfcComponent implements OnInit{
  uploadForm: FormGroup;
  title: string = 'Boolean IFC Catalogue';
  fileUploaded: boolean;
  currentFileUpload: File;
  fileSelected: boolean;
  progress: { percentage: number } = { percentage: 0 };
  fileUploadId;


  constructor(private formBuilder: FormBuilder, private http: HttpClient ) { }
  ngOnInit() {
    this.uploadForm = this.formBuilder.group({
      document: [null, null],
      type:  [null, null]
    });
    this.uploader.onAfterAddingFile = (fileItem: FileItem) => this.onAfterAddingFile(fileItem)

  }
  
  

  ///SELECT FILE /////////////
  @ViewChild('fileInput') fileInput: ElementRef;
  fileClicked() {
    this.fileUploaded = false;
    this.currentFileUpload = null;
    this.uploader.clearQueue();
    this.fileInput.nativeElement.click(); 
  }

  ngAfterViewInit() {
    this.uploader.onAfterAddingFile = (item => {
      this.fileInput.nativeElement.value = '';
      console.log(this.uploader.queue.length);
      if(this.uploader.queue.length > 0)
        console.log(this.uploader.queue[this.uploader.queue.length - 1]._file.name)
    });
  }

  onAfterAddingFile(fileItem: FileItem) {
    
    let latestFile = this.uploader.queue[this.uploader.queue.length-1]
    this.uploader.queue = []; 
    this.uploader.queue.push(latestFile);
    this.fileSelected = true;
  }

  ///// UPLOAD FILE FOR CONVERSION/////////
  public uploader:FileUploader = new FileUploader({
    isHTML5: true
  });

  
  uploadToServer(){
    //get latest selected in the queue
    let latestFileIndexInQueue : number = this.uploader.queue.length - 1;
    let fileItem = this.uploader.queue[latestFileIndexInQueue]._file;

    //uploading, observing progress and getting response
    this.currentFileUpload = this.uploader.queue[latestFileIndexInQueue]._file;
    this.progress.percentage = 0;
    this.sendFileToServer(fileItem).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress.percentage = Math.round(100 * event.loaded / event.total);
      } else if (event instanceof HttpResponse) {
        this.fileUploaded = true;
        this.fileUploadId = event.body;
      }
    }) 
  }

  sendFileToServer(file: File): Observable<HttpEvent<{}>> {
    let formdata: FormData = new FormData();
    formdata.append('file', file);
    const req = new HttpRequest('POST', '/uploadBooleanToIfc', formdata, {
      reportProgress: true,
      responseType: 'text'
    });

    return this.http.request(req);
  }

  /// DOWNLOAD CONVERTED FILE FROM SERVER
  download(){
    this.downLoadFile().subscribe(response => {  
      this.saveFile(response.body, this.fileUploadId);
    });
  }

  downLoadFile() {
    let headers = new HttpHeaders();
    headers = headers.append('Accept', 'text/txt; charset=utf-8');

    return this.http.get('/ifcfiles/' + this.fileUploadId, {
      headers: headers,
      observe: 'response',
      responseType: 'text'
    });
  }

  saveFile(data: any, filename?: string) {
    const blob = new Blob([data], {type: 'text/txt; charset=utf-8'});
    fileSaver.saveAs(blob, filename);
  }

  
}
