import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BooleanToIfcComponent } from './boolean-to-ifc.component';

describe('BooleanToIfcComponent', () => {
  let component: BooleanToIfcComponent;
  let fixture: ComponentFixture<BooleanToIfcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BooleanToIfcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BooleanToIfcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
