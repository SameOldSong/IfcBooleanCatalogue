import { Value } from './value';

export class Property {
	

	name: string;
	unit: string;
	values: Value[] = [];
	selectableValues: string[] = [];
	product: string;
	index: number;
	disabled: boolean;

	constructor(propertyName: string, product: string, unit: string) {
		this.name = propertyName;
		this.product = product;
		this.unit = unit;
	}

	setIndex(index: number) {
		this.index = index;
	}

	addValue(value: Value) {
		this.values.push(value);
	}

	setSelectableValues(values: string[]) {
		this.selectableValues = new Array<string>();
		for (var value of values)
			this.selectableValues.push(value);
	}

	initSelectableValues() {
		this.selectableValues = [];
		for (var value of this.values) {
			this.selectableValues.push(value.textValue);

		}
	}

	printSelectableValues() {
		console.log("Printing property " + this.name);
		this.selectableValues.forEach((value: string) => {
			console.log(value);
		})
	}

	setDisabled(flag: boolean){
		this.disabled = flag;
	}

	onlyNoneSelectable(): boolean {
		return this.selectableValues.length === 1 && this.selectableValues.includes("NONE");
	}

}