import { stringToKeyValue } from '@angular/flex-layout/extended/typings/style/style-transforms';

export class Dependency {
	
	trigger: Map<string, string>;
	dependents: Map<string, string[]>;
	triggeringValues: Array <string>;

	constructor(trigger: Map<string, string>, dependents: Map<string, string[]>) {
		this.trigger = trigger;
		this.dependents = dependents;
		this.initTriggeringValues();
	//	this.printTrigger();
	//	this.printDependents();
	}

	initTriggeringValues(){
		this.triggeringValues = new Array<string>();
		this.trigger.forEach((value: string, key: string) =>{
			this.triggeringValues.push(value);
		});
	}

	printTrigger() {
		this.trigger.forEach((value: string, key: string) =>{
			console.log("Trigger " + key + " " + value);
		});
	}

	printTriggeringValues() {
		this.triggeringValues.forEach((value: string) =>{
			console.log("Triggering value " + value);
		});
	}

	printDependents(){
		this.dependents.forEach((value: string[], key: string) =>{
			console.log("Dependent key " + key + " " + value.forEach((val: string)=>{
				console.log("Dependent value " + val);
			}));
		});
	}


}