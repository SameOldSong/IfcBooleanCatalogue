import { Injectable } from '@angular/core';
import { Property } from './property';
import { Dependency } from './dependency';

export class Value {

	textValue: string;
	dependencies: Array<Dependency>;
	property: Property;

	//values that were removed from other dropdowns as a result of selecting this value
	//key of map is property name; array contains list of removed values
	removedValues: Map<string, string[]>;

	constructor(property: Property, private value: string, dependencies: Array<Dependency>) {
		this.property = property;
		this.textValue = value;
		this.dependencies = dependencies;
		this.removedValues = new Map<string, string[]>();
	}

	addRemovedValue(value: Value) {
		if (this.removedValues.has(value.property.name))
			if (!this.removedValues[value.property.name].includes(value.textValue))
				this.removedValues[value.property.name].push(value.textValue);
			else
				this.removedValues[value.property.name] = [value.textValue];
	}

	resetRemovedValues() {
		this.removedValues.clear();
	}

	removedValue(value: Value): boolean {
		if (this.removedValues.get(value.property.name) === undefined)
			return false;
		if (this.removedValues.get(value.property.name) === null)
			return false;

		return this.removedValues.get(value.property.name).includes(value.textValue);
	}
}