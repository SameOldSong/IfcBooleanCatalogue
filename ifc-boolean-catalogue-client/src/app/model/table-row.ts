import { stringToKeyValue } from '@angular/flex-layout/extended/typings/style/style-transforms';

export class TableRow {
	
	values: Map<string, string>;


	constructor() {
		this.values = new Map<string, string>();
	}

	addColumnValue(column: string, value: string){
		this.values.set(column, value);
	}

	getValue(property: string): string {
		return this.values.get(property);
	}

	print(){
		console.log("Printing table row");
		this.values.forEach((val: string, key : string) => {
			console.log("Prop " + key + " VAlue " + val);
		});
	}

}