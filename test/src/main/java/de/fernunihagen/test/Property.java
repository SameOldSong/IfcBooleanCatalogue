package de.fernunihagen.test;

import java.util.ArrayList;
import java.util.List;

public class Property {
	String name;
	public String getName() {
		return name;
	}

	public List<Value> getValues() {
		return values;
	}

	List<Value> values;

	Property(String name, List<Value> values) {
		this.name = name;
		if (values == null)
			this.values = new ArrayList<Value>();
		else
			this.values = values;
	}

	public void addValue(Value value) {
		values.add(value);
	}
	
	
}
