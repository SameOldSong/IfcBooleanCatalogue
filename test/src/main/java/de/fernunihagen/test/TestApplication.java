package de.fernunihagen.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


public class TestApplication {

	public static void main(String[] args) {
		
		for(String oper : LogicalOperator.getNames(LogicalOperator.class))
			System.out.println(oper);
	}

}
