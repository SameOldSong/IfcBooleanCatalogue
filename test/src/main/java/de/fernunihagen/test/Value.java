package de.fernunihagen.test;

import java.util.ArrayList;
import java.util.List;

public class Value {
	String name;
	public String getName() {
		return name;
	}

	public List<Property> getProperties() {
		return properties;
	}

	List<Property> properties;

	Value(String name, List<Property> properties) {
		this.name = name;
		if (properties == null)
			properties = new ArrayList<Property>();
		else
			this.properties = properties;
	}
}
