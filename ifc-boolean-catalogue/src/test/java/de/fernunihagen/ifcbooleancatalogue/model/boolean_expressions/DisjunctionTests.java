package de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions;



import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.metrics.IfcBenchmarkEnum;
import de.fernunihagen.ifcbooleancatalogue.model.Product;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.FailureWhenParsingExpressionException;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.catalogue.Dependent;
import de.fernunihagen.ifcbooleancatalogue.shared.bool_exp_evaluator_tests.test_helpers.TestHelper;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.MalformedBoolExpressionException;
import de.fernunihagen.ifcbooleancatalogue.shared.model.AbstractProperty;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractSimpleBoolExpression;


public class DisjunctionTests {
    private final static String HZ_ART_FLACH = "Heizkoerperart = Flachheizkoerper";
    private final static String HZ_ART_BAD = "Heizkoerperart = Badheizkoerper";
    private final static String HZ_SERIE_FLORES = "Heizkoerperserie = Flores";
    private final static String HZ_SERIE_VENTIL = "Heizkoerperserie = Ventil";
    private final static String HZ_SERIE_COMPACT = "Heizkoerperserie = Compact";
    private final static String HZ_SERIE_SANTORINI = "Heizkoerperserie = Santorini";
    private final static String TEMP_110 = "Temperatur = 110";
    private final static String TEMP_100 = "Temperatur = 100";
    private final static String TEMP_90 = "Temperatur = 90";

    private AbstractProperty property;

    @BeforeEach
    public void initProperties() {
//	property = new AbstractProperty("Temperatur", new Product("Heizkoerper"));
//	property.addValue("100");
//	property.addValue("90");
//	property.addValue("110");
	
    }

    @Test
    public void contextLoads() {
    }

//    @Test
//    public void generateTriggers_ConsistsOfSimpleComponents()
//	    throws FailureWhenParsingExpressionException, MalformedBoolExpressionException {
//	AbstractDisjunction disjunction = TestHelper.buildDisjunction(new String[] { HZ_SERIE_FLORES, HZ_SERIE_VENTIL, HZ_SERIE_COMPACT, HZ_SERIE_SANTORINI }, "");
//	
//	
//	String triggerList = disjunction.generateTriggers().stream().map(Object::toString)
//		.collect(Collectors.joining(";"));
//	
//	//TestHelper.printTriggers(conjunction);
//	
//	assertTrue(disjunction.generateTriggers().size() == 4);
//	assertTrue(triggerList.contains(HZ_SERIE_FLORES));
//	assertTrue(triggerList.contains(HZ_SERIE_COMPACT));
//	assertTrue(triggerList.contains(HZ_SERIE_VENTIL));
//	assertTrue(triggerList.contains(HZ_SERIE_SANTORINI));
//	
//    }
//
//    @Test
//    public void generateTriggers_ConsistsOfDisjunctionsAndSimpleComponents()
//	    throws FailureWhenParsingExpressionException, MalformedBoolExpressionException {
//	AbstractDisjunction disjunction1 = TestHelper.buildDisjunction(new String[] { HZ_SERIE_FLORES, HZ_SERIE_VENTIL, HZ_SERIE_SANTORINI }, "");
//	AbstractSimpleBoolExpression simple = TestHelper.buildSimpleExpression(HZ_ART_FLACH, "");
//	AbstractDisjunction disjunction2 = TestHelper.buildDisjunction(new String[] { TEMP_110, TEMP_100 }, "");
//	
//	AbstractDisjunction disjunction = new AbstractDisjunction(Arrays.asList(disjunction1, simple, disjunction2));
//	
//	
//	//TestHelper.printTriggers(disjunction);
//	String triggerList = disjunction.generateTriggers().stream().map(Object::toString)
//		.collect(Collectors.joining(";"));
//	assertTrue(disjunction.generateTriggers().size() == 6);
//	assertTrue(triggerList.contains(HZ_SERIE_FLORES));
//	assertTrue(triggerList.contains(HZ_ART_FLACH));
//	assertTrue(triggerList.contains(HZ_SERIE_VENTIL));
//	assertTrue(triggerList.contains(HZ_SERIE_SANTORINI));
//	assertTrue(triggerList.contains(TEMP_110));
//	assertTrue(triggerList.contains(TEMP_100));
//	
//    }
//
//    @Test
//    public void generateTriggers_ConsistsOfDisjunctionsAndSimpleComponentsNotEqual()
//	    throws FailureWhenParsingExpressionException, MalformedBoolExpressionException {
//	AbstractDisjunction disjunction1 = TestHelper.buildDisjunction(new String[] { HZ_SERIE_FLORES, HZ_SERIE_VENTIL, HZ_SERIE_SANTORINI }, "");
//	AbstractDisjunction disjunction2 = TestHelper.buildDisjunction(new String[] { HZ_ART_FLACH, HZ_ART_BAD }, "");
//	AbstractSimpleBoolExpression notEqualExpr = new AbstractSimpleBoolExpression(property, RelationalOperator.GREATERTHAN, "90");
//	
//	AbstractDisjunction disjunction = new AbstractDisjunction(Arrays.asList(disjunction1, notEqualExpr, disjunction2));
//	
//	
//	//TestHelper.printTriggers(disjunction);
//	String triggerList = disjunction.generateTriggers().stream().map(Object::toString)
//		.collect(Collectors.joining(";"));
//	assertTrue(disjunction.generateTriggers().size() == 7);
//	assertTrue(triggerList.contains(HZ_SERIE_FLORES));
//	assertTrue(triggerList.contains(HZ_ART_FLACH));
//	assertTrue(triggerList.contains(HZ_ART_BAD));
//	assertTrue(triggerList.contains(HZ_SERIE_VENTIL));
//	assertTrue(triggerList.contains(HZ_SERIE_SANTORINI));
//	assertTrue(triggerList.contains(TEMP_110));
//	assertTrue(triggerList.contains(TEMP_100));
//	
//    }
//    
//    @Test
//    public void generateDependents_ConsistsOfSimpleComponents()
//	    throws FailureWhenParsingExpressionException, MalformedBoolExpressionException {
//	AbstractDisjunction disjunction = TestHelper.buildDisjunction(new String[] { HZ_SERIE_FLORES, HZ_SERIE_VENTIL, HZ_SERIE_COMPACT, HZ_SERIE_SANTORINI }, "");
//	
//	
//	List<Dependent> dependents = disjunction.generateDependents();
//	String dependentsList = dependents.stream().map(Object::toString)
//		.collect(Collectors.joining(";"));
//	
//	//TestHelper.printDependents(disjunction);
//	
//	assertTrue(dependents.size() == 1);
//	assertTrue(dependentsList.equals("Heizkoerperserie: Flores,Ventil,Compact,Santorini"));
//	
//    }
//    
    
}
