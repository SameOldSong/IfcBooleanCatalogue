package de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions;


import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.metrics.IfcBenchmarkEnum;
import de.fernunihagen.ifcbooleancatalogue.model.Product;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.FailureWhenParsingExpressionException;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.catalogue.Dependent;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.catalogue.Trigger;
import de.fernunihagen.ifcbooleancatalogue.shared.bool_exp_evaluator_tests.test_helpers.TestHelper;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.MalformedBoolExpressionException;
import de.fernunihagen.ifcbooleancatalogue.shared.model.AbstractProperty;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractSimpleBoolExpression;


public class ConjunctionTests {
    private final static String HZ_ART_FLACH = "Heizkoerperart = Flachheizkoerper";
    private final static String HZ_ART_BAD = "Heizkoerperart = Badheizkoerper";
    private final static String HZ_SERIE_FLORES = "Heizkoerperserie = Flores";
    private final static String HZ_SERIE_COMPACT = "Heizkoerperserie = Compact";
    private final static String TEMP_110 = "Temperatur = 110";
    private final static String TEMP_100 = "Temperatur = 100";
    private final static String TEMP_90 = "Temperatur = 90";
    private final static String TEMP_80 = "Temperatur = 80";

    private AbstractProperty property;

    @BeforeEach
    public void initProperties() {
//	property = new AbstractProperty("Temperatur", new Product("Heizkoerper"));
//	property.addValue("80");
//	property.addValue("90");
//	property.addValue("110");
//	property.addValue("100");

    }

    @Test
    public void contextLoads() {
    }

//    // (Temperatur != 100) AND (Temperatur != 90)
//    @Test
//    public void generateTriggers_ConsistsOfTwoSimplesOperatorNotEqualSameProperty()
//	    throws FailureWhenParsingExpressionException, MalformedBoolExpressionException {
//	AbstractSimpleBoolExpression notEqualExpr1 = new AbstractSimpleBoolExpression(property, RelationalOperator.NOTEQUALTO, "100");
//	AbstractSimpleBoolExpression notEqualExpr2 = new AbstractSimpleBoolExpression(property, RelationalOperator.NOTEQUALTO, "90");
//	AbstractConjunction conjunction = new AbstractConjunction(Arrays.asList(notEqualExpr1, notEqualExpr2));
//	String triggerList = conjunction.generateTriggers().stream().map(Object::toString)
//		.collect(Collectors.joining(";"));
//
//	// TestHelper.printTriggers(conjunction);
//
//	assertTrue(conjunction.generateTriggers().size() == 2);
//	assertTrue(triggerList.contains(TEMP_110));
//	assertTrue(triggerList.contains(TEMP_80));
//
//    }
//
//    @Test
//    public void generateTriggers_ConsistsOfTwoDisjunctionsAndSimpleOperatorNotEqual()
//	    throws FailureWhenParsingExpressionException, MalformedBoolExpressionException {
//	AbstractDisjunction disjunction1 = TestHelper.buildDisjunction(new String[] { HZ_ART_FLACH, HZ_ART_BAD }, "");
//	AbstractDisjunction disjunction2 = TestHelper.buildDisjunction(new String[] { HZ_SERIE_FLORES, HZ_SERIE_COMPACT }, "");
//	AbstractSimpleBoolExpression notEqualExpr = new AbstractSimpleBoolExpression(property, RelationalOperator.GREATERTHAN, "90");
//	AbstractConjunction conjunction = new AbstractConjunction(Arrays.asList(disjunction1, notEqualExpr, disjunction2));
//	String triggerList = conjunction.generateTriggers().stream().map(Object::toString)
//		.collect(Collectors.joining(";"));
//
//	// TestHelper.printTriggers(conjunction);
//
//	assertTrue(conjunction.generateTriggers().size() == 8);
//	assertTrue(triggerList.contains(HZ_SERIE_FLORES + "," + TEMP_100 + "," + HZ_ART_FLACH));
//	assertTrue(triggerList.contains(HZ_SERIE_COMPACT + "," + TEMP_100 + "," + HZ_ART_FLACH));
//	assertTrue(triggerList.contains(HZ_SERIE_FLORES + "," + TEMP_110 + "," + HZ_ART_FLACH));
//	assertTrue(triggerList.contains(HZ_SERIE_COMPACT + "," + TEMP_110 + "," + HZ_ART_FLACH));
//
//	assertTrue(triggerList.contains(HZ_SERIE_FLORES + "," + TEMP_100 + "," + HZ_ART_BAD));
//	assertTrue(triggerList.contains(HZ_SERIE_COMPACT + "," + TEMP_100 + "," + HZ_ART_BAD));
//	assertTrue(triggerList.contains(HZ_SERIE_FLORES + "," + TEMP_110 + "," + HZ_ART_BAD));
//	assertTrue(triggerList.contains(HZ_SERIE_COMPACT + "," + TEMP_110 + "," + HZ_ART_BAD));
//    }
//
//    @Test
//    public void generateTriggers_ConsistsOfThreeDisjunctions()
//	    throws FailureWhenParsingExpressionException, MalformedBoolExpressionException {
//	AbstractDisjunction disjunction1 = TestHelper.buildDisjunction(new String[] { HZ_ART_FLACH, HZ_ART_BAD }, "");
//
//	AbstractDisjunction disjunction2 = TestHelper.buildDisjunction(new String[] { HZ_SERIE_FLORES, HZ_SERIE_COMPACT }, "");
//
//	AbstractDisjunction disjunction3 = TestHelper.buildDisjunction(new String[] { TEMP_110, TEMP_90, TEMP_100 }, "");
//
//	AbstractConjunction conjunction = new AbstractConjunction(Arrays.asList(disjunction1, disjunction2, disjunction3));
//	// TestHelper.printTriggers(conjunction);
//
//	String triggerList = conjunction.generateTriggers().stream().map(Object::toString)
//		.collect(Collectors.joining(";"));
//
//	assertTrue(conjunction.generateTriggers().size() == 12);
//	assertTrue(triggerList.contains(TEMP_110 + "," + HZ_SERIE_FLORES + "," + HZ_ART_FLACH));
//	assertTrue(triggerList.contains(TEMP_90 + "," + HZ_SERIE_FLORES + "," + HZ_ART_FLACH));
//	assertTrue(triggerList.contains(TEMP_100 + "," + HZ_SERIE_FLORES + "," + HZ_ART_FLACH));
//
//	assertTrue(triggerList.contains(TEMP_110 + "," + HZ_SERIE_COMPACT + "," + HZ_ART_FLACH));
//	assertTrue(triggerList.contains(TEMP_90 + "," + HZ_SERIE_COMPACT + "," + HZ_ART_FLACH));
//	assertTrue(triggerList.contains(TEMP_100 + "," + HZ_SERIE_COMPACT + "," + HZ_ART_FLACH));
//
//	assertTrue(triggerList.contains(TEMP_110 + "," + HZ_SERIE_FLORES + "," + HZ_ART_BAD));
//	assertTrue(triggerList.contains(TEMP_90 + "," + HZ_SERIE_FLORES + "," + HZ_ART_BAD));
//	assertTrue(triggerList.contains(TEMP_100 + "," + HZ_SERIE_FLORES + "," + HZ_ART_BAD));
//
//	assertTrue(triggerList.contains(TEMP_110 + "," + HZ_SERIE_COMPACT + "," + HZ_ART_BAD));
//	assertTrue(triggerList.contains(TEMP_90 + "," + HZ_SERIE_COMPACT + "," + HZ_ART_BAD));
//	assertTrue(triggerList.contains(TEMP_100 + "," + HZ_SERIE_COMPACT + "," + HZ_ART_BAD));
//    }
//
//    @Test
//    public void generateTriggers_ConsistsOfTwoDisjunctionsAndSimple()
//	    throws FailureWhenParsingExpressionException, MalformedBoolExpressionException {
//	AbstractDisjunction disjunction1 = TestHelper.buildDisjunction(new String[] { HZ_ART_FLACH, HZ_ART_BAD }, "");
//
//	AbstractDisjunction disjunction2 = TestHelper.buildDisjunction(new String[] { TEMP_110, TEMP_90, TEMP_100 }, "");
//	AbstractSimpleBoolExpression simple = TestHelper.buildSimpleExpression(HZ_SERIE_FLORES, "");
//
//	AbstractConjunction conjunction = new AbstractConjunction(Arrays.asList(disjunction1, simple, disjunction2));
//	List <Trigger> triggers = conjunction.generateTriggers();
//	String triggerList = triggers.stream().map(Object::toString)
//		.collect(Collectors.joining(";"));
//
//	assertTrue(triggers.size() == 6);
//	assertTrue(triggerList.contains(TEMP_110 + "," + HZ_ART_FLACH + "," + HZ_SERIE_FLORES));
//	assertTrue(triggerList.contains(TEMP_90 + "," + HZ_ART_FLACH + "," + HZ_SERIE_FLORES));
//	assertTrue(triggerList.contains(TEMP_100 + "," + HZ_ART_FLACH + "," + HZ_SERIE_FLORES));
//
//	assertTrue(triggerList.contains(TEMP_110 + "," + HZ_ART_BAD + "," + HZ_SERIE_FLORES));
//	assertTrue(triggerList.contains(TEMP_90 + "," + HZ_ART_BAD + "," + HZ_SERIE_FLORES));
//	assertTrue(triggerList.contains(TEMP_100 + "," + HZ_ART_BAD + "," + HZ_SERIE_FLORES));
//
//    }
//
//    @Test
//    public void getTrigger_ConsistsOfSimpleExpressions()
//	    throws FailureWhenParsingExpressionException, MalformedBoolExpressionException {
//
//	AbstractSimpleBoolExpression simple = TestHelper.buildSimpleExpression(HZ_SERIE_FLORES, "");
//	AbstractSimpleBoolExpression simple2 = TestHelper.buildSimpleExpression(HZ_ART_BAD, "");
//	AbstractSimpleBoolExpression simple3 = TestHelper.buildSimpleExpression(TEMP_100, "");
//
//	AbstractConjunction conjunction = new AbstractConjunction(Arrays.asList(simple2, simple, simple3));
//	String triggerList = conjunction.generateTriggers().stream().map(Object::toString)
//		.collect(Collectors.joining(";"));
//
//	assertTrue(conjunction.generateTriggers().size() == 1);
//	assertTrue(triggerList.contains(HZ_ART_BAD + "," + HZ_SERIE_FLORES + "," + TEMP_100));
//
//    }
//
//    @Test
//    public void getDependents_ConsistsOfSimpleExpressions()
//	    throws FailureWhenParsingExpressionException, MalformedBoolExpressionException {
//
//	AbstractSimpleBoolExpression simple = TestHelper.buildSimpleExpression(HZ_SERIE_FLORES, "");
//	AbstractSimpleBoolExpression simple2 = TestHelper.buildSimpleExpression(HZ_ART_BAD, "");
//	AbstractSimpleBoolExpression simple3 = TestHelper.buildSimpleExpression(TEMP_100, "");
//
//	AbstractConjunction conjunction = new AbstractConjunction(Arrays.asList(simple2, simple, simple3));
//
//	List<Dependent> dependents = conjunction.generateDependents();
//	String dependendsList = dependents.stream().map(Object::toString).collect(Collectors.joining(";"));
//
//	//TestHelper.printDependents(conjunction);
//	assertTrue(dependents.size() == 3);
//	assertTrue(dependendsList.contains("Heizkoerperart: Badheizkoerper"));
//	assertTrue(dependendsList.contains("Heizkoerperserie: Flores"));
//	assertTrue(dependendsList.contains("Temperatur: 100"));
//
//    }
//
//    @Test
//    public void getDependents_ConsistsOfThreeDisjunctions()
//	    throws FailureWhenParsingExpressionException, MalformedBoolExpressionException {
//
//	AbstractDisjunction disjunction1 = TestHelper.buildDisjunction(new String[] { HZ_ART_FLACH, HZ_ART_BAD }, "");
//
//	AbstractDisjunction disjunction2 = TestHelper.buildDisjunction(new String[] { HZ_SERIE_FLORES, HZ_SERIE_COMPACT }, "");
//
//	AbstractDisjunction disjunction3 = TestHelper.buildDisjunction(new String[] { TEMP_110, TEMP_90, TEMP_100 }, "");
//
//	AbstractConjunction conjunction = new AbstractConjunction(Arrays.asList(disjunction1, disjunction2, disjunction3));
//
//	List<Dependent> dependents = conjunction.generateDependents();
//	String dependendsList = dependents.stream().map(Object::toString).collect(Collectors.joining(";"));
//
//	TestHelper.printDependents(conjunction);
//	assertTrue(dependents.size() == 3);
//	assertTrue(dependendsList.contains("Heizkoerperart: Flachheizkoerper,Badheizkoerper"));
//	assertTrue(dependendsList.contains("Heizkoerperserie: Flores,Compact"));
//	assertTrue(dependendsList.contains("Temperatur: 110,90,100"));
//
//    }
//
//    @Test
//    public void getDependents_ConsistsOfTwoDisjunctionsAndSimpleOperatorEqual()
//	    throws FailureWhenParsingExpressionException, MalformedBoolExpressionException {
//
//	AbstractDisjunction disjunction1 = TestHelper.buildDisjunction(new String[] { HZ_ART_FLACH, HZ_ART_BAD }, "");
//
//	AbstractDisjunction disjunction2 = TestHelper.buildDisjunction(new String[] { TEMP_110, TEMP_90, TEMP_100 }, "");
//	AbstractSimpleBoolExpression simple = TestHelper.buildSimpleExpression(HZ_SERIE_FLORES, "");
//
//	AbstractConjunction conjunction = new AbstractConjunction(Arrays.asList(disjunction1, simple, disjunction2));
//
//	List<Dependent> dependents = conjunction.generateDependents();
//	String dependendsList = dependents.stream().map(Object::toString).collect(Collectors.joining(";"));
//
//	TestHelper.printDependents(conjunction);
//	assertTrue(dependents.size() == 3);
//	assertTrue(dependendsList.contains("Heizkoerperart: Flachheizkoerper,Badheizkoerper"));
//	assertTrue(dependendsList.contains("Heizkoerperserie: Flores"));
//	assertTrue(dependendsList.contains("Temperatur: 110,90,100"));
//
//    }
//
//    @Test
//    public void getDependents_ConsistsOfTwoDisjunctionsAndSimpleOperatorNotEqual()
//	    throws FailureWhenParsingExpressionException, MalformedBoolExpressionException {
//
//	AbstractDisjunction disjunction1 = TestHelper.buildDisjunction(new String[] { HZ_ART_FLACH, HZ_ART_BAD }, "");
//	AbstractDisjunction disjunction2 = TestHelper.buildDisjunction(new String[] { HZ_SERIE_FLORES, HZ_SERIE_COMPACT }, "");
//	AbstractSimpleBoolExpression notEqualExpr = new AbstractSimpleBoolExpression(property, RelationalOperator.GREATERTHAN, "90");
//	AbstractConjunction conjunction = new AbstractConjunction(Arrays.asList(disjunction1, notEqualExpr, disjunction2));
//
//	List<Dependent> dependents = conjunction.generateDependents();
//	String dependendsList = dependents.stream().map(Object::toString).collect(Collectors.joining(";"));
//
//	TestHelper.printDependents(conjunction);
//	assertTrue(dependents.size() == 3);
//	assertTrue(dependendsList.contains("Heizkoerperart: Flachheizkoerper,Badheizkoerper"));
//	assertTrue(dependendsList.contains("Heizkoerperserie: Flores,Compact"));
//	assertTrue(dependendsList.contains("Temperatur: 110,100"));
//
//    }

}
