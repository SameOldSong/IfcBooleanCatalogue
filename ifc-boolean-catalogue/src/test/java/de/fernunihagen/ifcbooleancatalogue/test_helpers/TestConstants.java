package de.fernunihagen.ifcbooleancatalogue.test_helpers;

import java.time.format.DateTimeFormatter;

public interface TestConstants {
    //Boolean to Ifc
    public final String DOUBLE_NEW_LINE = "\n\n";
    public final String NEW_LINE = "\n";
    public final String NEW_LINE_REG_EXP = "\\R";
    final String IFC_FILE_SCHEMA = "IFC4";
    	
    public final String SINGLE_QOUTE = "'";
    public final String ASTERISK = "*";
    public final String SEMICOLON = ";";
    public final String FORWARD_SLASH = "/";
    public final String PERIOD = ".";
    public final String OPEN_BRACKET = "(";
    public final String BLANK = " ";
    public final String CLOSE_BRACKET = ")";
    public final String HYPHEN = "-";
    public final String COLON = ":";
    public final String COMMA = ",";
    public final String DOLLAR = "$";
    public final String HASH = "#";
    public final String MULTILINE_START = "{";
    public final String MULTILINE_END = "}";
    public final String OPEN_BOX_BRACKET = "[";
    public final String CLOSE_BOX_BRACKET = "]";
    public final String EMPTY_STRING = "";
    
    public final String EQUALS = "=";
    public final String GREATER_THAN_OR_EQUAL_TO = ">=";
    public final String LESS_THAN_OR_EQUAL_TO = "<=";

    public final String OR = "OR";
    public final String EQUIVALENCE = "<==>";
    public final String IMPLICATION = "==>";
    public final String NOT = "NOT";

    public final String ARTICLE_NUM = "ArtNum";
    public final String IFCPROXY = "IfcProxy";

    
    public final String LINE_SEPARATOR = System.getProperty("line.separator");

    public final String DEFINED_BY_RELATIONSHIP = "IsDefinedBy";
    public final String DEFINES_BY_PROPERTIES_RELATIONSHIP = "IfcRelDefinesByProperties";
    public final String RELATING_PROPERTY_DEFINITION = "RelatingPropertyDefinition";
    public final String IFC_PROPERTY_SET = "IfcPropertySet";
    static final String HAS_PROPERTIES = "HasProperties";
    static final String IFC_PROPERTY_SINGLE_VALUE = "IfcPropertySingleValue";
    static final String NOMINAL_VALUE = "NominalValue";

    public final String HEADER_START = "#HEADER";
    public final String HEADER_END = "#ENDHEADER";
    public final String IFC_FILE_END = DOUBLE_NEW_LINE + "ENDSEC;" + DOUBLE_NEW_LINE + "END-ISO-10303-21;";

    public final String STEPPED_INTERVAL = "STEP";
    public final String NONE = "NONE";

    public final String IFC_PROPERTY_LIST_VALUE = "IfcPropertyListValue";
    public final String LIST_VALUES = "ListValues";

    public final String FILE_DIRECTORY = System.getProperty("user.home");
    public final String TIMESTAMP = "dd-MM-yyyy_hhmm";
    public final String TXT = ".txt";
    public final String IFC = ".ifc";
    public final DateTimeFormatter FOMATTER = DateTimeFormatter.ofPattern(TIMESTAMP);
    
    //for conversion from IFC 
    public final String IFC_OBJECTIVE = "IFCOBJECTIVE";
    public final String IFC_METRIC = "IFC_METRIC";
    public final String IFC_END = "ENDSEC";
    
    // unit tests
    public final String PURMO_FOLDER = "src/test/resources/Purmo/";
    public final String PURMO_FOLDER_WITH_ART_NUM = "src/test/resources/Purmo/withArtNum/";
    public final String PURMO_FOLDER_JSON = "src/test/resources/Purmo/expected_json/";
    public final String PURMO_FOLDER_IFC = "src/test/resources/Purmo/ifc/";
   
}