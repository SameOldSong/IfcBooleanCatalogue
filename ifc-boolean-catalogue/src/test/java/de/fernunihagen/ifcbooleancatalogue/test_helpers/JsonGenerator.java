package de.fernunihagen.ifcbooleancatalogue.test_helpers;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.catalogue.Dependency;

public class JsonGenerator {
    /**
     * transform object to JSON string
     * 
     * @return: JSON string
     * @throws JsonProcessingException
     */
    public static String getJsonFromObject(Object object) throws JsonProcessingException {
	ObjectMapper objectMapper = new ObjectMapper();
	String dependenciesAsJson = objectMapper.writeValueAsString(object);
	return dependenciesAsJson;
    }
    
      
   
    
    public static List <Dependency> getDependenciesFromJsonFile(String filePath) throws IOException {
	ObjectMapper objectMapper = new ObjectMapper();
	return Arrays.asList(objectMapper.readValue(new File(filePath), Dependency[].class));
	
    }
    
   
    public static List <Dependency> getDependenciesFromJsonString(String json) throws IOException {
	ObjectMapper objectMapper = new ObjectMapper();
	return objectMapper.reader()
		      .forType(new TypeReference<List<Dependency>>() {})
		      .readValue(json);
	
    }
    
    public static Dependency getDependencyFromJsonString(String json) throws IOException {
   	ObjectMapper objectMapper = new ObjectMapper();
   	return objectMapper.readValue(json, Dependency.class);
   	
       }
    
   

}
