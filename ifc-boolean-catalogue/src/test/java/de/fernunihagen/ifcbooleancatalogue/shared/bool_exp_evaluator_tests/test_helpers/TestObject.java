package de.fernunihagen.ifcbooleancatalogue.shared.bool_exp_evaluator_tests.test_helpers;

import java.util.Map;

public class TestObject {
    
    public String given;
    public String when;
    public String then;
    public String expression;
    public Map<String, String> values;
    
    public TestObject(String given, String when, String then, String expression, Map values) {
	super();
	this.given = given;
	this.when = when;
	this.then = then;
	this.expression = expression;
	this.values = values;
    }
    public String getGiven() {
        return given;
    }
    public String getWhen() {
        return when;
    }
    public String getThen() {
        return then;
    }
    public String getExpression() {
        return expression;
    }
    public Map getValues() {
	return values;
    }
    
    
    

}
