package de.fernunihagen.ifcbooleancatalogue.shared.string_operations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import de.fernunihagen.ifcbooleancatalogue.model.MissingPropertyException;
import de.fernunihagen.ifcbooleancatalogue.model.Product;
import de.fernunihagen.ifcbooleancatalogue.model.Value;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.MalformedBoolExpressionException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers.StringHelper;
import de.fernunihagen.ifcbooleancatalogue.shared.model.AbstractProperty;

public class ValueCombinationsTest {
    Product product;

    @BeforeEach
    public void initProperties() {
	product = new Product("Heizkoerper");

    }

    @Test
    public void contextLoads() {
    }

//    @Test
//    public void testCataloguePurmo()
//	    throws MalformedBoolExpressionException, IOException, InvalidUnitException, MissingPropertyException {
//	AbstractProperty prop1 = new AbstractProperty("Heizkoerperart", product);
//	prop1.setValues(Arrays.asList(new Value("Art1", prop1), new Value("Art2", prop1)));
//	AbstractProperty prop2 = new AbstractProperty("Heizkoerperserie", product);
//	prop2.setValues(Arrays.asList(new Value("Serie1", prop2), new Value("Serie2", prop2)));
//	
//	AbstractProperty prop3 = new AbstractProperty("Laenge", product);
//	prop3.setValues(Arrays.asList(new Value("100", prop3), new Value("200", prop3)));
//
//	List<Map<String, String>> maps =StringHelper.getCombinations(new ArrayList(Arrays.asList(prop1, prop2, prop3)));
//	
//	for(Map<String, String> map : maps)
//	    System.err.println(map.values());
//    }

}
