package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.expressions.test_helpers;

import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.aggregator.ArgumentsAggregator;

public class TestObjectAggregator implements ArgumentsAggregator {
    @Override
    public TestObject aggregateArguments(ArgumentsAccessor arguments, ParameterContext context) {
	String given = arguments.getString(0);
	String expression = arguments.getString(1);
	String operator = arguments.getString(2);
	String numCompoundComponents = arguments.getString(3);
	String numSimpleComponents = arguments.getString(4);

	return new TestObject(given, expression, operator, numCompoundComponents, numSimpleComponents);
    }
}