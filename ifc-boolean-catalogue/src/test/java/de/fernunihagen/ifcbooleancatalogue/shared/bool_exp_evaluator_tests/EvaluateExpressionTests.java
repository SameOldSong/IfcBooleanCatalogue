package de.fernunihagen.ifcbooleancatalogue.shared.bool_exp_evaluator_tests;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import de.fernunihagen.ifcbooleancatalogue.model.MissingPropertyException;
import de.fernunihagen.ifcbooleancatalogue.shared.BoolExpEvaluator;
import de.fernunihagen.ifcbooleancatalogue.shared.bool_exp_evaluator_tests.test_helpers.CsvToTestObject;
import de.fernunihagen.ifcbooleancatalogue.shared.bool_exp_evaluator_tests.test_helpers.TestObject;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.MalformedBoolExpressionException;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractBoolExpression;
import de.fernunihagen.ifcbooleancatalogue.test_settings.TestSetUp;

public class EvaluateExpressionTests extends TestSetUp {

    @CsvFileSource(resources = "/shared/bool_exp_evaluator_tests/compound_expr.csv", numLinesToSkip = 1, delimiter = ';')
    @ParameterizedTest(name = "{index} ==> given{0}_when{1}_then{2}")
    public void testCompoundExpressions(@CsvToTestObject TestObject test)
	    throws MalformedBoolExpressionException, MissingPropertyException {
//	AbstractBoolExpression boolExp = compoundExpGenerator.buildCompoundBoolExp(test.getExpression(), product);
//
//	assertTrue(BoolExpEvaluator.isExpressionTrue(boolExp, test.getValues()) == Boolean.parseBoolean(test.getThen()));
    }
    
    
    @CsvFileSource(resources = "/shared/bool_exp_evaluator_tests/simple_expr.csv", numLinesToSkip = 1, delimiter = ';')
    @ParameterizedTest(name = "{index} ==> given{0}_when{1}_then{2}")
    public void testSimpleExpressions(@CsvToTestObject TestObject test)
	    throws MalformedBoolExpressionException, MissingPropertyException {
//	AbstractBoolExpression boolExp = simpleExpGenerator.buildSimpleExpression(test.getExpression(), product);
//
//	assertTrue(BoolExpEvaluator.isExpressionTrue(boolExp, test.getValues()) == Boolean.parseBoolean(test.getThen()));
    }
}
