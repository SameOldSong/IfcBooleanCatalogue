package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.MalformedBoolExpressionException;

public class BracketsHelperTests {

    @Test
    public void simpleExpressionBrackets() throws MalformedBoolExpressionException {
	String expression = "(Heizkörperart = Flachheizkörper)";
	assertTrue(BracketsHelper.getClosingBracketIndex(expression, 0) == 32);
    }

    @Test
    public void compoundExpressionTwoComponents() throws MalformedBoolExpressionException {
	String expression = "((Heizkörperart = Flachheizkörper) OR (Heizkörperart = Elektroheizkörper))";
	assertTrue(BracketsHelper.getClosingBracketIndex(expression, 0) == 73);
	assertTrue(BracketsHelper.getClosingBracketIndex(expression, 1) == 33);
	assertTrue(BracketsHelper.getClosingBracketIndex(expression, 38) == 72);
    }

    @Test
    public void compoundExpressionThreeComponents() throws MalformedBoolExpressionException {
	String expression = "((Heizkörperart = Flachheizkörper) OR (Heizkörperart = Elektroheizkörper) OR (Heizkörperart = Konvektor))";
	assertTrue(BracketsHelper.getClosingBracketIndex(expression, 0) == 104);
	assertTrue(BracketsHelper.getClosingBracketIndex(expression, 1) == 33);
	assertTrue(BracketsHelper.getClosingBracketIndex(expression, 38) == 72);
	assertTrue(BracketsHelper.getClosingBracketIndex(expression, 77) == 103);
    }

    @Test
    public void whenExceptionThrown_thenExpectationSatisfied() throws MalformedBoolExpressionException {

	assertThrows(MalformedBoolExpressionException.class, () -> {
	    String expression = "((Heizkörperart = Flachheizkörper) OR (Heizkörperart = Elektroheizkörper) OR (Heizkörperart = Konvektor))";
	    BracketsHelper.getClosingBracketIndex(expression, 3);
	});
    }

    @Test
    public void compoundExpressionCompoundComponentLeft() throws MalformedBoolExpressionException {
	String expression = "((((Heizkörperart = Flachheizkörper) OR (Heizkörperart = Elektroheizkörper)) AND (Heizkörperart = Konvektor)) OR (A > B))";
	assertTrue(BracketsHelper.getClosingBracketIndex(expression, 0) == 120);
	assertTrue(BracketsHelper.getClosingBracketIndex(expression, 1) == 108);
	assertTrue(BracketsHelper.getClosingBracketIndex(expression, 81) == 107);
	assertTrue(BracketsHelper.getClosingBracketIndex(expression, 2) == 75);
	assertTrue(BracketsHelper.getClosingBracketIndex(expression, 3) == 35);
    }

    @Test
    public void compoundExpressionCompoundComponentRight() throws MalformedBoolExpressionException {
	String expression = "(Heizkörperart = Flachheizkörper) OR (((Heizkörperart = Elektroheizkörper) AND (Heizkörperart = Konvektor)) OR (A > B))";
	assertTrue(BracketsHelper.getClosingBracketIndex(expression, 0) == 32);
	assertTrue(BracketsHelper.getClosingBracketIndex(expression, 37) == 118);
	assertTrue(BracketsHelper.getClosingBracketIndex(expression, 38) == 106);
	assertTrue(BracketsHelper.getClosingBracketIndex(expression, 39) == 73);

    }

    @Test
    public void simpleExpressionInvalid() throws MalformedBoolExpressionException {
	assertThrows(MalformedBoolExpressionException.class, () -> {
	    String expression = "(Heizkörperart = Flachheizkörper";
	    BracketsHelper.getClosingBracketIndex(expression, 0);
	});
    }

    @Test
    public void compoundExpressionInvalid() throws MalformedBoolExpressionException {

	assertThrows(MalformedBoolExpressionException.class, () -> {
	    String expression = "(((Heizkörperart = Elektroheizkörper) AND (Heizkörperart = Konvektor) OR (A > B))";

	    BracketsHelper.getClosingBracketIndex(expression, 0);
	});
    }

}
