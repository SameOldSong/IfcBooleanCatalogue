package de.fernunihagen.ifcbooleancatalogue.shared;


import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.springframework.boot.test.context.SpringBootTest;

import de.fernunihagen.ifcbooleancatalogue.model.MissingPropertyException;
import de.fernunihagen.ifcbooleancatalogue.model.Product;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.FailureWhenParsingExpressionException;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.catalogue.Dependency;
import de.fernunihagen.ifcbooleancatalogue.shared.DependencyManager;
import de.fernunihagen.ifcbooleancatalogue.shared.bool_exp_evaluator_tests.test_helpers.TestHelper;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.MalformedBoolExpressionException;
import de.fernunihagen.ifcbooleancatalogue.shared.model.AbstractProperty;
import de.fernunihagen.ifcbooleancatalogue.test_helpers.JsonGenerator;
import de.fernunihagen.ifcbooleancatalogue.test_settings.TestConstants;


public class DependencyManagerTests extends JsonGenerator implements TestConstants {
    private DependencyManager dependencyManager;
    private Product product;
    private AbstractProperty property1;
    private AbstractProperty property2;
    
    private String jsonFile;
    private String boolExpression;
    
    public DependencyManagerTests(String jsonFile, String boolExpression) {
	this.jsonFile = jsonFile;
	this.boolExpression = boolExpression;
    }
  
   
    @ParameterizedTest(name = "given{0}_whenGeneratingDependencies_thenCorrectAgainstJson")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"ImplicationFromSimpleEqualExpressions", "(Heizkoerperart = Flachheizkoerper) ==> (Heizkoerperserie = Compact)"},
                {"ImplicationWithDisjunctionOfSimpleExpressionsRightSide", "(Heizkoerperart = Flachheizkoerper) ==> (Heizkoerperserie = Compact OR Compact Hygiene OR Ventil Compact\r\n"
        		+ "OR Ventil Compact Hygiene OR Ventil Compact M OR Vertical OR Plan Compact OR Plan Compact Hygiene OR RAMO Compact)"}
        });
    }
    @BeforeEach
    public void initProductProperties() {
//	product = new Product("Heizkoerper");
//
//	catalogue = new Catalogue("HZ Catalogue");
//	catalogue.addProduct(product);
//
//	// init managers
//	boolExpManager = new AbstractBoolExpManager();
//	dependencyManager = new DependencyManager();
//
//	// init properties
//	property1 = new AbstractProperty("Heizkoerperart", product);
//	property1.addValue("Flachheizkoerper");
//	property2 = new AbstractProperty("Heizkoerperserie", product);
//	property2.addValue("Compact");
//	property2.addValue("Compact Hygiene");
//	property2.addValue("RAMO Compact");
//	property2.addValue("Ventil Compact");
//	property2.addValue("Ventil Compact Hygiene");
//	property2.addValue("Ventil Compact M");
//	property2.addValue("Vertical");
//	property2.addValue("Plan Compact");
//	property2.addValue("Plan Compact Hygiene");
//
//	product.addProperty(property1);
//	product.addProperty(property2);

    }

//    @Test
//    public void generateDependencies() throws MalformedBoolExpressionException, MissingPropertyException, IOException, FailureWhenParsingExpressionException {
//	generateBoolExpressions();
//	List<Dependency> dependencies = createDependencies(); 
//	checkAgainstExpectedDependenciesInJson(dependencies);
//    }
    
//    private void generateBoolExpressions() throws MalformedBoolExpressionException, MissingPropertyException {
//	
//	product.setMainExpression(
//		boolExpManager.generateExpressionsWithoutArtNum(Arrays.asList(boolExpression), product));
//
//    }
//    
//    private List<Dependency> createDependencies() throws MalformedBoolExpressionException, FailureWhenParsingExpressionException {
//	return dependencyManager.generateDependenciesForProduct(product);
//    }
    
//    private void checkAgainstExpectedDependenciesInJson(List<Dependency> dependencies) throws IOException {
//	String jsonPath = DEPENDENCY_MANAGER_FOLDER + jsonFile + ".json";
//	List<Dependency> expectedDependencies = JsonGenerator.getDependenciesFromJsonFile(jsonPath);
//	assertTrue(TestHelper.listsEqual(dependencies, expectedDependencies));
//    }

}
