package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.test_setup.TestSetUp;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.ParsingException;

public class SimpleBoolExpressionTests extends TestSetUp {

    @ParameterizedTest
    @MethodSource("provideExpressionAndExpectedOperator")
    public void should_ExtractCorrectOperator_WhenValidSimpleExpression(String expressionLine,
	    RelationalOperatorInFile operator) throws ParsingException {
	SimpleBoolExpressionInFile expression = new SimpleBoolExpressionInFile(expressionLine);
	assertEquals(expression.getOperator(), operator);
    }

    private static Stream<Arguments> provideExpressionAndExpectedOperator() {
	return Stream.of(Arguments.of("Heizkoerperserie = Compact", RelationalOperatorInFile.EQUALTO),
		Arguments.of("(Heizkoerperserie != Compact)", RelationalOperatorInFile.NOTEQUALTO),
		Arguments.of("Baulaenge > 100", RelationalOperatorInFile.GREATERTHAN),
		Arguments.of("(Baulaenge >= 100)", RelationalOperatorInFile.GREATERTHANOREQUALTO),
		Arguments.of("Baulaenge < 100", RelationalOperatorInFile.LESSTHAN),
		Arguments.of("Baulaenge <= 100", RelationalOperatorInFile.LESSTHANOREQUALTO));
    }

    @ParameterizedTest
    @MethodSource("provideExpressionAndExpectedPropertyValue")
    public void should_ExtractCorrectPropertyValue_WhenValidSimpleExpression(String expressionLine,
	    String expectedProperty, String expectedValue) throws ParsingException {
	SimpleBoolExpressionInFile expression = new SimpleBoolExpressionInFile(expressionLine);
	assertEquals(expression.getProperty().getName(), expectedProperty);
	assertEquals(expression.getValue(), expectedValue);
    }

    private static Stream<Arguments> provideExpressionAndExpectedPropertyValue() {
	return Stream.of(Arguments.of("(Heizkoerperserie = Compact Hygiene)", "Heizkoerperserie", "Compact Hygiene"),
		Arguments.of("(Heizkoerperserie != Compact)", "Heizkoerperserie", "Compact"),
		Arguments.of("Baulaenge > 100.5 mm", "Baulaenge", "100.5 mm"),
		Arguments.of("(Baulaenge special >= 100 mm)", "Baulaenge special", "100 mm"),
		Arguments.of("Baulaenge < 100", "Baulaenge", "100"),
		Arguments.of("Baulaenge <= 100", "Baulaenge", "100"));
    }

}
