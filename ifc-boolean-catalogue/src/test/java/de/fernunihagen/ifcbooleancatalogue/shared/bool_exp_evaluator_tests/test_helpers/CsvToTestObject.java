package de.fernunihagen.ifcbooleancatalogue.shared.bool_exp_evaluator_tests.test_helpers;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;

import org.junit.jupiter.params.aggregator.AggregateWith;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
@AggregateWith(TestObjectAggregator.class)
public @interface CsvToTestObject {
}