package de.fernunihagen.ifcbooleancatalogue.shared.bool_exp_evaluator_tests.test_helpers;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.aggregator.ArgumentsAggregator;

public class TestObjectAggregator implements ArgumentsAggregator {
    @Override
    public TestObject aggregateArguments(ArgumentsAccessor arguments, ParameterContext context) {
        String given = arguments.getString(0);
        String when = arguments.getString(1);
        String then = arguments.getString(2);
        String expression = arguments.getString(3);
        
        Map <String, String> values = new HashMap <String, String>();
        values.put("Heizkoerperart", arguments.getString(4));
        values.put("Heizkoerperserie", arguments.getString(5));
        values.put("Farbe", arguments.getString(6));
        values.put("Bautiefe", arguments.getString(7));
	return new TestObject(given, when, then, expression, values);
    }
}