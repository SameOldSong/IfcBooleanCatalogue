package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.expressions.test_helpers;

import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.expressions.LogicalOperatorInFile;

public class TestObject {
    
    private String given;
    private String expression;
    private LogicalOperatorInFile operator;
    private int numCompoundComponents;
    private int numSimpleComponents;
    
    public TestObject(String given, String expression, String operator, String numCompoundComponents, String numSimpleComponents) {
	this.given = given;
	this.expression = expression;
	this.operator = LogicalOperatorInFile.valueOf(operator);
	this.numCompoundComponents = Integer.parseInt(numCompoundComponents);
	this.numSimpleComponents = Integer.parseInt(numSimpleComponents);
    }
    public String getGiven() {
        return given;
    }
  
    public String getExpression() {
        return expression;
    }
    public int getNumCompoundComponents() {
        return numCompoundComponents;
    }
    public int getNumSimpleComponents() {
        return numSimpleComponents;
    }
    public LogicalOperatorInFile getOperator() {
        return operator;
    }

}
