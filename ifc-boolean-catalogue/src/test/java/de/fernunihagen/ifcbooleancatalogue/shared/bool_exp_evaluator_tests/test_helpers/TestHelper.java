package de.fernunihagen.ifcbooleancatalogue.shared.bool_exp_evaluator_tests.test_helpers;

import java.util.ArrayList;
import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.metrics.IfcBenchmarkEnum;
import de.fernunihagen.ifcbooleancatalogue.model.Product;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.catalogue.Dependency;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.catalogue.Dependent;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.catalogue.Trigger;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers.BracketsHelper;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers.OperatorHelper;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers.StringHelper;
import de.fernunihagen.ifcbooleancatalogue.shared.model.AbstractProperty;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractBoolExpression;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractSimpleBoolExpression;

public class TestHelper {

//    public static AbstractSimpleBoolExpression buildSimpleExpression(String exp, String productName) {
//
//	String expression = BracketsHelper.removeBrackets(exp);
//
//	RelationalOperator operator = OperatorHelper.getOperatorFromSimpleExpression(expression);
//
//	String propName = StringHelper.getPropertyFromSimpleExpression(expression);
//	String value = StringHelper.getValueFromSimpleExpression(expression);
//	Product product = new Product(productName);
//	return new AbstractSimpleBoolExpression(new AbstractProperty(propName, product), operator, value);
//    }
//
//    public static AbstractDisjunction buildDisjunction(String[] simpleExpressions, String product) {
//	List<AbstractBoolExpression> components = new ArrayList<AbstractBoolExpression>();
//
//	for (String simple : simpleExpressions)
//	    components.add(buildSimpleExpression(simple, product));
//
//	return new AbstractDisjunction(components);
//    }
//
//    public static void printTriggers(AbstractBoolExpression expression) {
//	for (Trigger trigger : expression.generateTriggers())
//	    System.out.println(trigger);
//    }
//
//    public static void printDependents(AbstractBoolExpression expression) {
//	for (Dependent dependent : expression.generateDependents())
//	    System.out.println(dependent);
//    }
//
//    public static boolean listsEqual(List<? extends Object> list1, List<? extends Object> list2) {
//	if (list1.size() != list2.size())
//	    return false;
//	for (Object object : list1)
//	    if (!list2.contains(object))
//		return false;
//	return true;
//    }
}
