package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.MalformedBoolExpressionException;

public class StepHelperTests {

   
    @Test
    public void shouldTransformSteppedStringToCorrectOrExpression_WhenOneInterval() throws MalformedBoolExpressionException {
	String expression = "(Baulaenge = 40-120 STEP10)";
	String normalized = StepHelper.replaceSteppedIntervalsInExpression(expression);
	String expected = "(Baulaenge = 40 OR 50 OR 60 OR 70 OR 80 OR 90 OR 100 OR 110 OR 120)";
	assertTrue(normalized.equals(expected));
    }

    @Test
    public void shouldTransformSteppedStringToCorrectOrExpression_WhenTwoIntervalsMixedWithNonIntervalValues() throws MalformedBoolExpressionException {
	String expression = "(Baulaenge = 10 40-120 STEP10 OR 140-200 STEP20 OR 230 OR 300)";
	String normalized = StepHelper.replaceSteppedIntervalsInExpression(expression);
	String expected = "(Baulaenge = 10 40 OR 50 OR 60 OR 70 OR 80 OR 90 OR 100 OR 110 OR 120 OR 140 OR 160 OR 180 OR 200 OR 230 OR 300)";
	assertTrue(normalized.equals(expected));
    }
    
    @Test
    public void shouldTransformSteppedStringToCorrectOrExpression_WhenThreeIntervalsWithDifferentSteps() throws MalformedBoolExpressionException {
	String expression = "(Baulaenge = 40-60 STEP10 OR 140-160 STEP20 OR 230-320 STEP30)";
	String normalized = StepHelper.replaceSteppedIntervalsInExpression(expression);
	String expected = "(Baulaenge = 40 OR 50 OR 60 OR 140 OR 160 OR 230 OR 260 OR 290 OR 320)";
	assertTrue(normalized.equals(expected));
    }
}
