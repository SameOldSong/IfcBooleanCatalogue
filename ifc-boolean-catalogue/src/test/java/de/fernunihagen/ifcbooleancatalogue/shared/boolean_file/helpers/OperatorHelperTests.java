package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import de.fernunihagen.ifcbooleancatalogue.model.MissingPropertyException;
import de.fernunihagen.ifcbooleancatalogue.shared.bool_exp_evaluator_tests.test_helpers.CsvToTestObject;
import de.fernunihagen.ifcbooleancatalogue.shared.bool_exp_evaluator_tests.test_helpers.TestObject;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.MalformedBoolExpressionException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers.OperatorHelper;
import de.fernunihagen.ifcbooleancatalogue.test_settings.TestSetUp;

public class OperatorHelperTests extends TestSetUp {

    @CsvFileSource(resources = "/shared/operatorHelper_tests/operator_compound_expr.csv", numLinesToSkip = 1, delimiter = ';')
    @ParameterizedTest(name = "{index} ==> given{0}_when{1}_thenOperator{2}")
    public void test(@CsvToTestObject TestObject test)
	    throws MalformedBoolExpressionException, MissingPropertyException {
	assertTrue(OperatorHelper.getLogicalOperator(test.getExpression()).toString().equals(test.getThen()));
    }
}
