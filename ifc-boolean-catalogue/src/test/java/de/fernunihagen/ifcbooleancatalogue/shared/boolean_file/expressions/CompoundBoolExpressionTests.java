package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.MethodSource;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.test_setup.TestSetUp;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.ParsingException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.expressions.test_helpers.CsvToTestObject;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.expressions.test_helpers.TestObject;

public class CompoundBoolExpressionTests extends TestSetUp {
    @ParameterizedTest
    @MethodSource("provideExpressionAndExpectedOperator")
    public void should_ExtractCorrectOperator_WhenValidSimpleExpression(String expressionLine,
	    LogicalOperatorInFile operator) throws ParsingException {
	CompoundBoolExpressionInFile expression = new CompoundBoolExpressionInFile(expressionLine);
	assertEquals(expression.getOperator(), operator);
    }

    private static Stream<Arguments> provideExpressionAndExpectedOperator() {
	return Stream.of(Arguments.of("(Heizkörperart = Flachheizkörper) OR (Heizkörperart = Elektroheizkörper)",
		LogicalOperatorInFile.OR));
    }

    @CsvFileSource(resources = "/shared/boolean_file/expressions/compound_expr.csv", numLinesToSkip = 1, delimiter = ';')
    @ParameterizedTest(name = "{index} ==> given{0}_then_Operator{2}_NumCompoundComponents{3}_NumSimpleComponents{4}")
    public void testCompoundExpressions(@CsvToTestObject TestObject test) throws ParsingException {
	CompoundBoolExpressionInFile expression = new CompoundBoolExpressionInFile(test.getExpression());

	assertTrue(expression.getOperator().equals(test.getOperator()));
	assertTrue(getNumSimpleComponents(expression) == test.getNumSimpleComponents());
	assertTrue(getNumCompoundComponents(expression) == test.getNumCompoundComponents());
    }

    private int getNumSimpleComponents(CompoundBoolExpressionInFile expression) {
	int numSimple = 0;
	for (BoolExpressionInFile component : expression.getComponents())
	    if (component instanceof SimpleBoolExpressionInFile)
		numSimple++;
	return numSimple;
    }

    private int getNumCompoundComponents(CompoundBoolExpressionInFile expression) {
	return expression.getNumComponents() - getNumSimpleComponents(expression);
    }

}
