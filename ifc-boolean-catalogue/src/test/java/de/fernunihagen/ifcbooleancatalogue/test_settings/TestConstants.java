package de.fernunihagen.ifcbooleancatalogue.test_settings;

public interface TestConstants {

    public final static String RESOURCES_PATH = "src/test/resources/";

    // PURMO
    public final static String PURMO_FOLDER = RESOURCES_PATH + "Purmo/";
    public final static String PURMO_CATALOGUE = PURMO_FOLDER + "CataloguePurmo/";
    public final static String EXPECTED_JSON_FOLDER = PURMO_FOLDER + "expected_json/";
    public final static String DEPENDENCY_MANAGER_FOLDER = EXPECTED_JSON_FOLDER + "DependencyManagerTests/";

   

}
