package de.fernunihagen.ifcbooleancatalogue.integration_tests;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import de.fernunihagen.ifcbooleancatalogue.model.MissingProductException;
import de.fernunihagen.ifcbooleancatalogue.model.MissingPropertyException;
import de.fernunihagen.ifcbooleancatalogue.model.Product;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.FailureWhenParsingExpressionException;
import de.fernunihagen.ifcbooleancatalogue.shared.DependencyManager;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.BoolFileParser;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.MalformedBoolExpressionException;
import de.fernunihagen.ifcbooleancatalogue.shared.model.AbstractProperty;
import de.fernunihagen.ifcbooleancatalogue.shared.model.Constants;
import de.fernunihagen.ifcbooleancatalogue.test_helpers.JsonGenerator;

public class BoolCatalogueTests extends JsonGenerator implements Constants {
    Map<String, List<AbstractProperty>> properties;
    
    DependencyManager dependencyManager;

    @Test
    public void contextLoads() {
    }
  
//    @Test
//    public void testPurmoHeizkoerperArt()
//	    throws MalformedBoolExpressionException, IOException, InvalidUnitException, MissingPropertyException, FailureWhenParsingExpressionException, MissingProductException {
//	File booleanFile = new File(PURMO_FOLDER + "HeizSerieSimple");
//	System.out.println(getJsonFromObject(new CatalogueGenerator(booleanFile).generateCatalogue()));
//	
////	ObjectMapper objectMapper = new ObjectMapper();
////	try(InputStream fileStream = new FileInputStream(PURMO_FOLDER_JSON + "HeizkoerperArt.json")) {
////	    Object list = mapper.readValue(fileStream, MovieList.class);
////	}
//    }
//    
//    @Test
//    public void testPurmoHeizkoerperArtSerie()
//	    throws MalformedBoolExpressionException, IOException, InvalidUnitException, MissingPropertyException, MissingProductException {
//	File booleanFile = new File(PURMO_FOLDER + "HeizSerie");
//	BooleanFileParser parser = new CatalogueGenerator(booleanFile);
//	//parser.generateCatalogueForWeb();
//	//System.out.println(getJson(new BoolCatalogueGenerator(booleanFile).generateCatalogueProperties()));
//
//    }
//    
//    @Test
//    public void testPurmoHeizkoerperBetriebsdruckBetriebsart() throws IOException, InvalidUnitException, MalformedBoolExpressionException, MissingPropertyException, FailureWhenParsingExpressionException, MissingProductException {
//    File booleanFile = new File(PURMO_FOLDER + "Betriebsdruck");
//
//	System.out.println(getJsonFromObject(new CatalogueGenerator(booleanFile).generateCatalogue()));
//    }
//    @Test
//    public void testPurmoHeizkoerperZubehoer() throws IOException, InvalidUnitException, MalformedBoolExpressionException, MissingPropertyException, FailureWhenParsingExpressionException, MissingProductException {
//    File booleanFile = new File(PURMO_FOLDER + "Zubehoer");
//
//	System.out.println(getJsonFromObject(new CatalogueGenerator(booleanFile).generateCatalogue()));
//    }
//    
//    @Test
//    public void testPurmoHeizkoerperAnschlussarten() throws IOException, InvalidUnitException, MalformedBoolExpressionException, MissingPropertyException, FailureWhenParsingExpressionException, MissingProductException {
//    File booleanFile = new File(PURMO_FOLDER + "Anschlussarten");
//
//	System.out.println(getJsonFromObject(new CatalogueGenerator(booleanFile).generateCatalogue()));
//    }
//
//        
//    @Test
//    public void testPurmoHeizkoerperBauhoeheLaenge() throws IOException, InvalidUnitException, MalformedBoolExpressionException, MissingPropertyException, FailureWhenParsingExpressionException, MissingProductException {
//    File booleanFile = new File(PURMO_FOLDER + "HoeheLaenge");
//
//	System.out.println(getJsonFromObject(new CatalogueGenerator(booleanFile).generateCatalogue()));
//    }
//    
//    @Test
//    public void testPurmoHeizkoerperTemperatur() throws IOException, InvalidUnitException, MalformedBoolExpressionException, MissingPropertyException, FailureWhenParsingExpressionException, MissingProductException {
//    File booleanFile = new File(PURMO_FOLDER + "Temperatur");
//
//	System.out.println(getJsonFromObject(new CatalogueGenerator(booleanFile).generateCatalogue()));
//    }


}

    

