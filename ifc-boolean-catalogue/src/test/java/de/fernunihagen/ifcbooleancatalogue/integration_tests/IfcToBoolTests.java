package de.fernunihagen.ifcbooleancatalogue.integration_tests;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.IfcFileGenerator;
import de.fernunihagen.ifcbooleancatalogue.ifc_to_boolean.BoolFileGenerator;
import de.fernunihagen.ifcbooleancatalogue.model.MissingProductException;
import de.fernunihagen.ifcbooleancatalogue.model.MissingPropertyException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.MalformedBoolExpressionException;
import de.fernunihagen.ifcbooleancatalogue.shared.model.Constants;

public class IfcToBoolTests implements Constants{

	@Test
	public void contextLoads() {
	}

	/**
	 * Tests in this class have no asserts, because they generate IFC file, which is
	 * then manually checked in Constructivity
	 * 
	 * @throws MalformedBoolExpressionException
	 * @throws IOException
	 * @throws InvalidUnitException
	 * @throws MissingPropertyException
	 */
	@Test
	public void testPurmoHZArt()
			throws MalformedBoolExpressionException, IOException, MissingPropertyException {
		File booleanFile = new File(PURMO_FOLDER_IFC + "HeizArt");
		new BoolFileGenerator().generateBoolFile(booleanFile, new File(booleanFile.getParent() + File.separator + "results"
				+ File.separator + booleanFile.getName().replace(IFC, "") + TXT));

	}
//	@Test
//	public void testPurmoHZSerie()
//			throws MalformedBoolExpressionException, IOException, InvalidUnitException, MissingPropertyException, MissingProductException {
//		File booleanFile = new File(PURMO_FOLDER + "HeizSerie");
//		new IfcFileGenerator().generateIFC(booleanFile, new File(booleanFile.getParent() + File.separator + "results"
//				+ File.separator + booleanFile.getName().replace(".txt", "") + ".ifc"));
//
//	}
//	@Test
//	public void testPurmoZubehoer()
//			throws MalformedBoolExpressionException, IOException, InvalidUnitException, MissingPropertyException, MissingProductException {
//		File booleanFile = new File(PURMO_FOLDER + "Zubehoer");
//		new IfcFileGenerator().generateIFC(booleanFile, new File(booleanFile.getParent() + File.separator + "results"
//				+ File.separator + booleanFile.getName().replace(".txt", "") + ".ifc"));
//
//	}
//	
//	@Test
//	public void testPurmoBetriebsdruck()
//			throws MalformedBoolExpressionException, IOException, InvalidUnitException, MissingPropertyException, MissingProductException {
//		File booleanFile = new File(PURMO_FOLDER + "Betriebsdruck");
//		new IfcFileGenerator().generateIFC(booleanFile, new File(booleanFile.getParent() + File.separator + "results"
//				+ File.separator + booleanFile.getName().replace(TXT, "") + IFC));
//
//	}
//	
//	@Test
//	public void testPurmoOberflaeche()
//			throws MalformedBoolExpressionException, IOException, InvalidUnitException, MissingPropertyException, MissingProductException {
//		File booleanFile = new File("src/test/resources/Purmo/Oberflaeche");
//		new IfcFileGenerator().generateIFC(booleanFile, new File(booleanFile.getParent() + File.separator + "results"
//				+ File.separator + booleanFile.getName().replace(".txt", "") + ".ifc"));
//
//	}
//	
//	@Test
//	public void testPurmoAnschlussarten()
//			throws MalformedBoolExpressionException, IOException, InvalidUnitException, MissingPropertyException, MissingProductException {
//		File booleanFile = new File("src/test/resources/Purmo/Anschlussarten");
//		new IfcFileGenerator().generateIFC(booleanFile, new File(booleanFile.getParent() + File.separator + "results"
//				+ File.separator + booleanFile.getName().replace(".txt", "") + ".ifc"));
//
//	}
//	
//	@Test
//	public void testPurmoAnbauten()
//			throws MalformedBoolExpressionException, IOException, InvalidUnitException, MissingPropertyException, MissingProductException {
//		File booleanFile = new File("src/test/resources/Purmo/Anbauten");
//		new IfcFileGenerator().generateIFC(booleanFile, new File(booleanFile.getParent() + File.separator + "results"
//				+ File.separator + booleanFile.getName().replace(".txt", "") + ".ifc"));
//
//	}
//	@Test
//	public void testPurmoBauHoeheLaenge()
//			throws MalformedBoolExpressionException, IOException, InvalidUnitException, MissingPropertyException, MissingProductException {
//		File booleanFile = new File("src/test/resources/Purmo/HoeheLaenge");
//		new IfcFileGenerator().generateIFC(booleanFile, new File(booleanFile.getParent() + File.separator + "results"
//				+ File.separator + booleanFile.getName().replace(".txt", "") + ".ifc"));
//
//	}
//	
//	@Test
//	public void testPurmoZumSitzenGeeignet()
//			throws MalformedBoolExpressionException, IOException, InvalidUnitException, MissingPropertyException, MissingProductException {
//		File booleanFile = new File("src/test/resources/Purmo/ZumSitzenGeeignet");
//		new IfcFileGenerator().generateIFC(booleanFile, new File(booleanFile.getParent() + File.separator + "results"
//				+ File.separator + booleanFile.getName().replace(".txt", "") + ".ifc"));
//
//	}
//	
//
//	@Test
//	public void testPurmoTemperatur()
//			throws MalformedBoolExpressionException, IOException, InvalidUnitException, MissingPropertyException, MissingProductException {
//		File booleanFile = new File("src/test/resources/Purmo/Temperatur");
//		new IfcFileGenerator().generateIFC(booleanFile, new File(booleanFile.getParent() + File.separator + "results"
//				+ File.separator + booleanFile.getName().replace(".txt", "") + ".ifc"));
//
//	}
//	
//	@Test
//	public void testPurmoCompleteCatalogue()
//			throws MalformedBoolExpressionException, IOException, InvalidUnitException, MissingPropertyException, MissingProductException {
//		File booleanFile = new File("src/test/resources/Purmo/CataloguePurmo");
//		new IfcFileGenerator().generateIFC(booleanFile, new File(booleanFile.getParent() + File.separator + "results"
//				+ File.separator + booleanFile.getName().replace(".txt", "") + ".ifc"));
//
//	}
//	
//	@Test
//	public void testPurmoArticleNumConstant()
//			throws MalformedBoolExpressionException, IOException, InvalidUnitException, MissingPropertyException, MissingProductException {
//		File booleanFile = new File("src/test/resources/Purmo/ArticleNumConstant");
//		new IfcFileGenerator().generateIFC(booleanFile, new File(booleanFile.getParent() + File.separator + "results"
//				+ File.separator + booleanFile.getName().replace(".txt", "") + ".ifc"));
//
//	}
//	
//	@Test
//	public void testPurmoArticleNumReference()
//			throws MalformedBoolExpressionException, IOException, InvalidUnitException, MissingPropertyException, MissingProductException {
//		File booleanFile = new File("src/test/resources/Purmo/ArticleNumReference");
//		new IfcFileGenerator().generateIFC(booleanFile, new File(booleanFile.getParent() + File.separator + "results"
//				+ File.separator + booleanFile.getName().replace(".txt", "") + ".ifc"));
//
//	}
//	
//	@Test
//	public void testPurmoArticleNumPartial()
//			throws MalformedBoolExpressionException, IOException, InvalidUnitException, MissingPropertyException, MissingProductException {
//		File booleanFile = new File("src/test/resources/Purmo/ArtNumberPartial");
//		new IfcFileGenerator().generateIFC(booleanFile, new File(booleanFile.getParent() + File.separator + "results"
//				+ File.separator + booleanFile.getName().replace(".txt", "") + ".ifc"));
//
//	}
//	
//
//	@Test
//	public void testKermiComplete()
//			throws MalformedBoolExpressionException, IOException, InvalidUnitException, MissingPropertyException, MissingProductException {
//		File booleanFile = new File("src/test/resources/Kermi/KermiComplete");
//		new IfcFileGenerator().generateIFC(booleanFile, new File(booleanFile.getParent() + File.separator + "results"
//				+ File.separator + booleanFile.getName().replace(".txt", "") + ".ifc"));
//
//	}
//	
}
