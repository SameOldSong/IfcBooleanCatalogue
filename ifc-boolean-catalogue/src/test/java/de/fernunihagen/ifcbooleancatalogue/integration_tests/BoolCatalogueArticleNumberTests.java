package de.fernunihagen.ifcbooleancatalogue.integration_tests;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.fernunihagen.ifcbooleancatalogue.model.MissingProductException;
import de.fernunihagen.ifcbooleancatalogue.model.MissingPropertyException;
import de.fernunihagen.ifcbooleancatalogue.model.Product;
import de.fernunihagen.ifcbooleancatalogue.shared.DependencyManager;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.BoolFileParser;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.MalformedBoolExpressionException;
import de.fernunihagen.ifcbooleancatalogue.shared.model.AbstractProperty;
import de.fernunihagen.ifcbooleancatalogue.shared.model.Constants;
import de.fernunihagen.ifcbooleancatalogue.test_helpers.JsonGenerator;


public class BoolCatalogueArticleNumberTests extends JsonGenerator implements Constants {
    Map<String, List<AbstractProperty>> properties;
   
    DependencyManager dependencyManager;
    Product product;


  @BeforeEach
  void setUp() {
      product = new Product("IfcSpaceHeater");
  }
   
    
//    @Test
//    public void testPurmoHZArtSerieWithArtNum()
//	    throws MalformedBoolExpressionException, IOException, InvalidUnitException, MissingPropertyException, MissingProductException {
////	File booleanFile = new File(PURMO_FOLDER_WITH_ART_NUM + "Betriebsdruck");
////	BooleanFileParser parser = new CatalogueGenerator(booleanFile);
////	//parser.generateCatalogueForWeb();
////	
////	Map<String, String> map = new HashMap<>();
////	map.put("Heizkoerperart", "Flachheizkoerper");
////	map.put("Heizkoerperserie", "Compact");
////	map.put("Max. Betriebsdruck", "10");
////	map.put("Betriebsart", "ohne Gebläse");
////	List <Map<String, String>> valueCombinations = new ArrayList<>();
////	valueCombinations.add(map);
////	
////	String artNum = new AbstractArticleNumManager().buildArticleNumber(map, product);
////	System.out.println(artNum);
////	//System.out.println(getJson(new BoolCatalogueGenerator(booleanFile).generateCatalogueProperties()));
//
//    }

    
}
