package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.ifc_objects.ifc_units;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units.IfcConversionBasedUnit;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.test_setup.TestSetUp;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.InvalidUnitException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.ParsingException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.units.ConversionBasedUnitInBoolFile;

public class IfcConversionBasedUnitTests extends TestSetUp {

    @Test
    public void should_CreateCorrectIfcConversionBasedUnit_FromInchToMillimetre()
	    throws ParsingException {
	String expectedUnitBlock = "#10  =  IFCCONVERSIONBASEDUNIT(*, .LENGTHUNIT., 'INCH', #11);\r\n"
		+ "#11  =  IFCMEASUREWITHUNIT(IFCLENGTHMEASURE(25.4), #12);\r\n"
		+ "#12  =  IFCSIUNIT(*, .LENGTHUNIT., .MILLI., .METRE.);";

	IfcConversionBasedUnit unit = new IfcConversionBasedUnit(10,
		new ConversionBasedUnitInBoolFile("IFCLENGTHMEASURE(IFCCONVERSIONBASEDUNIT(INCH, MILLIMETRE, 25.4))"));

	assertEquals(unit.print().trim(), expectedUnitBlock);

    }

    @Test
    public void should_ReturnTrue_IfUnitsEqual() throws InvalidUnitException {
	IfcConversionBasedUnit unit = new IfcConversionBasedUnit(10,
		new ConversionBasedUnitInBoolFile("IFCLENGTHMEASURE(IFCCONVERSIONBASEDUNIT(INCH, MILLIMETRE, 25.4))"));
	
	
	IfcConversionBasedUnit anotherUnit = new IfcConversionBasedUnit(10,
		new ConversionBasedUnitInBoolFile("IFCLENGTHMEASURE(IFCCONVERSIONBASEDUNIT(INCH, MILLIMETRE, 25.4))"));
	assertEquals(unit, anotherUnit);

    }

    @Test
    public void should_ReturnFalse_IfUnitsNotEqual() throws InvalidUnitException {
	IfcConversionBasedUnit unit = new IfcConversionBasedUnit(10,
		new ConversionBasedUnitInBoolFile("IFCLENGTHMEASURE(IFCCONVERSIONBASEDUNIT(INCH, MILLIMETRE, 25.4))"));
	IfcConversionBasedUnit anotherUnit = new IfcConversionBasedUnit(10,
		new ConversionBasedUnitInBoolFile("IFCAREAMEASURE(IFCCONVERSIONBASEDUNIT(SQUARE_FEET, SQUARE_METRE, 25.4))"));

	assertNotEquals(unit, anotherUnit);

    }

}
