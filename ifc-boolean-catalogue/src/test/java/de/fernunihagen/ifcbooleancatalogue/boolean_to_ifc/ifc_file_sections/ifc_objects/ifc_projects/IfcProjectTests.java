package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.ifc_objects.ifc_projects;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;

import org.junit.jupiter.api.Test;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.IfcProject;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.test_setup.TestSetUp;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.LineMissingInBooleanFileException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.ParsingException;

public class IfcProjectTests extends TestSetUp {
    // #1 = IFCPROJECT('Gfzwmps02Kd4igYmbjaDXa', $,
    // 'project_with_one_product_without_units', $, $, $, $, $,
    // $);
    private final String INPUT_FOLDER = getInputFolder();
    private final String EXPECTED_FOLDER = getExpectedFolder();

    @Test
    public void should_CreateCorrectIfcProject_WhenOneProductNoUnitAssigment()
	    throws IOException, LineMissingInBooleanFileException, ParsingException {
	String FILE_NAME = "project_with_one_product_without_units";
	IfcProject project = new IfcProject(1, getFileContent(INPUT_FOLDER + FILE_NAME));
	Pattern expectedPattern = Pattern.compile(getFileContentAsString(EXPECTED_FOLDER + FILE_NAME));
	assertTrue(expectedPattern.matcher(project.print().trim()).matches());
    }

    @Test
    public void should_CreateCorrectIfcProject_WhenOneProductAndUnitAssigmentUnitsOfDiffTypes()
	    throws IOException, LineMissingInBooleanFileException, ParsingException {
	String FILE_NAME = "project_with_one_product_two_units_of_different_types";
	IfcProject project = new IfcProject(1, getFileContent(INPUT_FOLDER + FILE_NAME));
	Pattern expectedPattern = Pattern.compile(getFileContentAsString(EXPECTED_FOLDER + FILE_NAME));
	assertTrue(expectedPattern.matcher(project.print().trim()).matches());
    }

    @Test
    public void should_CreateCorrectIfcProject_WhenTwoProductsAndUnitAssigment()
	    throws IOException, LineMissingInBooleanFileException, ParsingException {
	String FILE_NAME = "project_with_two_products_units_of_different_types";
	IfcProject project = new IfcProject(1, getFileContent(INPUT_FOLDER + FILE_NAME));
	Pattern expectedPattern = Pattern.compile(getFileContentAsString(EXPECTED_FOLDER + FILE_NAME));
	assertTrue(expectedPattern.matcher(project.print().trim()).matches());
    }

    ////// FOLDERS ///////////
    private String getExpectedFolder() {
	return getIfcProjectsFolder() + "expected" + File.separator;
    }

    private String getInputFolder() {
	return getIfcProjectsFolder() + "input" + File.separator;
    }

    private String getIfcProjectsFolder() {
	return IFC_OBJECTS_FOLDER
		+ File.separator + "ifc_projects" + File.separator + "ifc_project" + File.separator;
    }

}
