package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.regex.Pattern;

import org.junit.jupiter.api.Test;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.IfcFileHeaderSection;
import de.fernunihagen.ifcbooleancatalogue.test_helpers.TestConstants;

public class IfcFileHeaderSectionTests implements TestConstants {
    private final String IFC_FILE_NAME = "some_radiator.ifc";

    @Test
    public void testIfcFileHeaderCorrectlyCreated_WhenIfcFileNameProvided() {
	IfcFileHeaderSection header = new IfcFileHeaderSection(IFC_FILE_NAME);

	Pattern pattern = Pattern.compile(expectedHeaderPattern());

	assertTrue(pattern.matcher(header.getLines().trim()).matches());

    }

//	ISO-10303-21;
//	HEADER;
//	FILE_DESCRIPTION($,'2;1');
//	FILE_NAME('some_radiator.ifc','2020-05-28T15:30:59',(''),(''),'','manual','');
//	FILE_SCHEMA(('IFC4'));
//	ENDSEC;
    private String expectedHeaderPattern() {
	// 2020-05-28T14:08:33
	String RANDOM_TIMESTAMP = "\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}.*";

	String header = "ISO-10303-21;" + NEW_LINE_REG_EXP;
	header += "HEADER;" + NEW_LINE_REG_EXP;
	header += "FILE_DESCRIPTION\\(\\$,'2;1'\\);" + NEW_LINE_REG_EXP;
	header += "FILE_NAME\\('" + IFC_FILE_NAME + "','" + RANDOM_TIMESTAMP;

	header += "',\\(''\\),\\(''\\),'','manual',''\\);" + NEW_LINE_REG_EXP;
	header += "FILE_SCHEMA\\(\\('" + IFC_FILE_SCHEMA + "'\\)\\);" + NEW_LINE_REG_EXP;

	header += "ENDSEC;";

	return header;
    }

}
