package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.ifc_objects.ifc_properties;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.properties.IfcPropertySingleValue;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.test_setup.TestSetUp;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.ParsingException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.properties.Property;

public class IfcPropertySingleValueTests extends TestSetUp {

    @Test
    public void should_CreateCorrectPropertySingleValue_FromPropertyLineWithoutUnitAndPredefinedValue()
	    throws ParsingException {
	// property in file
	Property propertyInFile = new Property("Heizkoerperserie : IFCLABEL");
	propertyInFile.addValue(getTestValue(propertyInFile));

	// ifc property
	IfcPropertySingleValue property = new IfcPropertySingleValue(104, propertyInFile);
	String expected = "#104  =  IFCPROPERTYSINGLEVALUE('Heizkoerperserie', $, IFCLABEL('test value'), $);";
	assertEquals(expected, property.print().trim());
    }

    @Test
    public void should_CreateCorrectPropertySingleValue_FromPropertyLineWithLocalConversionUnitAndPredefinedValue()
	    throws ParsingException {
	// property in file
	Property propertyInFile = new Property(
		"Max. Betriebsdruck : IFCPRESSUREMEASURE(IFCCONVERSIONBASEDUNIT(BAR, PASCAL, 100000))");
	propertyInFile.addValue(getTestValue(propertyInFile));
	propertyInFile.getUnit().setGlobal(false);

	// ifc property
	IfcPropertySingleValue property = new IfcPropertySingleValue(99, propertyInFile);
	String expected = "#99  =  IFCPROPERTYSINGLEVALUE('Max. Betriebsdruck', $, IFCPRESSUREMEASURE(10.), #100);\r\n"
		+ "#100  =  IFCCONVERSIONBASEDUNIT(*, .PRESSUREUNIT., 'BAR', #101);\r\n"
		+ "#101  =  IFCMEASUREWITHUNIT(IFCPRESSUREMEASURE(100000.0), #102);\r\n"
		+ "#102  =  IFCSIUNIT(*, .PRESSUREUNIT., $, .PASCAL.);";
	assertEquals(expected, property.print().trim());
    }

    @Test
    public void should_CreateCorrectPropertySingleValue_FromPropertyLineWithLocalSiUnitAndPredefinedValue()
	    throws ParsingException {
	// property in file
	Property propertyInFile = new Property("Bauhoehe : IFCLENGTHMEASURE(MILLIMETRE)");
	propertyInFile.addValue(getTestValue(propertyInFile));
	propertyInFile.getUnit().setGlobal(false);

	// ifc property
	IfcPropertySingleValue property = new IfcPropertySingleValue(3, propertyInFile);
	String expected = "#3  =  IFCPROPERTYSINGLEVALUE('Bauhoehe', $, IFCLENGTHMEASURE(195.5), #4);\r\n"
		+ "#4  =  IFCSIUNIT(*, .LENGTHUNIT., .MILLI., .METRE.);";

	assertEquals(expected, property.print().trim());
    }
}
