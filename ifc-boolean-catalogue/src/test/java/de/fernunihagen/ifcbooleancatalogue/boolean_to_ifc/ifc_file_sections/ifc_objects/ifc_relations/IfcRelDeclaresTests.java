package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.ifc_objects.ifc_relations;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.Arrays;
import java.util.regex.Pattern;

import org.junit.jupiter.api.Test;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.relations.IfcRelDeclares;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.test_setup.TestSetUp;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.LineMissingInBooleanFileException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.ParsingException;

public class IfcRelDeclaresTests extends TestSetUp {
    private static final String RELATION_NAME = "proxy to products relation";
    @Test
    public void should_CreateCorrectIfcRelDeclares_WhenOneRelatingAndSeveralRelated()
	    throws IOException, LineMissingInBooleanFileException, ParsingException {
	
	//relation
	IfcRelDeclares relation = new IfcRelDeclares(5, 3, Arrays.asList(10, 20, 30));
	relation.setName(RELATION_NAME);
	
	//expected pattern
	String patternString = "#5  =  IFCRELDECLARES\\('[a-zA-Z0-9]{22}', \\$, '" + RELATION_NAME + "', \\$, #3, \\(#10, #20, #30\\)\\);";
	Pattern expectedPattern = Pattern.compile(patternString);
	
	assertTrue(expectedPattern.matcher(relation.print().trim()).matches());
    }
    
    @Test
    public void should_CreateCorrectIfcRelDeclares_WhenOneRelatingAndOneRelated()
	    throws IOException, LineMissingInBooleanFileException, ParsingException {
	
	//relation
	IfcRelDeclares relation = new IfcRelDeclares(5, 3, Arrays.asList(10));
	relation.setName(RELATION_NAME);
	
	//expected pattern
	String patternString = "#5  =  IFCRELDECLARES\\('[a-zA-Z0-9]{22}', \\$, '" + RELATION_NAME + "', \\$, #3, \\(#10\\)\\);";
	Pattern expectedPattern = Pattern.compile(patternString);
	
	assertTrue(expectedPattern.matcher(relation.print().trim()).matches());
    }

}
