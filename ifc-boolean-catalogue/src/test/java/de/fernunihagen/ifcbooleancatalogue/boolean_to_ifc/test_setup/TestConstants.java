package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.test_setup;

import java.io.File;

public interface TestConstants {
    // paths
    public final static String RESOURCES_PATH = "src/test/resources/";
    public final static String BOOL_TO_IFC_FOLDER = RESOURCES_PATH + "boolean_to_ifc/";
    public final static String IFC_OBJECTS_FOLDER = BOOL_TO_IFC_FOLDER + "ifc_file_sections" + File.separator + "ifc_objects";

    public final String DOUBLE_NEW_LINE = "\n\n";
    public final String NEW_LINE = "\n";
    public final String CARRIAGE_RETURN = "\r";
    public final String NEW_LINE_REG_EXP = "\\R";
    final String IFC_FILE_SCHEMA = "IFC4";

}
