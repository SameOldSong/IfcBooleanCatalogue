package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.ifc_objects.ifc_units;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.Test;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units.IfcUnitAssignment;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.test_setup.TestSetUp;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.BoolFileContent;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.InvalidUnitException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.LineMissingInBooleanFileException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.ParsingException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.units.UnitInBoolFile;

public class IfcUnitAssignmentTests extends TestSetUp {

    private final String INPUT_FOLDER = getInputFolder();
    private final String EXPECTED_FOLDER = getExpectedFolder();

    @Test
    public void should_CreateCorrectIfcUnitAssignment_WhenOneProductAndOneIfcSiUnit()
	    throws IOException, LineMissingInBooleanFileException, ParsingException {
	String testFile = "project_with_one_product_one_si_unit";

	List<UnitInBoolFile> unitsInFile = initUnitsFromFile(getInputFile(testFile));

	IfcUnitAssignment unitAssignment = new IfcUnitAssignment(5, unitsInFile);

	assertEquals(unitAssignment.print().trim(), getFileContentAsString(getExpectedFile(testFile)));
    }

    @Test
    public void should_CreateCorrectIfcUnitAssignment_WhenOneProductAndOneIfcConversionBasedUnit()
	    throws IOException, LineMissingInBooleanFileException, ParsingException {
	String testFile = "project_with_one_product_one_conversion_unit";

	List<UnitInBoolFile> unitsInFile = initUnitsFromFile(getInputFile(testFile));

	IfcUnitAssignment unitAssignment = new IfcUnitAssignment(5, unitsInFile);

	assertEquals(unitAssignment.print().trim(), getFileContentAsString(getExpectedFile(testFile)));
    }

    @Test
    public void should_CreateCorrectIfcUnitAssignmentWithOnlyFirstUnit_WhenOneProductAndTwoUnitsOfSameType()
	    throws IOException, LineMissingInBooleanFileException, ParsingException {
	String testFile = "project_with_one_product_two_units_of_same_type";

	List<UnitInBoolFile> unitsInFile = initUnitsFromFile(getInputFile(testFile));

	IfcUnitAssignment unitAssignment = new IfcUnitAssignment(5, unitsInFile);

	assertEquals(unitAssignment.print().trim(), getFileContentAsString(getExpectedFile(testFile)));
    }

    @Test
    public void should_CreateCorrectIfcUnitAssignmentWithTwoUnits_WhenOneProductAndTwoUnitsOfDifferentTypes()
	    throws IOException, LineMissingInBooleanFileException, ParsingException {
	String testFile = "project_with_one_product_two_units_of_different_types";

	List<UnitInBoolFile> unitsInFile = initUnitsFromFile(getInputFile(testFile));

	IfcUnitAssignment unitAssignment = new IfcUnitAssignment(5, unitsInFile);

	assertEquals(unitAssignment.print().trim(), getFileContentAsString(getExpectedFile(testFile)));
    }

    @Test
    public void should_CreateCorrectIfcUnitAssignmentWithOnlyDistinctUnits_WhenTwoProductsWithOverlappingUnitTypes()
	    throws IOException, LineMissingInBooleanFileException, ParsingException {
	String testFile = "project_with_two_products_units_of_different_types";

	List<UnitInBoolFile> unitsInFile = initUnitsFromFile(getInputFile(testFile));

	IfcUnitAssignment unitAssignment = new IfcUnitAssignment(5, unitsInFile);
	assertEquals(unitAssignment.print().trim(), getFileContentAsString(getExpectedFile(testFile)));
    }

    /////////////// HELPERS //////////////////////

    private List<UnitInBoolFile> initUnitsFromFile(String path) throws IOException, ParsingException {
	return new BoolFileContent(new File(path)).getGlobalUnits();
    }

    ///// FOLDERS //////
    private String getExpectedFolder() {
	return getUnitAssignmentFolder() + "expected" + File.separator;
    }

    private String getInputFolder() {
	return getUnitAssignmentFolder() + "input" + File.separator;
    }

    private String getUnitAssignmentFolder() {
	return IFC_OBJECTS_FOLDER + File.separator
		+ "ifc_unit_assignment" + File.separator;
    }

    private String getInputFile(String fileName) {
	return INPUT_FOLDER + fileName;
    }

    private String getExpectedFile(String fileName) {
	return EXPECTED_FOLDER + fileName;
    }
}
