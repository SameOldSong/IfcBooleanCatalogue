package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.ifc_objects.ifc_units;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units.IfcSiUnit;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.test_setup.TestSetUp;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.InvalidUnitException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.ParsingException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.units.SiUnitInBoolFile;

public class IfcSiUnitTests extends TestSetUp {

    @Test
    public void should_CreateCorrectIfcSIUnit_FromValueTypeAndUnitWithoutPrefix() throws ParsingException {
	String expectedUnit = "#10  =  IFCSIUNIT(*, .THERMODYNAMICTEMPERATUREUNIT., $, .DEGREE_CELSIUS.);";
	IfcSiUnit unit = new IfcSiUnit(10, new SiUnitInBoolFile("IFCTHERMODYNAMICTEMPERATUREMEASURE(DEGREE_CELSIUS)"));
	assertEquals(expectedUnit, unit.print().trim());
    }

    @Test
    public void should_CreateCorrectIfcSIUnit_FromValueTypeAndUnitWithPrefix() throws ParsingException {
	String expectedUnit = "#3  =  IFCSIUNIT(*, .LENGTHUNIT., .CENTI., .METRE.);";
	IfcSiUnit unit = new IfcSiUnit(3, new SiUnitInBoolFile("IFCLENGTHMEASURE(CENTIMETRE)"));
	assertEquals(expectedUnit, unit.print().trim());

    }

    @Test
    void should_ThrowException_IfUnitDoesntMatchType() {

	Assertions.assertThrows(InvalidUnitException.class, () -> {
	    new IfcSiUnit(3, new SiUnitInBoolFile("IFCTHERMODYNAMICTEMPERATUREMEASURE(CENTIMETRE)"));
	});
    }

    @Test
    public void should_ReturnTrue_IfUnitsEqual() throws InvalidUnitException {
	IfcSiUnit unit = new IfcSiUnit(3, new SiUnitInBoolFile("IFCLENGTHMEASURE(CENTIMETRE)"));
	IfcSiUnit anotherUnit = new IfcSiUnit(6, new SiUnitInBoolFile("IFCLENGTHMEASURE(CENTIMETRE)"));
	assertEquals(unit, anotherUnit);

    }

    @Test
    public void should_ReturnFalse_IfUnitsNotEqual() throws InvalidUnitException {
	IfcSiUnit unit = new IfcSiUnit(6, new SiUnitInBoolFile("IFCLENGTHMEASURE(CENTIMETRE)"));
	IfcSiUnit anotherUnit = new IfcSiUnit(6, new SiUnitInBoolFile("IFCLENGTHMEASURE(METRE)"));
	assertNotEquals(unit, anotherUnit);

    }

}
