package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.test_setup;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.BoolFileContent;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.ParsingException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.properties.Property;

public class TestSetUp implements TestConstants {

    protected BoolFileContent getFileContent(String path)
	    throws IOException, ParsingException {
	return new BoolFileContent(new File(path));
    }

    protected String getFileContentAsString(String path) throws IOException {

	byte[] encoded = Files.readAllBytes(Paths.get(path));
	return new String(encoded, Charset.forName("UTF-8"));
    }

    protected void setTestValuesToProperties(List<Property> properties) {

	properties.stream().forEach(property -> property.addValue(getTestValue(property)));
    }

    protected String getTestValue(Property property) {
	switch (property.getMeasureType()) {
	case "IFCLABEL":
	    return "test value";
	case "IFCPRESSUREMEASURE":
	    return "10";
	case "IFCLENGTHMEASURE":
	    return "195.5";
	case "IFCBOOLEAN":
	    return "TRUE";
	}
	return "";
    }
}
