package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.ifc_objects.ifc_projects;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;

import org.junit.jupiter.api.Test;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.IfcProjectLibrary;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.test_setup.TestSetUp;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.LineMissingInBooleanFileException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.ParsingException;

public class IfcProjectLibraryTests extends TestSetUp {
    // #3 = IFCPROJECTLIBRARY('k2tanFnCs5uvUqTP3gBlLw', $, $, $, $, $, $, $, $);

    // values to properties are not set here; this will be verified in integration
    // tests of higher level (IfcFileDataSection) and in dedicated unit tests
    // (IfcPropertySingleValue)

    @Test
    public void should_CreateCorrectIfcProjectLibrary_WhenOneProductNoUnitAssigment()
	    throws IOException, LineMissingInBooleanFileException, ParsingException {
	String FILE_NAME = "project_with_one_product_without_units";
	IfcProjectLibrary library = new IfcProjectLibrary(3, getFileContent(getInputFile(FILE_NAME)));
	Pattern expectedPattern = Pattern.compile(getFileContentAsString(getExpectedFile(FILE_NAME)));
	assertTrue(expectedPattern.matcher(library.print().trim()).matches());
    }

    // references to global units are not set in these tests; this will be verified
    // in IfcFileDataSection
    @Test
    public void should_CreateCorrectIfcProjectLibrary_WhenOneProductUnitsOfDifferentTypes()
	    throws IOException, LineMissingInBooleanFileException, ParsingException {
	String FILE_NAME = "project_with_one_product_two_units_of_different_types";
	IfcProjectLibrary library = new IfcProjectLibrary(3, getFileContent(getInputFile(FILE_NAME)));
	Pattern expectedPattern = Pattern.compile(getFileContentAsString(getExpectedFile(FILE_NAME)));
	System.out.println(library.print().trim());
	assertTrue(expectedPattern.matcher(library.print().trim()).matches());
    }

 // references to global units are not set in these tests; this will be verified
    // in IfcFileDataSection
    @Test
    public void should_CreateCorrectIfcProjectLibrary_WhenTwoProductsUnitsOfDifferentTypes()
	    throws IOException, LineMissingInBooleanFileException, ParsingException {
	String FILE_NAME = "project_with_two_products_units_of_different_types";
	IfcProjectLibrary library = new IfcProjectLibrary(3, getFileContent(getInputFile(FILE_NAME)));
	Pattern expectedPattern = Pattern.compile(getFileContentAsString(getExpectedFile(FILE_NAME)));
	assertTrue(expectedPattern.matcher(library.print().trim()).matches());
    }

    ////// FOLDERS ///////////
    private String getExpectedFolder() {
	return getIfcProjectsFolder() + "expected" + File.separator;
    }

    private String getExpectedFile(String fileName) {
	return getExpectedFolder() + File.separator + fileName;
    }

    private String getInputFile(String fileName) {
	return getInputFolder() + File.separator + fileName;
    }

    private String getInputFolder() {
	return getIfcProjectsFolder() + "input" + File.separator;
    }

    private String getIfcProjectsFolder() {
	return IFC_OBJECTS_FOLDER + File.separator + "ifc_projects" + File.separator + "ifc_library" + File.separator;
    }

}
