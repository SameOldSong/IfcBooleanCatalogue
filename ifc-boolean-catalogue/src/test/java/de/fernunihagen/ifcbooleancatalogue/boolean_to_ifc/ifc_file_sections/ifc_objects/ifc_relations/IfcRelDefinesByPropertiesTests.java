package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.ifc_objects.ifc_relations;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.Arrays;
import java.util.regex.Pattern;

import org.junit.jupiter.api.Test;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.relations.IfcRelDefinesByProperties;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.test_setup.TestSetUp;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.LineMissingInBooleanFileException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.ParsingException;

public class IfcRelDefinesByPropertiesTests extends TestSetUp {
    private final String REL_DESC = "'links property set to products'";

    @Test
    public void should_CreateCorrectIfcRelDefinesByProperties_WhenSeveralProperties()
	    throws IOException, LineMissingInBooleanFileException, ParsingException {

	// relation
	IfcRelDefinesByProperties relation = new IfcRelDefinesByProperties(5, Arrays.asList(10, 20, 30), 3);

	// expected pattern
	String patternString = "#5  =  IFCRELDEFINESBYPROPERTIES\\('[a-zA-Z0-9]{22}', \\$, \\$, " + REL_DESC
		+ ", \\(#10, #20, #30\\), #3\\);";
	Pattern expectedPattern = Pattern.compile(patternString);

	assertTrue(expectedPattern.matcher(relation.print().trim()).matches());
    }

    @Test
    public void should_CreateCorrectIfcRelDeclares_WhenOneProperty()
	    throws IOException, LineMissingInBooleanFileException, ParsingException {

	// relation
	IfcRelDefinesByProperties relation = new IfcRelDefinesByProperties(5, Arrays.asList(30), 3);

	// expected pattern
	String patternString = "#5  =  IFCRELDEFINESBYPROPERTIES\\('[a-zA-Z0-9]{22}', \\$, \\$, " + REL_DESC
		+ ", \\(#30\\), #3\\);";
	Pattern expectedPattern = Pattern.compile(patternString);

	assertTrue(expectedPattern.matcher(relation.print().trim()).matches());
    }

}
