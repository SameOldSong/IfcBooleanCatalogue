package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.ifc_objects.ifc_products;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;

import org.junit.jupiter.api.Test;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.IfcProxy;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.test_setup.TestSetUp;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.BoolFileContent;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.LineMissingInBooleanFileException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.ParsingException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.products.ProductInFile;

public class IfcProxyTests extends TestSetUp {
    private final String INPUT_FOLDER = getInputFolder();

    // references to global units are not set in this test case, this will be
    // verified in IfcFileDataSection integration tests
    @Test
    public void should_CreateCorrectProxyProduct_FromMultiplePropertiesWithAndWithoutUnitsOfDifferentUnitTypes()
	    throws ParsingException, IOException, LineMissingInBooleanFileException {
	String testFile = "product_WithMultipleProperties_withAndWithoutUnits_ofDifferentTypes";
	ProductInFile productInFile = initProductFromFile(getInputFile(testFile));
	setTestValuesToProperties(productInFile.getProperties());
	IfcProxy proxy = new IfcProxy(102, productInFile);
	Pattern expectedPattern = Pattern.compile(getFileContentAsString(getExpectedFile(testFile)));
	assertTrue(expectedPattern.matcher(proxy.print().trim()).matches());
    }

    private ProductInFile initProductFromFile(String inputFile)
	    throws IOException, ParsingException {
	return new BoolFileContent(new File(inputFile)).getProducts().get(0);
    }

    ///// FOLDERS //////
    private String getExpectedFolder() {
	return getIfcPropertiesFolder() + "expected" + File.separator;
    }

    private String getInputFolder() {
	return getIfcPropertiesFolder() + "input" + File.separator;
    }

    private String getIfcPropertiesFolder() {
	return IFC_OBJECTS_FOLDER + File.separator + "ifc_products" + File.separator;
    }

    private String getInputFile(String fileName) {
	return INPUT_FOLDER + fileName;
    }

    private String getExpectedFile(String fileName) {
	return getExpectedFolder() + fileName;
    }
}
