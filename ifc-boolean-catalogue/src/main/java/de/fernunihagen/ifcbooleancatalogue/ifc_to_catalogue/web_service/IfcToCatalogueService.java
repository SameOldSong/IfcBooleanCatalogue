package de.fernunihagen.ifcbooleancatalogue.ifc_to_catalogue.web_service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class IfcToCatalogueService {
	private static final String FILE_DIRECTORY = "C:\\Users\\Tatiana\\Desktop\\testcopy";
	 
	public String storeFile(MultipartFile file) throws IOException {
		DateTimeFormatter FOMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy_hhmm");
		
		String timeStamp = FOMATTER.format(LocalDateTime.now());
		String newFileName = file.getOriginalFilename().replace(".txt", "") + timeStamp + ".ifc";
		
		Path filePath = Paths.get(FILE_DIRECTORY + File.separator + newFileName);
		
		Files.copy(file.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
		return newFileName;
	}
	
	public String getFileFor(String fileName) {
		return FILE_DIRECTORY + File.separator + fileName;
	}
}
