package de.fernunihagen.ifcbooleancatalogue.ifc_to_catalogue.web_controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import de.fernunihagen.ifcbooleancatalogue.ifc_to_catalogue.web_service.IfcToCatalogueService;
import de.fernunihagen.ifcbooleancatalogue.model.Value;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class IfcToCatalogueController {

	private final IfcToCatalogueService fileService;

	@Autowired
	public IfcToCatalogueController(IfcToCatalogueService fileService) {
		this.fileService = fileService;
	}

	@PostMapping(value = "/uploadIfcToCatalogue")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<String> handleFileUpload(@RequestParam("file") MultipartFile file) {
		System.out.print("COPY");
		String message = "";
		try {
			String fileId = fileService.storeFile(file);

			return ResponseEntity.status(HttpStatus.OK).body(fileId);
		} catch (Exception e) {
			message = "FAIL to upload " + file.getOriginalFilename() + "!";
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
		}
	}

	
	@RequestMapping(value = "/catalogueIfc", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, ArrayList <Value>> getCatalogue() {
		HashMap<String, ArrayList<Value>> map = new HashMap<>();
//
//		ArrayList<Value> values = new ArrayList<Value>();
//		// Heizkörperart
//		// ArrayList <HashMap<String, ArrayList<String>>> dependents;
//		Map<String, String> mymap = new HashMap<String, String>() {
//			{
//				put("Heizkörperserie", "Compact|Compact Hygiene");
//				put("Bauhöhe", "200|300");
//				put("Zubehör", "FZ-Halterung|Bohrkonsolen-Set 160 mm");
//			}
//		};
//
//		values.add(new Value("Flachheizkörper", mymap));
//
//		mymap = new HashMap<String, String>() {
//			{
//				put("Heizkörperserie", "Ventil Compact|Ventil Compact Hygiene");
//				put("Bauhöhe", "300|400");
//				put("Zubehör", "Bohrkonsolen-Set 160 mm|Bohrkonsolen-Set 200 mm");
//			}
//		};
//
//		values.add(new Value("Badheizkörper", mymap));
//		map.put("Heizkörperart", values);
//
//		// Heizkörperserie
//		values = new ArrayList<Value>();
//		mymap = new HashMap<String, String>() {
//			{
//
//				put("Bauhöhe", "300");
//				put("Zubehör", "Bohrkonsolen-Set 160 mm");
//			}
//		};
//		values.add(new Value("Compact", mymap));
//
//		mymap = new HashMap<String, String>() {
//			{
//
//				put("Bauhöhe", "400");
//				put("Zubehör", "Bohrkonsolen-Set 200 mm");
//			}
//		};
//		values.add(new Value("Compact Hygiene", mymap));
//
//		mymap = new HashMap<String, String>() {
//			{
//
//				put("Bauhöhe", "300");
//				put("Zubehör", "Bohrkonsolen-Set 160 mm");
//			}
//		};
//		values.add(new Value("Ventil Compact", mymap));
//
//		mymap = new HashMap<String, String>() {
//			{
//
//				put("Bauhöhe", "400");
//				put("Zubehör", "Bohrkonsolen-Set 200 mm");
//			}
//		};
//		values.add(new Value("Ventil Compact Hygiene", mymap));
//
//		map.put("Heizkörperserie", values);
//
//		// Bauhöhe
//		values = new ArrayList<Value>();
//
//		values.addAll(new ArrayList<Value>(Arrays.asList(new Value("200"), new Value("300"), new Value("400"))));
//
//		map.put("Bauhöhe", values);
//		
//		
//		// Zubehör
//		values = new ArrayList<Value>();
//
//		values.addAll(new ArrayList<Value>(Arrays.asList(new Value("FZ-Halterung"), new Value("Bohrkonsolen-Set 160 mm"), new Value("Bohrkonsolen-Set 200 mm"))));
//
//		map.put("Zubehör", values);
//		return map;
		return map;
	}

}
