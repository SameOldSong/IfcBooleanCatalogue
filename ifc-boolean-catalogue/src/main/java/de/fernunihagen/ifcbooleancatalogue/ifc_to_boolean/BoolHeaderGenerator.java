package de.fernunihagen.ifcbooleancatalogue.ifc_to_boolean;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.shared.model.Constants;

public class BoolHeaderGenerator implements Constants{
	private String fileName;
	//private ArrayList<IfcUnit> globalUnits;

//	public BoolHeaderGenerator(String fileName, ArrayList<IfcUnit> globalUnits) {
//		this.fileName = fileName;
//	//	this.globalUnits = globalUnits;
//	}

	public String buildIfcHeader(List <Integer> proxyRefs) {
		String header = "ISO-10303-21;\n";
		header += "HEADER;\n";
		header += "FILE_DESCRIPTION($,'2;1');\n";

		String timestamp = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss").format(LocalDateTime.now());
		header += "FILE_NAME(" + "'" + fileName + "'" + "," + "'" + timestamp + "'" + ",(''),(''),'','manual','');\n";
		header += "FILE_SCHEMA(('IFC4'));\n";
		header += "ENDSEC;\n\n";

		header += "DATA;\n\n";
		header += buildDataHeader(proxyRefs);

		return header;
	}

	private String buildDataHeader(List <Integer> proxyRefs) {

//		// #1
//		IfcProject project = new IfcProject(fileName);
//
//		// #2
//		ArrayList <Integer> refs = new ArrayList <Integer>();
//		refs.add(new Integer(3));
//		IfcRelDeclares projectToLibraryRel = new IfcRelDeclares(2, "project-library relation", 1, refs);
//
//		// #3
//		IfcProjectLibrary library = new IfcProjectLibrary();
//
//		// #100 reserved for Dummy Object
//		IfcRelDeclares productToLibraryRel = new IfcRelDeclares(4, "product-library relation", 3, proxyRefs);
//
//		// #5 if units exist, create unitAssignment
//		String unitAssignmenBlock = "";
//		if (globalUnits.size() > 0) {
//			IfcUnitAssignment unitAssignment = new IfcUnitAssignment(globalUnits);
//			project.setUnitAssignment("#" + unitAssignment.getLineIndex());
//			unitAssignmenBlock = unitAssignment.createIfcBlock();
//
//		}

//		// assemble all lines into header
//		String header = project.toIfcLine();
//		header += projectToLibraryRel.toIfcLine();
//		header += library.toIfcLine();
//		header += productToLibraryRel.toIfcLine();
//		header += unitAssignmenBlock;
//
//		header += LINE_SEPARATOR;
//		header += LINE_SEPARATOR;
//
//		return header;
	    
	    return null;
	}

}