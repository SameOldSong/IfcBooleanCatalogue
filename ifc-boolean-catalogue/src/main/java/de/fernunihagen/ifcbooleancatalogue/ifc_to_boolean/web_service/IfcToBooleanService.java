package de.fernunihagen.ifcbooleancatalogue.ifc_to_boolean.web_service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.IfcFileGenerator;
import de.fernunihagen.ifcbooleancatalogue.ifc_to_boolean.BoolFileGenerator;
import de.fernunihagen.ifcbooleancatalogue.model.MissingPropertyException;
import de.fernunihagen.ifcbooleancatalogue.shared.WebService;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.MalformedBoolExpressionException;
import de.fernunihagen.ifcbooleancatalogue.shared.model.Constants;

@Component
public class IfcToBooleanService extends WebService implements Constants{
	 
    public String storeFile(MultipartFile file) throws IOException {

	String timeStamp = FOMATTER.format(LocalDateTime.now());
	String newFileName = file.getOriginalFilename().replace(TXT, "") + timeStamp + IFC;

	Path filePath = Paths.get(FILE_DIRECTORY + File.separator + newFileName);

	Files.copy(file.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
	return newFileName;
    }

    public String getFileFor(String fileName) {
	return FILE_DIRECTORY + File.separator + fileName;
    }

    public String convertToIfc(MultipartFile file)
	    throws IOException, MalformedBoolExpressionException, MissingPropertyException {
	//TO FIX
	return null;
	
//	String timeStamp = FOMATTER.format(LocalDateTime.now());
//	String newFileName = file.getOriginalFilename().replace(TXT, "") + timeStamp + IFC;
//
//	new BoolFileGenerator().generateBoolFile(convertMultiPartToFile(file), new File(newFileName));
//	return newFileName;
    }
}
