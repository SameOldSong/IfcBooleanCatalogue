package de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.objectives.LogicalOperator;
import de.fernunihagen.ifcbooleancatalogue.model.Value;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.ListOfExpressions;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.MalformedBoolExpressionException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers.BracketsHelper;

public abstract class AbstractCompoundBoolExpression implements AbstractBoolExpression {
    protected LogicalOperator operator;

    public LogicalOperator getOperator() {
	return operator;
    }

    @Override
    public abstract List<? extends AbstractBoolExpression> getComponents();

    public List<String> getValuesAsListOfStrings() {
	List<String> values = new ArrayList<>();

	for (AbstractBoolExpression expression : getComponents())
	    if (expression instanceof AbstractSimpleBoolExpression)
		values.add(((AbstractSimpleBoolExpression) expression).getValueAsText());
	    else
		values.addAll(((AbstractCompoundBoolExpression) expression).getValuesAsListOfStrings());
	return values;
    }

    public List<String> getPropertiesAsListOfStrings() {
	List<String> properties = new ArrayList<>();

	for (AbstractBoolExpression expression : getComponents())
	    if (expression instanceof AbstractSimpleBoolExpression)
		properties.add(((AbstractSimpleBoolExpression) expression).getPropertyName());
	    else
		properties.addAll(((AbstractCompoundBoolExpression) expression).getPropertiesAsListOfStrings());
	return properties;
    }

    @Override
    public boolean equals(Object anotherExpression) {
	if (!(anotherExpression instanceof AbstractCompoundBoolExpression))
	    return false;

	for (AbstractBoolExpression component : getComponents())
	    if (component.equals(anotherExpression))
		return true;

	return false;
    }

    @Override
    public ArrayList<Value> getValues() {
	ArrayList<Value> values = new ArrayList<Value>();

	for (AbstractBoolExpression component : getComponents())
	    values.addAll(component.getValues());

	return values;
    }

    protected boolean allComponentsSimpleExpressions() {

	for (AbstractBoolExpression component : getComponents())
	    if (!(component instanceof AbstractSimpleBoolExpression))
		return false;
	return true;
    }

    protected boolean allComponentsHaveSameProperty() {

	return new ListOfExpressions(getComponents()).allExpressionsHaveSameProperty();
    }

    @Override
    public String toString() {

	ArrayList<String> stringComponents = new ArrayList<String>();
	for (AbstractBoolExpression component : getComponents())
	    stringComponents.add(component.toString());

	return concatComponents(stringComponents, getOperator());
    }

    private String concatComponents(ArrayList<String> stringComponents, LogicalOperator operator) {

	String result = stringComponents.stream()
		.map(component -> addBracketsIfNeeded(component) + BLANK + operator + BLANK)
		.collect(Collectors.joining());

	// remove redundant logical operator after the last component
	result = result.substring(0, result.lastIndexOf(CLOSE_BRACKET) + 1);

	return result;
    }

    protected String addBracketsIfNeeded(String expression) {
	if (expression.startsWith(NOT))
	    return expression;

	int closingBracketIndex;
	try {
	    closingBracketIndex = BracketsHelper.getClosingBracketIndex(expression, 0);
	    if (closingBracketIndex == expression.length() - 1)
		return expression;
	} catch (MalformedBoolExpressionException e) {

	    e.printStackTrace();
	}

	return OPEN_BRACKET + expression + CLOSE_BRACKET;
    }

    public abstract void setComponents(List<AbstractBoolExpression> components);

}
