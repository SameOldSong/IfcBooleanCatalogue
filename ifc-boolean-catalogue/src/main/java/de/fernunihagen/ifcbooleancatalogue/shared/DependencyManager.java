package de.fernunihagen.ifcbooleancatalogue.shared;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.metrics.IfcBenchmarkEnum;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.objectives.LogicalOperator;
import de.fernunihagen.ifcbooleancatalogue.model.Product;
import de.fernunihagen.ifcbooleancatalogue.model.Value;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.Equivalence;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.FailureWhenParsingExpressionException;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.Implication;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.ListOfExpressions;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.catalogue.Dependency;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.catalogue.Dependent;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.catalogue.Trigger;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.MalformedBoolExpressionException;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractBoolExpression;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractCompoundBoolExpression;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractNegation;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractSimpleBoolExpression;

/**
 * responsible for generating dependencies between property values
 * 
 * @author TatianaNikolaeva
 *
 */
public class DependencyManager {

    /**
     * builds dependencies between property values based on boolean expressions and
     * attaches them to Value objects
     * 
     * @param map
     * @throws JsonProcessingException
     * @throws MalformedBoolExpressionException
     * @throws FailureWhenParsingExpressionException
     */
//    public void addDependenciesToProducts(AbstractCatalogue listOfProducts)
//	    throws JsonProcessingException, MalformedBoolExpressionException, FailureWhenParsingExpressionException {
//
//	for (Product product : listOfProducts.getProducts()) { 
//	    List<Dependency> dependencies = generateDependenciesForProduct(product);
//	    product.setDependencies(dependencies);
//	}
//    }

    public List<Dependency> generateDependenciesForProduct(Product product) throws MalformedBoolExpressionException, FailureWhenParsingExpressionException {

	List<Dependency> dependencies = new ArrayList<Dependency>();
	for (AbstractBoolExpression expression : product.getExpressions()) {
	    List<Dependency> newDependencies = createDependencies(expression);
	    addNewDependencies(dependencies, newDependencies);
	}
	

	return dependencies;

    }

    private List<Dependency> createDependencies(AbstractBoolExpression expression)
	    throws MalformedBoolExpressionException, FailureWhenParsingExpressionException {
	if (expression instanceof Implication)
	    return createDependenciesFromImplication((Implication) expression);

	if (expression instanceof Equivalence) {
	    List<Dependency> dependencies = createDependenciesFromImplication(
		    ((Equivalence) expression).getLeftToRightImplication());

	    dependencies
		    .addAll(createDependenciesFromImplication(((Equivalence) expression).getRightToLeftImplication()));
	    return dependencies;

	}

	return new ArrayList<Dependency>();
    }

    

    private List<Dependency> createDependenciesFromImplication(Implication implication)
	    throws MalformedBoolExpressionException, FailureWhenParsingExpressionException {
	List<Dependency> dependencies = new ArrayList<Dependency>();
	
	// TO FIX
//	List<Trigger> triggersList = implication.getLeftPart().generateTriggers();
//	List<Dependent> dependents = implication.getRightPart().generateDependents();

//	for (Trigger trigger : triggersList)
//	    dependencies.add(new Dependency(trigger, dependents));

	return dependencies;
    }

    private void addNewDependencies(List<Dependency> dependencies, List<Dependency> newDependencies) {
	for (Dependency dependency : newDependencies) {

	    Dependency existingDependencyWithSameTriggers = getDependencyWithSameTrigger(dependency, dependencies);
	    if (existingDependencyWithSameTriggers == null)
		dependencies.add(dependency);
	    else
		existingDependencyWithSameTriggers.addDependents(dependency.getDependents());
	}
    }

    private Dependency getDependencyWithSameTrigger(Dependency dependency, List<Dependency> dependencies) {
	for (Dependency existingDep : dependencies)
	    if (existingDep.hasSameTrigger(dependency))
		return existingDep;
	return null;
    }

}
