package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions;

import java.util.List;

public class LineMissingInBooleanFileException extends ParsingException {
    public LineMissingInBooleanFileException(String string, String fileSection) {
	super("Line containing " + string + " is missing in the file section " + fileSection);
    }

    public LineMissingInBooleanFileException(String string, List<String> fileSection) {
	super("Line containing " + string + " is missing in the file section " + fileSection);
    }
}
