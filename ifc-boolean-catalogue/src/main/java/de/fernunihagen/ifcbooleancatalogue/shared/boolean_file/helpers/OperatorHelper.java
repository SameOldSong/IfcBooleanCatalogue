package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers;

import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.MalformedBoolExpressionException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.expressions.LogicalOperatorInFile;
import de.fernunihagen.ifcbooleancatalogue.shared.model.Constants;

public class OperatorHelper implements Constants {

    public static LogicalOperatorInFile getLogicalOperator(String exp) throws MalformedBoolExpressionException {
        String expression = BracketsHelper.removeRedundantBrackets(exp.trim());
    
        if (expression.startsWith(NOT))
            return OperatorHelper.getLogicalOperatorInExpressionWithNegation(expression);
    
        // find the closing bracket of the first component
        // logical operator comes right after this closing bracket
        int openingBracketIndex = 0;
        int closingBracketIndex = BracketsHelper.getClosingBracketIndex(expression, openingBracketIndex);
    
        return OperatorHelper.extractLogicalOperatorBetweenComponents(closingBracketIndex, expression);
    
    }

    static LogicalOperatorInFile getLogicalOperatorInExpressionWithNegation(String expression)
            throws MalformedBoolExpressionException {
        int closingBracketIndexForNotComponent = BracketsHelper.getClosingBracketIndex(expression, 3);
    
        if (closingBracketIndexForNotComponent == expression.length() - 1)
            return LogicalOperatorInFile.NOT;
        return OperatorHelper.extractLogicalOperatorBetweenComponents(closingBracketIndexForNotComponent, expression);
    }

    // extracts logical operator that comes after the closing bracket of the first
    // component
    static LogicalOperatorInFile extractLogicalOperatorBetweenComponents(int closingBracketIndex, String expression) {
        int index = closingBracketIndex + 1;
        while (index < expression.length() && !Character.isLetter(expression.charAt(index)))
            index++;
        int startIndex = index;
    
        while (index < expression.length() && Character.isLetter(expression.charAt(index)))
            index++;
    
        return LogicalOperatorInFile.valueOf(expression.substring(startIndex, index).trim());
    }

   
    
    

}
