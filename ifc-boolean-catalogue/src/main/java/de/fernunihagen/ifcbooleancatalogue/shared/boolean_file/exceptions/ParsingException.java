package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions;

public class ParsingException extends Exception {
	protected String message;
	public ParsingException(String message){
		
		this.message = message;
	}
}
