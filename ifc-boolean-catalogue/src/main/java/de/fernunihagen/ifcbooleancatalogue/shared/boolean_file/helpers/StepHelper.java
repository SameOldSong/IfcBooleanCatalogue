package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.MalformedBoolExpressionException;
import de.fernunihagen.ifcbooleancatalogue.shared.model.Constants;

public class StepHelper implements Constants {
    private static final String STEP = "STEP";

    // 40-120 STEP10
    private static final String STEP_PATTERN = "\\d+\\-\\d+\\s+STEP{1}\\d+";

    public static String replaceSteppedIntervalsInExpression(String expression)
	    throws MalformedBoolExpressionException {
	if (!expression.contains(STEP))
	    return expression;

	for (String stepSubstring : getAllMatches(expression))
	    expression = expression.replace(stepSubstring, normalizedStepString(stepSubstring));

	return expression;
    }

    private static String normalizedStepString(String stepSubstring) throws MalformedBoolExpressionException {

	// 40-120 STEP10
	int intervalStart = Integer.parseInt(stepSubstring.split(HYPHEN)[0].trim());
	int intervalEnd = Integer.parseInt(stepSubstring.split(HYPHEN)[1].split(BLANK)[0].trim());
	int step = Integer.parseInt(stepSubstring.substring(stepSubstring.indexOf(STEP)).replace(STEP, ""));

	String normalized = Integer.toString(intervalStart) + BLANK + OR + BLANK;
	while (intervalStart < intervalEnd)
	    normalized += Integer.toString(intervalStart += step) + BLANK + OR + BLANK;
	
	return removeLastOr(normalized);
    }

    private static String removeLastOr(String line) {
	return line.trim().substring(0, line.trim().length() - 2).trim();
    }

    private static List<String> getAllMatches(String text) {
	List<String> matches = new ArrayList<String>();
	Matcher m = Pattern.compile(STEP_PATTERN).matcher(text);
	while (m.find()) {
	    matches.add(m.group());
	}
	return matches;
    }

}
