package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions;

public class MissingOperatorInSimpleExpressionException extends ParsingException {

    public MissingOperatorInSimpleExpressionException(String expression) {
	super("Failed to find operator in expression: " + expression);
    }
	
}
