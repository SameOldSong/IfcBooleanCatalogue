package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.units;

import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers.BoolLineHelper;

public class ConversionBasedUnitInBoolFile extends UnitInBoolFile {
    // IFCPRESSUREMEASURE(IFCCONVERSIONBASEDUNIT(BAR, PASCAL, 100000))

    public ConversionBasedUnitInBoolFile(String unitInPropertyLine) {
	super(unitInPropertyLine);

    }

    // 100000
    public String getConversionFactor() {
	return getComponentsInBrackets().split(COMMA)[2].trim();
    }

    public SiUnitInBoolFile getSiUnit() {
	return new SiUnitInBoolFile(measureType, getBaseUnit());
    }

    // PASCAL
    public String getBaseUnit() {
	return getComponentsInBrackets().split(COMMA)[1].trim();
    }

    // BAR
    public String getConversionUnit() {
	return getComponentsInBrackets().split(COMMA)[0].trim();
    }

    // IFCCONVERSIONBASEDUNIT(BAR, PASCAL, 100000)
    private String getComponentsInBrackets() {
	int afterOpenBracket = BoolLineHelper.getFirstOpenBracketIndex(unitName) + 1;
	int beforeClosingBracket = unitName.length() - 1;
	return unitName.substring(afterOpenBracket, beforeClosingBracket).trim();
    }

    @Override
    public boolean equals(Object anotherUnit) {
	if (!conversionBasedUnit(anotherUnit))
	    return false;
	if (differentConversionUnit((ConversionBasedUnitInBoolFile) anotherUnit))
	    return false;

	if (differentBaseUnit((ConversionBasedUnitInBoolFile) anotherUnit))
	    return false;

	if (differentConversionFactor((ConversionBasedUnitInBoolFile) anotherUnit))
	    return false;

	if (differentSiUnit((ConversionBasedUnitInBoolFile) anotherUnit))
	    return false;
	return true;
    }

    private boolean conversionBasedUnit(Object anotherUnit) {
	return anotherUnit instanceof ConversionBasedUnitInBoolFile;
    }

    private boolean differentSiUnit(ConversionBasedUnitInBoolFile anotherUnit) {
	return getSiUnit().equals(anotherUnit.getSiUnit());
    }

    private boolean differentConversionFactor(ConversionBasedUnitInBoolFile anotherUnit) {
	return getConversionFactor().equals(anotherUnit.getConversionFactor());
    }

    private boolean differentBaseUnit(ConversionBasedUnitInBoolFile anotherUnit) {
	return getBaseUnit().equals(anotherUnit.getBaseUnit());
    }

    private boolean differentConversionUnit(ConversionBasedUnitInBoolFile anotherUnit) {
	return getConversionUnit().equals(anotherUnit.getConversionUnit());
    }
}
