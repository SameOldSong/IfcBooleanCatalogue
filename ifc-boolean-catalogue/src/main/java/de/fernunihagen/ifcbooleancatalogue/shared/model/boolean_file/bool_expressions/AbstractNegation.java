package de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions;

import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.objectives.LogicalOperator;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.MalformedBoolExpressionException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers.BracketsHelper;

public class AbstractNegation extends AbstractCompoundBoolExpression {
    private AbstractBoolExpression positivePart;

    public AbstractNegation(AbstractBoolExpression positivePart) {
	this.operator = LogicalOperator.NOT;

	this.positivePart = positivePart;
	addComponent(positivePart);

    }

    public AbstractBoolExpression getPositivePart() {
	return positivePart;
    }
    @Override
    public String toString() {
	return "NOT" + positivePart;
    }

    @Override
    public void addComponent(AbstractBoolExpression component) {
	// TODO Auto-generated method stub
	
    }

    @Override
    public List<? extends AbstractBoolExpression> getComponents() {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public void setComponents(List<AbstractBoolExpression> components) {
	// TODO Auto-generated method stub
	
    }
    
    

}
