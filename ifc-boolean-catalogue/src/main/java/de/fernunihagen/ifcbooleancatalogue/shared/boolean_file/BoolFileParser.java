package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.LineMissingInBooleanFileException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers.BoolLineHelper;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers.StringHelper;
import de.fernunihagen.ifcbooleancatalogue.shared.model.Constants;

public class BoolFileParser extends StringHelper implements Constants {

    public static int getNumOfHeaders(List<String> fileLines) throws LineMissingInBooleanFileException {
	Map<String, Long> counts = fileLines.stream().collect(Collectors.groupingBy(e -> e, Collectors.counting()));
	if (counts.get(HEADER_START) == 0)
	    throw new LineMissingInBooleanFileException(HEADER_START, fileLines);
	return Math.toIntExact(counts.get(HEADER_START));
    }

    public static List<String> getCurProductLines(List<String> fileLines, int headerIndex) {
	int headerLineIndex = getLineIndexOf(fileLines, HEADER_START, headerIndex);
	int endHeaderLineIndex = getLineIndexOf(fileLines, HEADER_START, headerLineIndex + 1);
	return fileLines.subList(headerLineIndex, endHeaderLineIndex);
    }

    private static int getLineIndexOf(List<String> fileLines, String searchString, int occurence) {
	int lineIndex = 0;
	int headerCounter = 0;

	while (lineIndex < fileLines.size() && headerCounter != occurence) {

	    if (fileLines.get(lineIndex).equals(searchString))
		headerCounter++;
	    lineIndex++;
	}
	return lineIndex;
    }

    public static List<String> getLines(File file) throws IOException {
	return Files.readAllLines(file.toPath());
    }

    public static List<String> extractHeaderFromLines(List<String> lines) throws LineMissingInBooleanFileException {
	if (lines.indexOf(HEADER_END) == -1)
	    throw new LineMissingInBooleanFileException(HEADER_END, lines);
	return BoolLineHelper.removeEmptyAndComments(lines.subList(1, lines.indexOf(HEADER_END)));

    }

    public static List<String> extractUnitsFromHeader(List<String> header) {
	return header.stream().filter(line -> line.contains(OPEN_BRACKET)).map(line -> line.split(COLON)[1].trim())
		.collect(Collectors.toList());
    }

    public static List<String> extractPropertiesFromHeader(List<String> productLines)
	    throws LineMissingInBooleanFileException {
	List<String> header = extractHeaderFromLines(productLines);
	return header.stream().filter(line -> BoolLineHelper.isPropertyLine(line)).collect(Collectors.toList());
    }

    public static List<String> getValuesOfPropertyFromBooleanExpressions(String property) {
	return null;
    }

    public static List<String> extractBoolExpressionsFromLines(List<String> lines) {
	List<String> filteredLines = BoolLineHelper
		.removeEmptyAndComments(lines.subList(lines.indexOf(HEADER_END) + 1, lines.size()));
	List<String> expressions = new ArrayList<String>();
	int index = 0;
	while (index < filteredLines.size()) {
	    List<String> expressionParts = BoolLineHelper.collectExpressionParts(filteredLines, index);
	    index += expressionParts.size();
	    expressions.add(BoolLineHelper.mergeExpressionParts(expressionParts));
	}
	return expressions;
    }

    public static String getRandomValueForPropertyFromExpressions(String name, List<String> expressions) {
	// TODO Auto-generated method stub
	return null;
    }

}
