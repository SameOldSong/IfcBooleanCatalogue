package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions;

public class MissingPropertyException extends ParsingException {

    public MissingPropertyException(String message) {
	super(message);
    }

    public MissingPropertyException(String property, String expression) {
	super("Property [" + property + "] in boolean expression [" + expression + "] is not declared in header");
    }
}
