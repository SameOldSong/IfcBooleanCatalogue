package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.units;

public class DerivedUnitInBoolFile extends UnitInBoolFile {

    public DerivedUnitInBoolFile(String propertyLine) {
	super(propertyLine);
    }
    
    @Override
    public boolean equals(Object anotherUnit) {
	if (!derivedUnit(anotherUnit))
	    return false;
	return super.equals(anotherUnit);
    }

    private boolean derivedUnit(Object anotherUnit) {
	return anotherUnit instanceof DerivedUnitInBoolFile;
    }

}
