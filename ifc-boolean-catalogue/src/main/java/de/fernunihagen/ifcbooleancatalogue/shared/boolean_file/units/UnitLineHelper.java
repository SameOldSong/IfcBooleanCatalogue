package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.units;

import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers.BoolLineHelper;
import de.fernunihagen.ifcbooleancatalogue.shared.model.Constants;

public class UnitLineHelper implements Constants {

    private static final String IFCDERIVEDUNIT = "IFCDERIVEDUNIT";
    private static final String IFCCONVERSIONBASEDUNIT = "IFCCONVERSIONBASEDUNIT";

    public static UnitInBoolFile createUnit(String propertyLine) {
	if (isDerivedUnit(propertyLine))
	    return new DerivedUnitInBoolFile(getUnitFromPropertyLine(propertyLine));

	if (isConversionBasedUnit(propertyLine))
	    return new ConversionBasedUnitInBoolFile(getUnitFromPropertyLine(propertyLine));

	if (isSiUnit(propertyLine))
	    return new SiUnitInBoolFile(getUnitFromPropertyLine(propertyLine));

	return null;
    }

    private static String getUnitFromPropertyLine(String propertyLine) {
	return propertyLine.split(COLON)[1].trim();
    }

    private static boolean isDerivedUnit(String propertyLine) {
	return propertyLine.contains(IFCDERIVEDUNIT);
    }

    private static boolean isSiUnit(String propertyLine) {
	return propertyLine.contains(OPEN_BRACKET);
    }

    public static boolean isConversionBasedUnit(String propertyLine) {

	return propertyLine.contains(IFCCONVERSIONBASEDUNIT);
    }

    /**
     * extract IFCLENGTHMEASURE from Bauhöhe : IFCLENGTHMEASURE(MILLIMETRE) or
     * IFCLABEL from Heizkörperserie : IFCLABEL
     * 
     * @param propertyLine
     * @return
     */
    public static String getMeasureTypeFromPropertyLine(String propertyLine) {
        int endIndex;
        if (propertyLine.contains(OPEN_BRACKET))
            endIndex = BoolLineHelper.getFirstOpenBracketIndex(propertyLine);
        else
            endIndex = propertyLine.length();
        return propertyLine.substring(BoolLineHelper.getFirstColonIndex(propertyLine) + 1, endIndex).trim();
    }

    /**
     * extract MILLIMETRE from Bauhöhe : IFCLENGTHMEASURE(MILLIMETRE)
     * 
     * @param propertyLine
     * @return
     */
    public static String getUnitNameFromPropertyLine(String propertyLine) {
        if (!propertyLine.contains(OPEN_BRACKET))
            return null;
        return propertyLine.substring(BoolLineHelper.getFirstOpenBracketIndex(propertyLine) + 1, propertyLine.trim().length() - 1)
        	.trim();
    }

    /**
     * extract IFCLENGTHMEASURE(MILLIMETRE) from Bauhöhe :
     * IFCLENGTHMEASURE(MILLIMETRE)
     * 
     * @param propertyLine
     * @return
     */
    public static String getMeasureTypeAndUnitFromPropertyLine(String propertyLine) {
        if (!propertyLine.contains(OPEN_BRACKET))
            return null;
        return propertyLine.split(COLON)[1].trim();
    }
}
