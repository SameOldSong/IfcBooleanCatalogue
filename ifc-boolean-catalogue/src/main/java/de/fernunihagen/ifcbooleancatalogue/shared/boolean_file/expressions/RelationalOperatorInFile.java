package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.expressions;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.InvalidOperatorException;

public enum RelationalOperatorInFile {
    EQUALTO("="), NOTEQUALTO("!="), GREATERTHAN(">"), GREATERTHANOREQUALTO(">="), LESSTHAN("<"), LESSTHANOREQUALTO("<=");

    String symbolInFile;

    private RelationalOperatorInFile(String symbolInFile) {
	this.symbolInFile = symbolInFile;
    }

    public String getSymbolInFile() {
	return symbolInFile;
    }

    public static RelationalOperatorInFile getOperator(String symbolInFile) throws InvalidOperatorException {
	for (RelationalOperatorInFile operator : RelationalOperatorInFile.class.getEnumConstants())
	    if (operator.getSymbolInFile().equals(symbolInFile))
		return operator;
	throw new InvalidOperatorException(symbolInFile);
    }

    public static List<String> getSymbolsOfOperators() {
	List<? extends Enum> constants = Arrays.asList(RelationalOperatorInFile.class.getEnumConstants());
	return constants.stream().map(constant -> ((RelationalOperatorInFile) constant).getSymbolInFile())
		.collect(Collectors.toList());
    }
}
