package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions;

public class InvalidOperatorException extends ParsingException {

	public InvalidOperatorException(String operator){
		
		super("This operator " + operator + " is invalid.");
	}
}
