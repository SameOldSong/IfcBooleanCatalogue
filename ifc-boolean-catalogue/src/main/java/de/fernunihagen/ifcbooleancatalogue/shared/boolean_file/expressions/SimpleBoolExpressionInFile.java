package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.expressions;

import java.util.Arrays;
import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.MissingOperatorInSimpleExpressionException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.ParsingException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers.BracketsHelper;

public class SimpleBoolExpressionInFile extends BoolExpressionInFile {
    private RelationalOperatorInFile operator;
    private PropertyInBoolExpression property;
    private String value;

    public SimpleBoolExpressionInFile(String expressionLine) throws ParsingException {

	operator = getOperator(expressionLine);
	String propertyName = getProperty(BracketsHelper.removeBrackets(expressionLine));
	value = getValue(BracketsHelper.removeBrackets(expressionLine));

	property = new PropertyInBoolExpression(propertyName, value);
    }

    private String getProperty(String expression) throws ParsingException {
	return BracketsHelper.removeBrackets(expression).split(getOperator(expression).getSymbolInFile())[0].trim();
    }

    private String getValue(String expression) throws ParsingException {
	return BracketsHelper.removeBrackets(expression).split(getOperator(expression).getSymbolInFile())[1].trim();
    }

    public RelationalOperatorInFile getOperator() {
	return operator;
    }

    private static RelationalOperatorInFile getOperator(String expression) throws ParsingException {
	for (String operatorSymbol : RelationalOperatorInFile.getSymbolsOfOperators())
	    if (expression.contains(BLANK + operatorSymbol + BLANK))
		return RelationalOperatorInFile.getOperator(operatorSymbol);

	throw new MissingOperatorInSimpleExpressionException(expression);
    }

    public PropertyInBoolExpression getProperty() {
	return property;
    }

    public String getValue() {
	return value;
    }

    @Override
    public List<BoolExpressionInFile> getComponents() {
	return Arrays.asList(this);
    }

    public String toString() {
	return property.getName() + " " + operator.getSymbolInFile() + " " + getValue();
    }

    @Override
    public List<PropertyInBoolExpression> getProperties() {
	return Arrays.asList(property);
    }
}
