package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.expressions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.MalformedBoolExpressionException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.ParsingException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers.BracketsHelper;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers.OperatorHelper;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers.StepHelper;

public class CompoundBoolExpressionInFile extends BoolExpressionInFile {
    private LogicalOperatorInFile operator;

    public CompoundBoolExpressionInFile(String expression) throws ParsingException {
	String normalizedExpr = normalizeExpressionLine(expression.trim());
	operator = OperatorHelper.getLogicalOperator(normalizedExpr);
	components = buildComponents(normalizedExpr);
    }

    public CompoundBoolExpressionInFile(List<BoolExpressionInFile> components, LogicalOperatorInFile operator)
	    throws ParsingException {

	this.operator = operator;
	this.components = components;
    }

    private String normalizeExpressionLine(String expression) throws ParsingException {
	String normalized = BoolExpressionHelper.normalizeExprWithMultipleOrAndSingleProperty(expression);
	normalized = StepHelper.replaceSteppedIntervalsInExpression(normalized);
	normalized = BoolExpressionHelper.transformExpressionIfImplicationOrEquivalence(normalized);
	return normalized;
    }

    private List<BoolExpressionInFile> buildComponents(String expressionString) throws ParsingException {
	String expression = removeRedundantBrackets(expressionString);

	List<BoolExpressionInFile> components = new ArrayList<>();
	for (String component : extractComponentsFromLine(expression))
	    components.add(BoolExpressionHelper.createExpression(component));

	return components;
    }

    private String removeRedundantBrackets(String expressionString) throws MalformedBoolExpressionException {
	return BracketsHelper.removeRedundantBrackets(expressionString);
    }

    private List<String> extractComponentsFromLine(String expressionString) throws MalformedBoolExpressionException {
	if (operatorIsNot(expressionString))
	    return removeNotFromLine(expressionString);
	return extractComponentsFromLineIfOperatorOtherThanNot(expressionString);
    }

    private List<String> removeNotFromLine(String expressionLine) {
	return Arrays.asList(expressionLine.replace("NOT", "").trim());
    }

    private boolean operatorIsNot(String expressionLine) throws MalformedBoolExpressionException {
	return OperatorHelper.getLogicalOperator(expressionLine).equals(LogicalOperatorInFile.NOT);
    }

    private List<String> extractComponentsFromLineIfOperatorOtherThanNot(String expressionLine)
	    throws MalformedBoolExpressionException {
	List<String> components = new ArrayList<>();

	int currentIndex = 0;
	while (currentIndex >= 0 && currentIndex < expressionLine.length()) {
	    String component = extractComponent(expressionLine.substring(currentIndex));
	    components.add(component);
	    currentIndex = expressionLine.indexOf(OPEN_BRACKET, currentIndex + component.length());
	}
	return components;
    }

    private String extractComponent(String expressionLine) throws MalformedBoolExpressionException {

	if (operatorNotBeforeOpenBracket(expressionLine))
	    return extractComponentWithNot(expressionLine);
	return extractComponentWithoutNot(expressionLine);
    }

    private String extractComponentWithoutNot(String expressionLine) throws MalformedBoolExpressionException {
	int openBracket = expressionLine.indexOf(OPEN_BRACKET);
	int closingBracket = BracketsHelper.getClosingBracketIndex(expressionLine, openBracket);
	return expressionLine.substring(openBracket + 1, closingBracket);
    }

    private String extractComponentWithNot(String expressionLine) throws MalformedBoolExpressionException {
	int openBracket = expressionLine.indexOf(OPEN_BRACKET);
	int closingBracket = BracketsHelper.getClosingBracketIndex(expressionLine, openBracket);
	return NOT + expressionLine.substring(openBracket, closingBracket + 1);
    }

    private boolean operatorNotBeforeOpenBracket(String expressionLine) {
	int openBracket = expressionLine.indexOf(OPEN_BRACKET);
	if (openBracket == 0)
	    return false;
	return expressionLine.substring(openBracket - 3, openBracket).equalsIgnoreCase("NOT");
    }

    public int getNumComponents() {
	return components.size();
    }

    @Override
    public List<PropertyInBoolExpression> getProperties() {

	List<PropertyInBoolExpression> properties = new ArrayList<>();
	for (BoolExpressionInFile component : components)
	    properties.addAll(component.getProperties());
	return properties;
    }

    public LogicalOperatorInFile getOperator() {
	return operator;
    }
}
