package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers;

import java.util.Arrays;

public class EnumHelper {
    public static String[] getEnumNames(Class<? extends Enum<?>> e) {
	return Arrays.stream(e.getEnumConstants()).map(Enum::name).toArray(String[]::new);
    }

}
