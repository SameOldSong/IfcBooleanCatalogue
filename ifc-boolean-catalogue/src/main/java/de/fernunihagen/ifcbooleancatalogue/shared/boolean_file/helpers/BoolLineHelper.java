package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.LineMissingInBooleanFileException;
import de.fernunihagen.ifcbooleancatalogue.shared.model.Constants;

public class BoolLineHelper implements Constants {
    private static final String PRODUCT = "Produkt";
    private static final String MANUFACTURER = "Hersteller";

    public static String extractProductFromHeader(List<String> lines) throws LineMissingInBooleanFileException {
	Optional<String> productLine = lines.stream().filter(line -> line.contains(PRODUCT)).findFirst();
	if (!productLine.isPresent())
	    throw new LineMissingInBooleanFileException(PRODUCT, lines);
	return productLine.toString().split(COLON)[1].replace(CLOSE_BOX_BRACKET, "").trim();
    }

    public static List<String> removeEmptyAndComments(List<String> list) {
	return list.stream().filter(line -> (!isEmpty(line) && !isComment(line))).collect(Collectors.toList());
    }

    public static String mergeExpressionParts(List<String> parts) {
	return parts.stream().collect(Collectors.joining());
    }

    public static List<String> collectExpressionParts(List<String> lines, int index) {
	if (multilineExpression(lines.get(index)))
	    return collectMultipleLines(lines, index);
	else
	    return Arrays.asList(lines.get(index).trim());
    }

    private static List<String> collectMultipleLines(List<String> lines, int index) {
	List<String> parts = new ArrayList<String>();

	// first part starts with {
	String line = lines.get(index).substring(1);

	while (index < lines.size() && !line.trim().endsWith(MULTILINE_END)) {
	    parts.add(line.trim() + BLANK);
	    line = lines.get(++index);
	}
	// remove closing ] from the last part
	parts.add(line.trim().replace(MULTILINE_END, ""));

	return parts;
    }

    private static boolean multilineExpression(String line) {
	return line.startsWith(MULTILINE_START);
    }

    private static boolean isComment(String line) {
	return line.trim().startsWith(HASH);
    }

    private static boolean isEmpty(String line) {
	return line.trim().length() == 0;
    }

    public static int getFirstColonIndex(String value) {
	return value.indexOf(COLON);
    }

    public static int getFirstOpenBracketIndex(String value) {
	return value.indexOf(OPEN_BRACKET);
    }

    public static int getFirstCommaIndex(String value) {
	return value.indexOf(COMMA);
    }

    /**
     * extract Bauhöhe from Bauhöhe : IFCLENGTHMEASURE(MILLIMETRE)
     * 
     * @param propertyLine
     * @return
     */
    public static String getPropertyNameFromPropertyLine(String propertyLine) {
	return propertyLine.substring(0, getFirstColonIndex(propertyLine)).trim();
    }

    public static boolean isPropertyLine(String line) {
	if (line.contains(MANUFACTURER))
	    return false;
	if (line.contains(PRODUCT))
	    return false;
	if (line.contains(HASH))
	    return false;
	if (!line.contains(COLON))
	    return false;

	return true;
    }

}
