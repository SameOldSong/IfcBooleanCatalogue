package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions;

public class MalformedBoolExpressionException extends ParsingException {

    public MalformedBoolExpressionException(String expression) {
	super("Check brackets in this expression: " + expression);
    }
	
}
