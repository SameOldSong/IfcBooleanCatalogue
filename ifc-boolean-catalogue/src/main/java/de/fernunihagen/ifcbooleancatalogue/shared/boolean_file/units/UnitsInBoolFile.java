package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.units;

import java.util.ArrayList;
import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.properties.Property;

public class UnitsInBoolFile {
    private List<UnitInBoolFile> unitsInFile = new ArrayList<>();
    private List<UnitInBoolFile> globalUnitsInFile = new ArrayList<>();

    public int getNumUnits() {
	return getUnits().size();
    }

    public List<UnitInBoolFile> getGlobalUnits() {
	return globalUnitsInFile;
    }

    private boolean thisMeasureTypeDoesNotExistInGlobalUnits(String unitType) {
	for (UnitInBoolFile unit : globalUnitsInFile)
	    if (unit.getMeasureType().equals(unitType))
		return false;
	return true;
    }

    public List<UnitInBoolFile> getUnits() {
	return unitsInFile;
    }

    public void addUnitToGlobalUnits(UnitInBoolFile unit) {
	if (sameUnitExistsInGlobalUnits(unit))
	    unit.setGlobal(true);
	else
	    addNewGlobalUnitIfNewMeasureType(unit);

    }

    private void addNewGlobalUnitIfNewMeasureType(UnitInBoolFile unit) {
	if (thisMeasureTypeDoesNotExistInGlobalUnits(unit.getMeasureType())) {
	    globalUnitsInFile.add(unit);
	    unit.setGlobal(true);
	}
    }

    private boolean sameUnitExistsInGlobalUnits(UnitInBoolFile unit) {
	for (UnitInBoolFile existingUnit : globalUnitsInFile)
	    if (existingUnit.equals(unit))
		return true;
	return false;
    }

    public void addUnits(List<Property> propertiesWithUnits) {
	propertiesWithUnits.stream().forEach(property -> unitsInFile.add(property.getUnit()));

	propertiesWithUnits.stream().forEach(property -> addUnitToGlobalUnits(property.getUnit()));
    }

    public boolean unitsExist() {
	return getNumUnits() > 0;
    }
}
