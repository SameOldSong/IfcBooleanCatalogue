package de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.metrics.IfcBenchmarkEnum;
import de.fernunihagen.ifcbooleancatalogue.model.ArticleNumber;
import de.fernunihagen.ifcbooleancatalogue.model.Value;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.MalformedBoolExpressionException;
import de.fernunihagen.ifcbooleancatalogue.shared.model.AbstractProperty;

public class AbstractSimpleBoolExpression implements AbstractBoolExpression {
    protected AbstractProperty property;
    protected IfcBenchmarkEnum operator;
    protected Value value;

    public AbstractSimpleBoolExpression(AbstractProperty property, IfcBenchmarkEnum operator, String value) {
	new AbstractSimpleBoolExpression(property, operator, property.addValue(value));
    }

    public AbstractSimpleBoolExpression(AbstractProperty property, IfcBenchmarkEnum operator, Value value) {
	this.property = property;
	this.operator = operator;
	this.value = value;

    }

    public AbstractSimpleBoolExpression(ArticleNumber property2, IfcBenchmarkEnum operator2, String value2) {
	// TODO Auto-generated constructor stub
    }

    @Override
    public String toString() {
	return OPEN_BRACKET + getPropertyName() + BLANK + getOperator().getSymbol() + BLANK + getValue().getText()
		+ CLOSE_BRACKET;
    }

    public String getPropertyName() {
	return property.getName();
    }

    public AbstractProperty getProperty() {
	return property;
    }

    public IfcBenchmarkEnum getOperator() {
	return operator;
    }

    public String getOperatorAsString() {
	return operator.toString();
    }

    public Value getValue() {
	return value;
    }

    public String getValueAsText() {
	return getValue().getText();
    }

    public List<AbstractSimpleBoolExpression> getComponents() {
	return Arrays.asList(this);
    }

    @Override
    public boolean equals(Object anotherExpr) {
	if (!(anotherExpr instanceof AbstractSimpleBoolExpression))
	    return false;

	AbstractSimpleBoolExpression anotherExpression = (AbstractSimpleBoolExpression) anotherExpr;

	return sameProperty(anotherExpression) && sameOperator(anotherExpression) && sameValue(anotherExpression);
    }

    private boolean sameValue(AbstractSimpleBoolExpression anotherExpression) {
	return this.getValueAsText().equals(anotherExpression.getValueAsText());
    }

    private boolean sameOperator(AbstractSimpleBoolExpression anotherExpression) {
	return this.getOperatorAsString().equals(anotherExpression.getOperatorAsString());
    }

    private boolean sameProperty(AbstractSimpleBoolExpression anotherExpression) {
	return this.getPropertyName().equals(anotherExpression.getPropertyName());
    }

    @Override
    public List<Value> getValues() {
	return new ArrayList<Value>(Arrays.asList(getValue()));
    }

    @Override
    public void addComponent(AbstractBoolExpression component) {
	// TODO Auto-generated method stub
	
    }

}
