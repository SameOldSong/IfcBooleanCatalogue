package de.fernunihagen.ifcbooleancatalogue.shared.model;

import java.util.ArrayList;
import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.model.Value;

public class AbstractProperty implements Constants {
    protected String name;
    protected List<Value> values;

    AbstractProperty(String name, ArrayList<Value> values) {
	this.name = name;
	this.values = values;
    }

    protected AbstractProperty(String name) {
	this.name = name;
	this.values = new ArrayList<>();
    }

    public Value addValue(String valueText) {
	Value value = getValue(valueText);

	if (value == null) {
	    value = new Value(valueText.trim(), this);
	    values.add(value);
	}
	return value;
    }

    public String getName() {
	return name;
    }

    public List<Value> getValues() {
	return values;
    }

    public Value getValue(String valueText) {
	for (Value val : values)
	    if (val.getText().equals(valueText.trim()))
		return val;
	return null;
    }

    public String getNotNoneValue() {
	for (Value value : values)
	    if (value.getText() != null && !value.getText().equals(NONE))
		return value.getText();
	return null;
    }

    public void setValues(List<Value> list) {
	this.values = list;
    }

}
