package de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions;

import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.model.Value;
import de.fernunihagen.ifcbooleancatalogue.shared.model.Constants;

public interface AbstractBoolExpression extends Constants {

    public List<? extends AbstractBoolExpression> getComponents(); 
    public List<Value> getValues();
    public void addComponent(AbstractBoolExpression component) ;
    
  

   

   

    

   

}
