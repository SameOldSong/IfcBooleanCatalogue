package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.products;

import java.util.ArrayList;
import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.BoolFileParser;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.ParsingException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.units.UnitInBoolFile;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.units.UnitsInBoolFile;

public class ProductsInBoolFile {
    private List<ProductInFile> products = new ArrayList<>();
    private List<String> fileLines;
    private UnitsInBoolFile unitsInFile;

    public ProductsInBoolFile(List<String> fileLines) throws ParsingException {
	this.fileLines = fileLines;
	unitsInFile = new UnitsInBoolFile();
	initProductsFromFile();

    }

    private void initProductsFromFile() throws ParsingException {

	int numHeaders = BoolFileParser.getNumOfHeaders(fileLines);

	for (int curHeaderIdx = 1; curHeaderIdx <= numHeaders; curHeaderIdx++)
	    extractProductFromFileAndAddToProducts(curHeaderIdx);

    }

    private void extractProductFromFileAndAddToProducts(int curHeaderIdx) throws ParsingException {

	List<String> curProductLines = BoolFileParser.getCurProductLines(fileLines, curHeaderIdx);
	addNewProductToProducts(curProductLines);

    }

    private void addNewProductToProducts(List<String> curProductLines) throws ParsingException {
	ProductInFile product = new ProductInFile(curProductLines);
	unitsInFile.addUnits(product.getPropertiesWithUnits());
	products.add(product);

    }

    public List<ProductInFile> getProducts() {
	return products;
    }

    public int getNumOfProducts() {
	return products.size();
    }

    public void add(ProductInFile productInFile) {
	products.add(productInFile);
    }

    public boolean unitsExist() {
	return unitsInFile.unitsExist();
    }

    public List<UnitInBoolFile> getGlobalUnits() {
	return unitsInFile.getGlobalUnits();
    }

}
