package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.expressions;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum LogicalOperatorInFile {
    AND("AND"), OR("OR"), NOT("NOT"), IMPLICATION("==>"), EQUIVALENCE("<==>");

    String symbolInFile;

    private LogicalOperatorInFile(String symbolInFile) {
	this.symbolInFile = symbolInFile;
    }

    public String getSymbolInFile() {
	return symbolInFile;
    }

    public static List<String> getOperators() {
	return Arrays.asList(LogicalOperatorInFile.values()).stream().map(operator -> operator.getSymbolInFile())
		.collect(Collectors.toList());

    }
}
