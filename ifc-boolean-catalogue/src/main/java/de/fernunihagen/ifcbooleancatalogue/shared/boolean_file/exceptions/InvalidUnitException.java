package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions;

public class InvalidUnitException extends ParsingException {

	public InvalidUnitException(String message){
		
		super(message);
	}
}
