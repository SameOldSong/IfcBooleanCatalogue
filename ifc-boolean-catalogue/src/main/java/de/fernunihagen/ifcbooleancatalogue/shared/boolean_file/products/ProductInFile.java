package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.products;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.helpers.PropertyHelper;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.BoolFileParser;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.MissingPropertyException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.ParsingException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.expressions.BoolExpressionHelper;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.expressions.BoolExpressionInFile;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.expressions.CompoundBoolExpressionInFile;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.expressions.LogicalOperatorInFile;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.expressions.PropertyInBoolExpression;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers.BoolLineHelper;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.properties.Property;
import de.fernunihagen.ifcbooleancatalogue.shared.model.Constants;

public class ProductInFile implements Constants {
    private String productName;
    private List<BoolExpressionInFile> expressions = new ArrayList<>();
    private List<Property> properties = new ArrayList<>();

    public ProductInFile(List<String> productLines) throws ParsingException {

	productName = BoolLineHelper.extractProductFromHeader(productLines);
	initPropertiesFromHeader(BoolFileParser.extractPropertiesFromHeader(productLines));
	createBoolExpressions(BoolFileParser.extractBoolExpressionsFromLines(productLines));

	addValuesFromExpressionsToProperties();
    }

    private void addValuesFromExpressionsToProperties() throws MissingPropertyException {
	for (BoolExpressionInFile expression : expressions)
	    addValuesOfExpressionToHeaderProperties(expression);

    }

    private void addValuesOfExpressionToHeaderProperties(BoolExpressionInFile expression)
	    throws MissingPropertyException {
	for (PropertyInBoolExpression prop : expression.getProperties())
	    if (propertyExistsInHeader(prop.getName()))
		findExistingProperty(prop).addValue(prop.getValue());
	    else
		throw new MissingPropertyException(prop.getName(), expression.toString());

    }

    private Property findExistingProperty(PropertyInBoolExpression prop) throws MissingPropertyException {

	return PropertyHelper.findExistingProperty(prop, properties);
    }

    private boolean propertyExistsInHeader(String propName) {
	return PropertyHelper.propertyExistsInHeader(propName, properties);
    }

    private void createBoolExpressions(List<String> boolExpressionsAsLines) throws ParsingException {
	for (String boolExprLine : boolExpressionsAsLines)
	    expressions.add(BoolExpressionHelper.createExpression(boolExprLine));

    }

    private void initPropertiesFromHeader(List<String> linesOfProperties) {
	linesOfProperties.stream().forEach(line -> properties.add(new Property(line)));
    }

    public List<Property> getProperties() {
	return properties;
    }

    public List<BoolExpressionInFile> getExpressions() {
	return expressions;
    }

    public String getProductName() {
	return productName;
    }

    public List<Property> getPropertiesWithUnits() {
	return properties.stream().filter(property -> property.hasUnit()).collect(Collectors.toList());
    }

    public BoolExpressionInFile getMainExpression() throws ParsingException {
	return new CompoundBoolExpressionInFile(expressions, LogicalOperatorInFile.AND);
    }

}
