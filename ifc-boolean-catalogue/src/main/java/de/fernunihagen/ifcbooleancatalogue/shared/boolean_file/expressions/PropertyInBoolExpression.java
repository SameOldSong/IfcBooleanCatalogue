package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.expressions;

public class PropertyInBoolExpression {
    private String name;

    private String value;

    public PropertyInBoolExpression(String name, String value) {
	this.name = name.trim();
	this.value = value.trim();

    }

    public String getName() {
	return name.trim();
    }

    public String getValue() {
	return value;
    }

    @Override
    public boolean equals(Object anotherProperty) {
	if (!(anotherProperty instanceof PropertyInBoolExpression))
	    return false;

	return this.getName().trim().equals(((PropertyInBoolExpression) anotherProperty).getName().trim());
    }

}
