package de.fernunihagen.ifcbooleancatalogue.shared;

import com.fasterxml.jackson.annotation.JsonIgnore;

import de.fernunihagen.ifcbooleancatalogue.model.Value;
import de.fernunihagen.ifcbooleancatalogue.shared.model.Constants;

public class SelectedValue {
    Value value;
    String valueText;
    String property;

    public SelectedValue(Value value) {
	this.value = value;
	valueText = value.getText();
	property = value.getPropertyName();
    }
    
    //only for jackson deserialization
    public SelectedValue()
    {
     super();
    }

    public String getValueText() {
	return valueText;
    }

    public String getProperty() {
	return property;
    }
    @Override
    public boolean equals(Object value) {
	SelectedValue anotherValue = (SelectedValue)value;
	return samePropertyName(anotherValue) && sameText(anotherValue);
    }

    private boolean samePropertyName(SelectedValue anotherValue) {
	return property.equals(anotherValue.getProperty());
    }

    private boolean sameText(SelectedValue anotherValue) {
	return valueText.equals(anotherValue.getValueText());
    }

    @Override
    public String toString() {

	return property + Constants.BLANK + Constants.EQUALS + Constants.BLANK + valueText;

    }
    @JsonIgnore
    public Value getValue() {
	return value;
    }

}
