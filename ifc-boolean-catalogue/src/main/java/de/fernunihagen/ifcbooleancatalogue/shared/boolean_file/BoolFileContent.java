package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FilenameUtils;

import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.ParsingException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.products.ProductInFile;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.products.ProductsInBoolFile;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.units.UnitInBoolFile;

public class BoolFileContent {
    private ProductsInBoolFile productsInFile;
    private String projectName;

    public BoolFileContent(File file) throws IOException, ParsingException {
	List<String> fileLines = BoolFileParser.getLines(file);
	projectName = FilenameUtils.removeExtension(file.getName());
	productsInFile = new ProductsInBoolFile(fileLines);

    }

    public List<ProductInFile> getProducts() {
	return productsInFile.getProducts();
    }

    public String getProjectName() {
	return projectName;
    }

    public boolean unitsExist() {
	return productsInFile.unitsExist();
    }

    public List<UnitInBoolFile> getGlobalUnits() {
	return productsInFile.getGlobalUnits();
    }

}
