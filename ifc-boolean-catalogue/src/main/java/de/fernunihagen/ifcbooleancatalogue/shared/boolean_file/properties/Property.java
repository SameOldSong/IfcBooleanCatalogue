package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.properties;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.metrics.IfcReference;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.expressions.PropertyInBoolExpression;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers.BoolLineHelper;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.units.UnitInBoolFile;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.units.UnitLineHelper;

public class Property {
    private String name;
    private String measureType; // IFCLABEL, IFCLENGTHMEASURE
    private UnitInBoolFile unit;
    private List<String> values = new ArrayList<>();

    // this is the first IfcReference in "IfcReference path" (= block of
    // IfcReferences describing this property) that is referenced by IfcMetric. It
    // is not set until there is an IfcMetric that references this
    // property. By using this IfcReference, we get multiple IfcMetrics referencing
    // the same property, i.e. avoid redundant identical blocks of IfcReference.
    private IfcReference firstReferenceInPathToProperty;

    public Property(String propertyLine) {
	this.name = BoolLineHelper.getPropertyNameFromPropertyLine(propertyLine);
	this.measureType = UnitLineHelper.getMeasureTypeFromPropertyLine(propertyLine);
	this.unit = UnitLineHelper.createUnit(propertyLine);
    }

    public boolean hasLocalUnit() {
	return unit != null && unit.isLocal();
    }
    
    public boolean hasGlobalUnit() {
	return !hasLocalUnit();
    }

    public String getName() {
	return name.trim();
    }

    public String getMeasureType() {
	return measureType;
    }

    public UnitInBoolFile getUnit() {
	return unit;
    }

    public boolean hasUnit() {
	return unit != null;
    }
    
    public boolean hasNoUnit() {
	return !hasUnit();
    }

    @Override
    public boolean equals(Object anotherProperty) {
	if (!(anotherProperty instanceof PropertyInBoolExpression))
	    return false;

	return this.getName().trim().equals(((PropertyInBoolExpression) anotherProperty).getName().trim());
    }

    public boolean hasValue(String value) {
	for (String existingValue : values)
	    if (existingValue.trim().equals(value.trim()))
		return true;

	return false;
    }

    public void addValue(String value) {
	if (!values.contains(value))
	    values.add(value);
    }

    public String getRandomValue() {
	return values.get(new Random().nextInt(values.size()));
    }

    public IfcReference getFirstReferenceInPathToProperty() {
	return firstReferenceInPathToProperty;
    }

    public void setFirstReferenceInPathToProperty(IfcReference lineIndexOfReferencePath) {
	this.firstReferenceInPathToProperty = lineIndexOfReferencePath;
    }
    
    public boolean isUsedInBoolExpressions() {
	return values.size() > 0;
    }

}
