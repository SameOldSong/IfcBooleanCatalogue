package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.expressions;

import org.springframework.util.StringUtils;

import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.MalformedBoolExpressionException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.ParsingException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers.BracketsHelper;
import de.fernunihagen.ifcbooleancatalogue.shared.model.Constants;

public class BoolExpressionHelper implements Constants {

    public static BoolExpressionInFile createExpression(String boolExprLine) throws ParsingException {
	if (simpleExpression(boolExprLine))
	    return new SimpleBoolExpressionInFile(boolExprLine);
	return new CompoundBoolExpressionInFile(boolExprLine);
    }

    static String transformExpressionIfImplicationOrEquivalence(String boolExprLine)
	    throws MalformedBoolExpressionException {
	if (isEquivalence(boolExprLine))
	    return normalizeEquivalence(boolExprLine);

	if (isImplication(boolExprLine))
	    return normalizeImplication(boolExprLine);

	return boolExprLine;
    }

    static String normalizeExprWithMultipleOrAndSingleProperty(String boolExprLine)
	    throws MalformedBoolExpressionException {
	if (multipleOrExprWithSingleProperty(boolExprLine))
	    return createSimpleExpressionsConnectedByOr(boolExprLine);

	return boolExprLine;
    }

    private static String createSimpleExpressionsConnectedByOr(String boolExprLine)
	    throws MalformedBoolExpressionException {
	String expression = BracketsHelper.removeRedundantBrackets(boolExprLine);
	String newExpression = "";
	String property = expression.split(EQUALS)[0].trim();
	String[] values = expression.split(EQUALS)[1].trim().split(OR);
	for (String value : values)
	    newExpression += createLineOfSimpleEqualExpr(property, value) + BLANK + OR + BLANK;
	return removeRedundantOrFromTheEndOfExpressionLine(newExpression);
    }

    private static String removeRedundantOrFromTheEndOfExpressionLine(String newExpression) {
	return newExpression.trim().substring(0, newExpression.length() - 3);
    }

    private static String createLineOfSimpleEqualExpr(String property, String value) {
	return OPEN_BRACKET + property.trim() + BLANK + EQUALS + BLANK + value.trim() + CLOSE_BRACKET;
    }

    private static boolean multipleOrExprWithSingleProperty(String boolExprLine)
	    throws MalformedBoolExpressionException {
	// (Heizkoerperart = Flachheizkoerper OR Badheizkoerper OR Gliederheizkoerper)
	// matches without brackets too

//	String pattern = "\\(?(\\w+\\s*)+\\s+\\=\\s+(\\w+\\s*)+(\\s+OR\\s+\\w+)+\\)?";
//	return Pattern.matches(pattern, boolExprLine);
	String expression = BracketsHelper.removeRedundantBrackets(boolExprLine);
	if (!exprHasExactlyOneEqualSymbol(expression))
	    return false;

	if (!exprHasAtLeastOneOr(expression))
	    return false;

	if (exprHasBrackets(expression))
	    return false;

	return true;
    }

    private static boolean exprHasBrackets(String expression) {
	return expression.contains(OPEN_BRACKET) || expression.contains(CLOSE_BRACKET);
    }

    private static boolean exprHasAtLeastOneOr(String expression) {
	return StringUtils.countOccurrencesOf(expression, OR) >= 1;
    }

    private static boolean exprHasExactlyOneEqualSymbol(String expression) {
	return StringUtils.countOccurrencesOf(expression, EQUALS) == 1;

    }

    private static String normalizeImplication(String boolExprLine) throws MalformedBoolExpressionException {
	String leftPart = BracketsHelper.removeRedundantBrackets(
		boolExprLine.split(LogicalOperatorInFile.IMPLICATION.getSymbolInFile())[0].trim());
	String rightPart = BracketsHelper.removeRedundantBrackets(
		boolExprLine.split(LogicalOperatorInFile.IMPLICATION.getSymbolInFile())[1].trim());

	return normalizeImplication(leftPart, rightPart);
    }

    private static String normalizeImplication(String leftPart, String rightPart) {

	return "NOT" + OPEN_BRACKET + leftPart + CLOSE_BRACKET + " OR " + OPEN_BRACKET + rightPart + CLOSE_BRACKET;
    }

    private static String normalizeEquivalence(String boolExprLine) {
	String leftPart = boolExprLine.split(LogicalOperatorInFile.EQUIVALENCE.getSymbolInFile())[0].trim();
	String rightPart = boolExprLine.split(LogicalOperatorInFile.EQUIVALENCE.getSymbolInFile())[0].trim();

	String leftToRightImplication = OPEN_BRACKET + normalizeImplication(leftPart, rightPart) + CLOSE_BRACKET;
	String rightToLeftImplication = OPEN_BRACKET + normalizeImplication(rightPart, leftPart) + CLOSE_BRACKET;

	return leftToRightImplication + " AND " + rightToLeftImplication;
    }

    private static boolean isImplication(String boolExprLine) {
	return boolExprLine.contains(LogicalOperatorInFile.IMPLICATION.getSymbolInFile());
    }

    private static boolean isEquivalence(String boolExprLine) {
	return boolExprLine.contains(LogicalOperatorInFile.EQUIVALENCE.getSymbolInFile());
    }

    public static boolean simpleExpression(String boolExprLine) {
	for (String operator : LogicalOperatorInFile.getOperators())
	    if (boolExprLine.contains(operator))
		return false;
	return true;
    }

}
