package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.objectives.LogicalOperator;
import de.fernunihagen.ifcbooleancatalogue.model.Value;
import de.fernunihagen.ifcbooleancatalogue.shared.model.AbstractProperty;
import de.fernunihagen.ifcbooleancatalogue.shared.model.Constants;

public class StringHelper implements Constants {

    public static boolean isSimpleBoolExpression(String expression) {

	for (String operator : LogicalOperator.getOperators(LogicalOperator.class))
	    if (expression.contains(operator))
		return false;

	return true;
    }

    public static List<Map<String, String>> generateValueCombinations(Map<String, String> selectedValues,
	    List<AbstractProperty> propertiesWithoutSelectedValue) {
	// all values were selected in UI
	if (propertiesWithoutSelectedValue.size() == 0)
	    return Arrays.asList(selectedValues);

	List<Map<String, String>> combinations = getCombinations(propertiesWithoutSelectedValue);
	for (Map<String, String> combination : combinations)
	    combination.putAll(selectedValues);

	return combinations;
    }

    public static boolean isCurrentSymbolCloseBracket(String value, int index) {
	return value.charAt(index) == CLOSE_BRACKET.toCharArray()[0];
    }

    public static boolean isCurrentSymbolComma(String value, int index) {
	return value.charAt(index) == COMMA.toCharArray()[0];
    }

    public static List<Map<String, String>> getCombinations(List<AbstractProperty> properties) {

	if (properties.size() == 1) {
	    List<Map<String, String>> combinations = new ArrayList<>();
	    for (Value value : properties.get(0).getValues()) {
		Map<String, String> map = new HashMap<String, String>();

		map.put(properties.get(0).getName(), value.getText());
		combinations.add(map);
	    }
	    return combinations;
	}
	AbstractProperty property = properties.remove(0);
	List<Map<String, String>> combinations = getCombinations(properties);
	List<Map<String, String>> newCombinations = new ArrayList<>();
	for (Value value : property.getValues()) {
	    List<Map<String, String>> valCombinations = new ArrayList<>();
	    for (Map<String, String> map : combinations) {
		Map<String, String> newMap = new HashMap<>();
		newMap.putAll(map);
		newMap.put(property.getName(), value.getText());
		valCombinations.add(newMap);
	    }
	    newCombinations.addAll(valCombinations);
	}

	return newCombinations;
    }

    

    public static String randomAlphaNumeric(int count) {
	String ALPHA_NUMERIC_STRING = "abcdefghijklmnopqrstuvwABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	StringBuilder builder = new StringBuilder();
	while (count-- != 0) {
	    int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
	    builder.append(ALPHA_NUMERIC_STRING.charAt(character));
	}
	return builder.toString();
    }

}
