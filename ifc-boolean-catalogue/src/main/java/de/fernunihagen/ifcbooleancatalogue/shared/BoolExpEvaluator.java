package de.fernunihagen.ifcbooleancatalogue.shared;

import java.util.ArrayList;
import java.util.Map;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.objectives.LogicalOperator;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.MalformedBoolExpressionException;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractBoolExpression;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractCompoundBoolExpression;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractSimpleBoolExpression;

public class BoolExpEvaluator {
    /**
     * determines if the specified boolean expression evaluates to true with the
     * given values
     * 
     * @param expression
     * @param evalValues
     * @return
     * @throws MalformedBoolExpressionException
     */
    public static boolean isExpressionTrue(AbstractBoolExpression expression, Map<String, String> evalValues)
	    throws MalformedBoolExpressionException {

	// simple
	if (expression instanceof AbstractSimpleBoolExpression)
	    return getBoolValueOfSimpleExpression((AbstractSimpleBoolExpression) expression, evalValues);

	// compound
	AbstractCompoundBoolExpression compExpression = (AbstractCompoundBoolExpression) expression;

	ArrayList<Boolean> componentValues = new ArrayList<Boolean>();
	for (AbstractBoolExpression component : compExpression.getComponents())
	    componentValues.add(isExpressionTrue(component, evalValues));
	return getBoolValueOfCompoundExpression(componentValues, compExpression.getOperator());

    }

    private static boolean getBoolValueOfSimpleExpression(AbstractSimpleBoolExpression simpleExp,
	    Map<String, String> evalValues) {
	String evalValue = evalValues.get(simpleExp.getPropertyName());
	if (evalValue == null)
	    return false;
	return evaluateSimpleBoolExp(simpleExp, evalValue);
    }

    private static boolean getBoolValueOfCompoundExpression(ArrayList<Boolean> componentValues,
	    LogicalOperator operator) {

	switch (operator) {

	case AND: {
	    for (Boolean value : componentValues)
		if (value == false)
		    return false;
	    return true;
	}

	case OR: {
	    for (Boolean value : componentValues)
		if (value == true)
		    return true;
	    return false;
	}

	case NOT:
	    return componentValues.get(0) == false;
	}

	return false;
    }

    private static boolean evaluateSimpleBoolExp(AbstractSimpleBoolExpression expression, String value) {

	switch (expression.getOperator()) {

	case EQUALTO:
	    return value.trim().equals(expression.getValue().getText());
	case NOTEQUALTO:
	    return !value.trim().equals(expression.getValue().getText());
	case GREATERTHAN:
	    return Double.parseDouble(value.trim()) > Double.parseDouble(expression.getValue().getText());
	case GREATERTHANOREQUALTO:
	    return Double.parseDouble(value.trim()) >= Double.parseDouble(expression.getValue().getText());
	case LESSTHAN:
	    return Double.parseDouble(value.trim()) < Double.parseDouble(expression.getValue().getText());
	case LESSTHANOREQUALTO:
	    return Double.parseDouble(value.trim()) <= Double.parseDouble(expression.getValue().getText());
	}

	return false;
    }
}
