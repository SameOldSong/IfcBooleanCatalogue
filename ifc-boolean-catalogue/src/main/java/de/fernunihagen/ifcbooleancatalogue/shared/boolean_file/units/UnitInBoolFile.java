package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.units;

import de.fernunihagen.ifcbooleancatalogue.shared.model.Constants;

public abstract class UnitInBoolFile implements Constants {

    protected boolean global;
    protected String measureType; // IFCPRESSUREMEASURE
    protected String unitName;// IFCCONVERSIONBASEDUNIT(BAR, PASCAL, 100000) or MILLIMETRE

    public UnitInBoolFile(String unitInPropertyLine) {
	// IFCPRESSUREMEASURE
	measureType = UnitLineHelper.getMeasureTypeFromPropertyLine(unitInPropertyLine);
	// IFCCONVERSIONBASEDUNIT(BAR, PASCAL, 100000)
	unitName = UnitLineHelper.getUnitNameFromPropertyLine(unitInPropertyLine);
    }

    UnitInBoolFile(String measureType, String unitName) {
	// IFCPRESSUREMEASURE
	this.measureType = measureType;
	// IFCCONVERSIONBASEDUNIT(BAR, PASCAL, 100000)
	this.unitName = unitName;
    }

    public void setGlobal(boolean global) {
	this.global = global;
    }

    public boolean isGlobal() {
	return global;
    }

    public String getMeasureType() {
	return measureType;
    }

    public String getUnitName() {
	return unitName.trim();
    }

    @Override
    public boolean equals(Object anotherUnit) {
	return getUnitName().equals(((UnitInBoolFile) anotherUnit).getUnitName());
    }

    public boolean isLocal() {
	return !global;
    }

}
