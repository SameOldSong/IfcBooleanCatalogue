package de.fernunihagen.ifcbooleancatalogue.shared;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.model.MissingPropertyException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.MalformedBoolExpressionException;
import de.fernunihagen.ifcbooleancatalogue.shared.model.Constants;

public class IfcFileParser implements Constants {

    private File file;
//    private AbstractPropertyManager propertyManager;
//    private AbstractBoolExpManager boolExpManager;
    private boolean endOfFile;

    /**
     * constructor for file reader
     * 
     * @param file
     */
//    public IfcFileParser(File file) {
//	this.file = file;
//	boolExpManager = new AbstractBoolExpManager();
//	endOfFile = false;
//
//    }

    /**
     * reads IFC file, initializes properties from header and boolean expressions.
     * While parsing expressions, adds values to properties.
     * 
     * @throws IOException
     * @throws MalformedBoolExpressionException
     * @throws InvalidUnitException
     * @throws MissingPropertyException
     */
//    public void parseIfcFile()
//	    throws IOException, MalformedBoolExpressionException, MissingPropertyException {
//	BufferedReader reader = new BufferedReader(new FileReader(file));
//	List<String> ifcHeader = new ArrayList<String>();
//	String line = reader.readLine();
//	while (!line.contains(IFC_OBJECTIVE))
//	    if (!empty(line) && !commentLine(line))
//		ifcHeader.add(line = reader.readLine());
//	//TODO
//	//propertyManager.setIfcHeader(ifcHeader);
//
//	List<String> ifcLines = new ArrayList<String>();
//	ifcLines.add(line);
//	while (!line.contains(IFC_END))
//	    if (!empty(line) && !commentLine(line))
//		ifcLines.add(reader.readLine());
//	boolExpManager.generateBoolExpressionsFromIfc(ifcLines);
//
//    }
//
//    public ArrayList<String> getBoolExpressions(BufferedReader reader) throws IOException {
//	ArrayList<String> expressions = new ArrayList<String>();
//
//	String line = reader.readLine();
//	while (line != null && !line.contains(HEADER_START)) {
//	    if (!empty(line) && !commentLine(line))
//		expressions.add(readCompleteExpression(line, reader));
//
//	    line = reader.readLine();
//	}
//
//	if (line == null)
//	    endOfFile = true;
//
//	return expressions;
//
//    }
//
//    private ArrayList<String> getHeader(BufferedReader reader) throws IOException {
//
//	String line = reader.readLine();// first header line
//
//	ArrayList<String> header = new ArrayList<String>();
//	while (line != null && !line.contains(HEADER_END)) {
//	    if (!empty(line) && !commentLine(line))
//		header.add(line);
//	    line = reader.readLine();
//	}
//	if (line == null)
//	    endOfFile = true;
//	return header;
//    }
//
//    private String readCompleteExpression(String line, BufferedReader reader) throws IOException {
//	if (multilineExpression(line))
//	    return line.trim().substring(1) + readMultipleLineExpression(reader);
//	else
//	    return line.trim();
//    }
//
//    private String readMultipleLineExpression(BufferedReader reader) throws IOException {
//	String expression = "";
//	String line = reader.readLine();
//	while (!line.trim().endsWith(MULTILINE_END)) {
//	    expression += " " + line.trim();
//	    line = reader.readLine();
//	}
//	expression += " " + line.trim().substring(0, line.length() - 1);
//	return expression;
//    }
//
//    private boolean multilineExpression(String line) {
//	return line.startsWith(MULTILINE_START);
//    }
//
//    private boolean commentLine(String line) {
//	return line.trim().startsWith(HASH);
//    }
//
//    private boolean empty(String line) {
//	return line.trim().length() == 0;
//    }
//
//    public AbstractPropertyManager getPropertyManager() {
//	return propertyManager;
//    }
//
//    public AbstractBoolExpManager getBoolExpManager() {
//	return boolExpManager;
//    }
    
    //TODO
//    public void parseFileForCatalogue()
//	    throws IOException, InvalidUnitException, MalformedBoolExpressionException, MissingPropertyException {
//	BufferedReader reader = new BufferedReader(new FileReader(file));
//	String line = reader.readLine();
//	while (!line.contains(HEADER_START))
//	    reader.readLine();
//	while (!endOfFile) {
//	    propertyManager.parseHeaderAndInitProperties(getHeader(reader));
//	    boolExpManager.generateExpressionsForCatalogue((getBoolExpressions(reader)));
//
//	}
//
//    }

}
