package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.units;

public class SiUnitInBoolFile extends UnitInBoolFile {
    // IFCLENGTHMEASURE(MILLIMETRE)
    public SiUnitInBoolFile(String unitInPropertyLine) {
	super(unitInPropertyLine);
    }

    SiUnitInBoolFile(String measureType, String unitName) {
	super(measureType, unitName);
    }

    @Override
    public boolean equals(Object anotherUnit) {
	if (!siUnit(anotherUnit))
	    return false;
	return super.equals(anotherUnit);
    }

    private boolean siUnit(Object anotherUnit) {
	return anotherUnit instanceof SiUnitInBoolFile;
    }
}
