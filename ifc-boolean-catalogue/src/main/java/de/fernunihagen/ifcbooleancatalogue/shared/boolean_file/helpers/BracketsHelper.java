package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers;

import java.util.regex.Pattern;

import org.springframework.util.StringUtils;

import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.MalformedBoolExpressionException;
import de.fernunihagen.ifcbooleancatalogue.shared.model.Constants;

public class BracketsHelper implements Constants {
    private final static char OPEN_BRACKET_CHAR = OPEN_BRACKET.charAt(0);
    private final static char CLOSE_BRACKET_CHAR = CLOSE_BRACKET.charAt(0);

    /**
     * finds closing bracket that corresponds to the specified opening bracket
     * 
     * @param expression
     * @param openingBracketIndex
     * @return index of the closing bracket
     * @throws MalformedBoolExpressionException
     */
    public static int getClosingBracketIndex(String expression, int openingBracketIndex)
	    throws MalformedBoolExpressionException {

	checkOpeningBracket(expression, openingBracketIndex);
	checkNumOfOpenAndClosingBracketsMatch(expression);

	int index = openingBracketIndex;
	int numNotClosedBrackets = 1;
	while ((numNotClosedBrackets != 0) && (index < expression.length())) {
	    index++;
	    if (expression.charAt(index) == OPEN_BRACKET_CHAR)
		numNotClosedBrackets++;
	    else if (expression.charAt(index) == CLOSE_BRACKET_CHAR)
		numNotClosedBrackets--;
	}

	checkClosingBracket(expression, index);

	return index;

    }

    private static void checkNumOfOpenAndClosingBracketsMatch(String expression)
	    throws MalformedBoolExpressionException {
	int numOfOpenBrackets = StringUtils.countOccurrencesOf(expression, OPEN_BRACKET);
	int numOfCloseBrackets = StringUtils.countOccurrencesOf(expression, CLOSE_BRACKET);

	if (numOfOpenBrackets != numOfCloseBrackets)
	    throw new MalformedBoolExpressionException(expression);

    }

    private static void checkOpeningBracket(String expression, int openingBracketIndex)
	    throws MalformedBoolExpressionException {
	if (expression.charAt(openingBracketIndex) != OPEN_BRACKET_CHAR)
	    throw new MalformedBoolExpressionException(expression);

    }

    private static void checkClosingBracket(String expression, int openingBracketIndex)
	    throws MalformedBoolExpressionException {
	if (expression.charAt(openingBracketIndex) != CLOSE_BRACKET_CHAR)
	    throw new MalformedBoolExpressionException(expression);

    }

    public static String removeRedundantBrackets(String exp) throws MalformedBoolExpressionException {
	String expression = exp.trim();

	// in case of redundant outer brackets - remove them:(((A = 1) OR (B = 2)))
	while (redundantBracketsExist(expression))
	    expression = expression.substring(1, expression.length() - 1).trim();
	return expression;
    }

    private static boolean redundantBracketsExist(String expression) throws MalformedBoolExpressionException {
	if (expressionStartsWithLetter(expression))
	    return false;
	
	int openingBracketIndex = 0;
	int closingBracketIndex = BracketsHelper.getClosingBracketIndex(expression, openingBracketIndex);
	return closingBracketIndex == expression.length() - 1;
    }

    private static boolean expressionStartsWithLetter(String expression) {
	// Heizkoerperart = Flachheizkoerper
	String pattern = "\\w.*";
	return Pattern.matches(pattern, expression);
    }

    public static String removeBrackets(String expression) {

	String exp = expression.trim();
	while (exp.startsWith(OPEN_BRACKET))
	    exp = exp.substring(1);

	while (exp.endsWith(CLOSE_BRACKET))
	    exp = exp.substring(0, exp.length() - 1);

	return exp;
    }

    public static String getContentOfBoxBrackets(String value) {
	if (!value.contains(OPEN_BOX_BRACKET))
	    return null;

	return value.substring(value.indexOf(OPEN_BOX_BRACKET) + 1,
		value.indexOf(CLOSE_BOX_BRACKET, value.indexOf(OPEN_BOX_BRACKET)));
    }

    public static boolean isClosingBracket(String expr, int index) {
	return expr.charAt(index) == CLOSE_BRACKET.toCharArray()[0];
    }

    public static int getOpenBracketIndex(String value) {
	return value.indexOf(OPEN_BRACKET);
    }

    public static String getSubstringInBrackets(String valInBrackets) {
	int openBracketIdx = getOpenBracketIndex(valInBrackets);
	return valInBrackets.substring(openBracketIdx + 1, valInBrackets.length() - 1);
    }

}
