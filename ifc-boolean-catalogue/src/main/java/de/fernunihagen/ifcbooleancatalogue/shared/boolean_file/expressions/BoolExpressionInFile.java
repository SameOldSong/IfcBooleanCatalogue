package de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.expressions;

import java.util.ArrayList;
import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.shared.model.Constants;

public abstract class BoolExpressionInFile implements Constants{
    protected List<BoolExpressionInFile> components = new ArrayList<BoolExpressionInFile>();

    public List<BoolExpressionInFile> getComponents() {
	return components;
    }
    
    public abstract List<PropertyInBoolExpression> getProperties();
    
    
}
