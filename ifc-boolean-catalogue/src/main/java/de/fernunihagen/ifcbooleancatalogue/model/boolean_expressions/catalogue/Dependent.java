package de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.catalogue;

import java.util.List;
import java.util.stream.Collectors;

public class Dependent {

    private String property;
    private List<String> selectableValues;

    public Dependent(String property, List<String> selectableValues) {
	this.property = property;
	this.selectableValues = selectableValues;
    }
    //only for jackson deserialization
    public Dependent()
    {
     super();
    }

    @Override
    public boolean equals(Object dependent) {
	Dependent anotherDependent = (Dependent) dependent;
	return samePropertyName(anotherDependent) && sameSelectableValues(anotherDependent);

    }

    private boolean sameSelectableValues(Dependent anotherDependent) {
	if (getNumSelectableValues() != anotherDependent.getNumSelectableValues())
	    return false;

	for (String value : selectableValues)
	    if (!anotherDependent.hasSelectableValue(value))
		return false;

	return true;
    }

    private boolean hasSelectableValue(String value) {
	return selectableValues.contains(value);
    }

    private Object getNumSelectableValues() {
	return selectableValues.size();
    }

    private boolean samePropertyName(Dependent anotherDependent) {
	return property.equals(anotherDependent.getProperty());
    }

    public String getProperty() {
	return property;
    }

    public List<String> getSelectableValues() {
	return selectableValues;
    }

    public void addSelectableValues(List<String> newSelectableValues) {
	selectableValues.addAll(newSelectableValues);
    }

    @Override
    public String toString() {
	String values = selectableValues.stream().map(Object::toString).collect(Collectors.joining(","));

	return property + ": " + values;
    }
}
