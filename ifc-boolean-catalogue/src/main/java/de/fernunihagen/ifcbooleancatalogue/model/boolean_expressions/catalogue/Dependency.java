package de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.catalogue;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonRootName;

public class Dependency {

    // (Heizkoerperart = Flachheizkoerper) ==> (Heizkoerperserie = Compact)
    // left part of the map (keys) is the combination of triggering values
    // left: (Heizkoerperart = Flachheizkoerper)
    // left part should always be interpreted as connected by AND
    // right part of the map consists of dependent values
    // right part should always be interpreted as connected by OR
    // right: (Heizkoerperserie = Compact)
//    @JsonIgnore
//    List<Value> triggerValues;

    // trigger is a value or a combination of values selected by user in dropdown
    // menus of some properties
    // as a result of this selection, selectable values in other dropdown menus
    // (dependents)
    // should be automatically changed
    Trigger trigger;

    // list of properties (dropdown menus) whose selectable values depend on trigger
    List<Dependent> dependents;

    public Dependency(Trigger trigger, List<Dependent> dependents) {
	this.trigger = trigger;
	this.dependents = dependents;
    }
    
    //only for jackson deserialization
    public Dependency()
    {
     super();
    }

    public boolean hasSameTrigger(Dependency anotherDependency) {
	return trigger.equals(anotherDependency.getTrigger());
    }

    public Trigger getTrigger() {
	return trigger;
    }

    public List<Dependent> getDependents() {
	return dependents;
    }

    public void addDependents(List<Dependent> newDependents) {
	for (Dependent newDependent : newDependents) {
	    Dependent existingDependent = findDependentWithSameProperty(newDependent.getProperty());
	    if (existingDependent == null)
		dependents.add(newDependent);
	    else
		existingDependent.addSelectableValues(newDependent.getSelectableValues());
	}
    }

    private Dependent findDependentWithSameProperty(String property) {
	for (Dependent dependent : dependents)
	    if (dependent.getProperty().equals(property))
		return dependent;
	return null;
    }
    
    @Override
    public boolean equals(Object dependency) {
	Dependency anotherDependency = ((Dependency)dependency);
	return sameTrigger(anotherDependency) && sameDependents(anotherDependency);
    }
    
    @JsonIgnore
    public int getNumDependents() {
	return dependents.size();
    }

    private boolean sameDependents(Dependency anotherDependency) {
	if(getNumDependents() != anotherDependency.getNumDependents())
	    return false;
	
	for(Dependent dependent : dependents)
	    if(!anotherDependency.hasDependent(dependent))
		return false;
	
	return true;
	    
    }

    private boolean hasDependent(Dependent theDependent) {
	for(Dependent dependent : dependents)
	    if(dependent.equals(theDependent))
		return true;
	return false;
    }

    private boolean sameTrigger(Dependency anotherDependency) {
	return trigger.equals(anotherDependency.getTrigger());
    }

}
