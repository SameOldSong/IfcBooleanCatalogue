package de.fernunihagen.ifcbooleancatalogue.model;

public class MissingProductException extends Exception {
	private String message;
	public MissingProductException(){
		
		this.message = "Product name is missing in Header. The line should look like this: Produkt : <product name>";
	}
}
