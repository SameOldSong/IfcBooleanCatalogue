package de.fernunihagen.ifcbooleancatalogue.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.metrics.IfcBenchmarkEnum;
import de.fernunihagen.ifcbooleancatalogue.shared.model.AbstractProperty;
import de.fernunihagen.ifcbooleancatalogue.shared.model.Constants;

@JsonPropertyOrder({ "text", "propertyName"})
public class Value {
    @JsonIgnore
    AbstractProperty property;
    String valueText;
    

    // for building article number
    int indexInArtNum;
    @JsonIgnore
    String valueInArtNum;

    public Value(String valueText, AbstractProperty property) {
	this.valueText = valueText;
	this.property = property;
    }

    public String getText() {
	return valueText;
    }
    @Override
    public String toString() {

	return property.getName() + Constants.BLANK + Constants.EQUALS + Constants.BLANK + valueText;

    }

   

    @JsonIgnore
    public AbstractProperty getProperty() {
	return property;
    }

    public String getPropertyName() {
	return property.getName();
    }

    public void setProperty(AbstractProperty property) {
	this.property = property;
    }

    public int getIndexInArtNum() {
	return indexInArtNum;
    }

    @JsonIgnore
    public void setIndexInArtNum(int indexInArtNum) {
	this.indexInArtNum = indexInArtNum;
    }

    @JsonIgnore
    public String getValueInArtNum() {
	return valueInArtNum;
    }

    @JsonIgnore
    public void setValueInArtNum(String valueInArtNum) {
	this.valueInArtNum = valueInArtNum;
    }

    public boolean matches(IfcBenchmarkEnum operator, Value benchmarkValue) {
	return matches(operator, benchmarkValue.getText());
    }

    public boolean matches(IfcBenchmarkEnum operator, String benchmarkValue) {
	switch (operator) {
	case GREATERTHAN:
	    return Double.parseDouble(this.getText()) > Double.parseDouble(benchmarkValue);
	case GREATERTHANOREQUALTO:
	    return Double.parseDouble(this.getText()) >= Double.parseDouble(benchmarkValue);
	case LESSTHAN:
	    return Double.parseDouble(this.getText()) < Double.parseDouble(benchmarkValue);
	case LESSTHANOREQUALTO:
	    return Double.parseDouble(this.getText()) <= Double.parseDouble(benchmarkValue);
	case NOTEQUALTO:
	    return !this.getText().equals(benchmarkValue);
	case EQUALTO:
	    return this.getText().equals(benchmarkValue);
	}
	return false;
    }
    @Override
    public boolean equals(Object anotherValue) {
	return samePropertyName(anotherValue) && sameText(anotherValue);
    }

    private boolean samePropertyName(Object anotherValue) {
	return property.getName().equals(((Value)anotherValue).getPropertyName());
    }

    private boolean sameText(Object anotherValue) {
	return getText().equals(((Value)anotherValue).getText());
    }
}
