package de.fernunihagen.ifcbooleancatalogue.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreType;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.properties.IfcProperty;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.properties.IfcValueType;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units.IfcUnit;
import de.fernunihagen.ifcbooleancatalogue.shared.model.Constants;
@JsonIgnoreType
public class ArticleNumber extends IfcProperty implements Constants{
	public ArticleNumber(String name, IfcValueType type, IfcUnit unit) {
	super(1);
	// TODO Auto-generated constructor stub
    }


	Map <Integer, List <String>> indexedValues;
//	public ArticleNumber(Product product, String name) {
//		super(name);
//		//super(name, product, IfcValueType.IFCLABEL, null);
//		indexedValues = new HashMap <Integer, List <String>>();
//		
//	}
	
	
	public void addValue(int position, String value) {
		List <String> values = indexedValues.get(position);
		if(values == null)
			values = new ArrayList<String>();
		values.add(value);
		indexedValues.put(position, values);
	}


	public Map<Integer, List<String>> getIndexedValues() {
		return indexedValues;
	}


	@Override
	public void initLineComponents() {
	    // TODO Auto-generated method stub
	    
	}


	@Override
	public String getIfcClassName() {
	    // TODO Auto-generated method stub
	    return null;
	}


	@Override
	public boolean hasGlobalUnit() {
	    // TODO Auto-generated method stub
	    return false;
	}

}
