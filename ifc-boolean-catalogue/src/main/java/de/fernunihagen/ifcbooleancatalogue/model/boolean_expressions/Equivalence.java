package de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions;

import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractBoolExpression;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractCompoundBoolExpression;

public class Equivalence extends AbstractCompoundBoolExpression {
    private Implication leftToRight;
    private Implication rightToLeft;

    public Equivalence(AbstractBoolExpression leftExpression, AbstractBoolExpression rightExpression) {
//	super(LogicalOperator.AND);
//	
//	
//	leftToRight = new Implication(leftExpression, rightExpression);
//	rightToLeft = new Implication(rightExpression, leftExpression);
//
//	components.add(leftToRight);
//	components.add(rightToLeft);

    }

    public Implication getLeftToRightImplication() {
	return leftToRight;
    }

    public Implication getRightToLeftImplication() {
	return rightToLeft;
    }

    @Override
    public void addComponent(AbstractBoolExpression component) {
	// TODO Auto-generated method stub
	
    }

    @Override
    public List<? extends AbstractBoolExpression> getComponents() {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public void setComponents(List<AbstractBoolExpression> components) {
	// TODO Auto-generated method stub
	
    }

}
