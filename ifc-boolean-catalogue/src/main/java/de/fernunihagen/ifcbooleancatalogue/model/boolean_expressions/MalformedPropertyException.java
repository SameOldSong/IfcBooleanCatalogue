package de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions;

public class MalformedPropertyException extends Exception {
	private String message;
	public MalformedPropertyException(String message){
		
		this.message = message;
	}
}
