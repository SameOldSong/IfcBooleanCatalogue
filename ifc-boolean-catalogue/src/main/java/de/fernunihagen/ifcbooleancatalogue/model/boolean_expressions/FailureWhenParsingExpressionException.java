package de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions;

public class FailureWhenParsingExpressionException extends Exception {
	private String message;
	public FailureWhenParsingExpressionException(String message){
		
		this.message = message;
	}
}
