package de.fernunihagen.ifcbooleancatalogue.model;

import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.ParsingException;

public class MissingPropertyException extends ParsingException {
    private String message;

    public MissingPropertyException(String propertyName) {

	super("Property " + propertyName + " is missing in Header");
    }
}
