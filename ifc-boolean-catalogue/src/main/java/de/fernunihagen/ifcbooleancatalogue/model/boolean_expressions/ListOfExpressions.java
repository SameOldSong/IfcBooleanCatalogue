package de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractBoolExpression;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractCompoundBoolExpression;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractSimpleBoolExpression;

public class ListOfExpressions {
    private final List<? extends AbstractBoolExpression> expressions;
    private final List<AbstractCompoundBoolExpression> compoundExpressions;
    private final List<AbstractSimpleBoolExpression> simpleExpressions;

    // contains not only simpleExpressions, but also simple components of compound
    // expressions
    private final List<AbstractSimpleBoolExpression> simpleComponents;

    public ListOfExpressions(List<? extends AbstractBoolExpression> expressions) {
	this.expressions = expressions;
	this.compoundExpressions = getCompoundExpressions(expressions);
	this.simpleExpressions = getSimpleExpressions(expressions);

	this.simpleComponents = (List<AbstractSimpleBoolExpression>) merge(simpleExpressions,
		getSimpleComponentsOfCompoundExpressions(compoundExpressions));
    }

    public List<? extends AbstractBoolExpression> getExpressions() {
	return expressions;
    }

    public List<AbstractBoolExpression> getCommonSimpleComponents() {
	List<AbstractBoolExpression> commonComponents = new ArrayList<AbstractBoolExpression>();
	List<AbstractBoolExpression> checkedComponents = new ArrayList<AbstractBoolExpression>();

	for (AbstractBoolExpression expression : expressions) {
	    for (AbstractBoolExpression component : expression.getComponents()) {
		if (!checkedComponents.contains(component) && isCommonComponent(component))
		    commonComponents.add(component);
		checkedComponents.add(component);
	    }
	}
	return commonComponents;
    }

    private boolean isCommonComponent(AbstractBoolExpression component) {
	for (AbstractBoolExpression expression : expressions)
	    if (expression instanceof AbstractCompoundBoolExpression && !expression.getComponents().contains(component))
		return false;
	return true;
    }

    public boolean allExpressionsHaveSameProperty() {

	String firstProperty = simpleComponents.get(0).getPropertyName();
	for (AbstractBoolExpression expression : simpleComponents)
	    if (!((AbstractSimpleBoolExpression) expression).getPropertyName().equals(firstProperty))
		return false;
	return true;
    }

//    public boolean contains(BoolExpression anotherExpression) {
//	for (BoolExpression expression : expressions)
//	    if (expression.equals(anotherExpression))
//		return true;
//
//	return false;
//    }

    private List<? extends AbstractBoolExpression> merge(List<? extends AbstractBoolExpression> listOne,
	    List<? extends AbstractBoolExpression> listTwo) {

	List<AbstractBoolExpression> mergedList = new ArrayList<AbstractBoolExpression>(listOne);
	List<AbstractBoolExpression> copyOfListTwo = new ArrayList<AbstractBoolExpression>(listTwo);

	for (AbstractBoolExpression simple : (ArrayList<AbstractBoolExpression>) copyOfListTwo)
	    if (!mergedList.contains(simple))
		mergedList.add(simple);

	return mergedList;

    }

    private List<AbstractCompoundBoolExpression> getCompoundExpressions(List<? extends AbstractBoolExpression> expressions) {
	List<AbstractCompoundBoolExpression> compounds = new ArrayList<AbstractCompoundBoolExpression>();

	for (AbstractBoolExpression expression : expressions)
	    if (expression instanceof AbstractCompoundBoolExpression)
		compounds.add((AbstractCompoundBoolExpression) expression);

	return compounds;
    }

    private List<AbstractSimpleBoolExpression> getSimpleExpressions(List<? extends AbstractBoolExpression> expressions) {
	List<AbstractSimpleBoolExpression> simples = new ArrayList<AbstractSimpleBoolExpression>();

	for (AbstractBoolExpression expression : expressions)
	    if (expression instanceof AbstractSimpleBoolExpression)
		simples.add((AbstractSimpleBoolExpression) expression);
	return simples;
    }

    private List<AbstractSimpleBoolExpression> getSimpleComponentsOfCompoundExpressions(
	    List<? extends AbstractBoolExpression> compoundExpressions) {
	List<AbstractSimpleBoolExpression> simples = new ArrayList<AbstractSimpleBoolExpression>();

	for (AbstractBoolExpression expression : compoundExpressions)
	    if (expression instanceof AbstractSimpleBoolExpression)
		simples.add((AbstractSimpleBoolExpression) expression);
	    else
		simples.addAll(getSimpleComponentsOfCompoundExpressions(expression.getComponents()));

	return simples;
    }

    public List<AbstractSimpleBoolExpression> getSimpleExpressions() {
	return simpleExpressions;
    }

    public List<AbstractSimpleBoolExpression> getSimpleComponentsOfAllCompoundExpressions() {
	return simpleComponents;
    }
}
