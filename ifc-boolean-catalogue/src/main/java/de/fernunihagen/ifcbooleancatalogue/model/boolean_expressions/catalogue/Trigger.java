package de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.catalogue;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import de.fernunihagen.ifcbooleancatalogue.model.Value;
import de.fernunihagen.ifcbooleancatalogue.shared.SelectedValue;

public class Trigger {
    private List<SelectedValue> selectedValues;

    public Trigger(List<Value> values) {
	selectedValues = new ArrayList<SelectedValue>();
	addSelectedValues(values);
    }
    
    //only for jackson deserialization
    public Trigger()
    {
     super();
    }

    /**
     * @return the selectedValues
     */
    public List<SelectedValue> getSelectedValues() {
	return selectedValues;
    }

    @JsonIgnore
    public int getNumOfValues() {
	return selectedValues.size();
    }

    @Override
    public boolean equals(Object anotherTrigger) {
	if (this.getNumOfValues() != ((Trigger) anotherTrigger).getNumOfValues())
	    return false;

	for (SelectedValue value : selectedValues)
	    if (!((Trigger) anotherTrigger).contains(value))
		return false;

	return true;
    }

    private boolean contains(SelectedValue valueToCheck) {
	for (SelectedValue value : selectedValues)
	    if (value.equals(valueToCheck))
		return true;

	return false;
    }

    public void addSelectedValues(List<Value> values) {
	for (Value value : values)
	    addSelectedValue(value);

    }

    public void addSelectedValue(Value value) {
	selectedValues.add(new SelectedValue(value));
    }

    @Override
    public String toString() {
	String trigger = "";
	for (SelectedValue value : selectedValues)
	    trigger += value.toString() + ",";

	return trigger.substring(0, trigger.length() - 1);
    }

}
