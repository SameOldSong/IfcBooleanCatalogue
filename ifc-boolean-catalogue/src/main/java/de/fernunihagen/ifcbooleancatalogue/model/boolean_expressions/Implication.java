package de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions;

import java.util.ArrayList;
import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractBoolExpression;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractCompoundBoolExpression;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractNegation;

public class Implication extends AbstractCompoundBoolExpression {
    private AbstractBoolExpression leftPart;
    private AbstractBoolExpression rightPart;

    public Implication(AbstractBoolExpression leftExpression, AbstractBoolExpression rightExpression) {
//	super(LogicalOperator.OR);
//	
//	//parts
//	leftPart = leftExpression;
//	rightPart = rightExpression;
//	
//	// (NOT leftPart) OR rightPart
//	AbstractNegation negatedLeftPart = new AbstractNegation(leftPart);
//	
//
//	components.add(negatedLeftPart);
//	components.add(rightPart);
    }

    public AbstractBoolExpression getLeftPart() {
	return leftPart;
    }

    public AbstractBoolExpression getRightPart() {
	return rightPart;
    }

    @Override
    public void addComponent(AbstractBoolExpression component) {
	// TODO Auto-generated method stub
	
    }

    @Override
    public List<? extends AbstractBoolExpression> getComponents() {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public void setComponents(List<AbstractBoolExpression> components) {
	// TODO Auto-generated method stub
	
    }

}
