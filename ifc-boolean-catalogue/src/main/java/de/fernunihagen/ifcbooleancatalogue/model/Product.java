package de.fernunihagen.ifcbooleancatalogue.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.catalogue.Dependency;
import de.fernunihagen.ifcbooleancatalogue.shared.model.AbstractProperty;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractBoolExpression;
@JsonPropertyOrder({ "name", "manufacturer", "properties", "dependencies"})
public class Product {
    private String name;
    private String manufacturer;
    private AbstractBoolExpression mainExpression;
    private List<AbstractProperty> properties;
    private List<Dependency> dependencies;

    public Product(String name) {

	this.name = name;
	properties = new ArrayList<>();
	dependencies = new ArrayList<>();
    }

    @Override
    public boolean equals(Object product) {
	Product anotherProduct = ((Product) product);
	if (!sameManufacturer(anotherProduct))
	    return false;
	if (!sameProperties(anotherProduct))
	    return false;
	if (!sameExpressions(anotherProduct))
	    return false;
	return true;
    }

    private boolean sameExpressions(Product anotherProduct) {
	return mainExpression.equals(anotherProduct.getMainExpression());
    }

    private boolean sameProperties(Product anotherProduct) {
	if (getNumProperties() != anotherProduct.getNumProperties())
	    return false;
	for (AbstractProperty property : properties)
	    if (!anotherProduct.hasProperty(property))
		return false;

	return true;

    }

    private boolean hasProperty(AbstractProperty theProperty) {
	for(AbstractProperty property : properties)
	    if(property.equals(theProperty))
		return true;
	return false;
    }

    private boolean sameManufacturer(Product anotherProduct) {
	return manufacturer.equals(anotherProduct.getManufacturer());
    }

    @JsonIgnore
    public List<AbstractBoolExpression> getExpressions() {
	//TO FIX
	return null;
	//return mainExpression.getComponents();
    }

    public String getName() {
	return name;
    }

    public void addExpression(AbstractBoolExpression expression) {
	//TO FIX
	//mainExpression.getComponents().add(expression);
    }

    public List<AbstractProperty> getProperties() {
	return properties;
    }

    public void addProperty(AbstractProperty property) {
	properties.add(property);
    }

    public void addProperties(List<AbstractProperty> newProperties) {
	properties.addAll(newProperties);
    }

    public String getManufacturer() {
	return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
	this.manufacturer = manufacturer;
    }

    /**
     * returns list of properties that are not contained in the specified subset of
     * properties
     * 
     * @param subsetOfProperties
     * @return
     */
    @JsonIgnore
    public List<AbstractProperty> getMissingProperties(List<String> subsetOfProperties) {

	List<AbstractProperty> missingProperties = new ArrayList<AbstractProperty>();
	for (AbstractProperty property : properties)
	    if (!subsetOfProperties.contains(property.getName()))
		missingProperties.add(property);
	return missingProperties;
    }

    public void setMainExpression(AbstractBoolExpression mainExpression) {
	this.mainExpression = mainExpression;
    }

    @JsonIgnore
    public AbstractProperty getProperty(String name) {
	for (AbstractProperty property : properties)
	    if (property.getName().equals(name))
		return property;

	return null;
    }

    @JsonIgnore
    public AbstractBoolExpression getMainExpression() {
	return mainExpression;
    }

    @JsonIgnore
    public int getNumProperties() {
	return properties.size();
    }

    @JsonIgnore
    public int getNumExpressions() {
	return mainExpression.getComponents().size();
    }

    public List<Dependency> getDependencies() {
        return dependencies;
    }

    public void setDependencies(List<Dependency> dependencies) {
        this.dependencies = dependencies;
    }

}
