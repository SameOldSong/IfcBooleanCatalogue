package de.fernunihagen.ifcbooleancatalogue.boolean_to_catalogue.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.metrics.IfcBenchmarkEnum;
import de.fernunihagen.ifcbooleancatalogue.model.Product;
import de.fernunihagen.ifcbooleancatalogue.model.Value;
import de.fernunihagen.ifcbooleancatalogue.shared.model.AbstractProperty;

public class Property extends AbstractProperty {

    Property(String name) {
	super(name);
	// TODO Auto-generated constructor stub
    }

    private Product product;

//    Property(String name, ArrayList<Value> values) {
//	this.name = name;
//	this.values = values;
//    }
//
//    public Property(String name, Product product) {
//	this.name = name;
//	this.product = product;
//	this.values = new ArrayList<Value>();
//    }

    @JsonIgnore
    public Product getProduct() {
	return product;
    }

    @JsonIgnore
    public ArrayList<String> getValuesAsStrings() {
	ArrayList<String> valStrings = new ArrayList<String>();
	for (Value val : values)
	    valStrings.add(val.getText());
	return valStrings;
    }

    @JsonIgnore
    public Value getValue(String valueText) {
	for (Value val : values)
	    if (val.getText().equals(valueText.trim()))
		return val;
	return null;
    }

    @JsonIgnore
    public String getNotNoneValue() {
	for (Value value : values)
	    if (value.getText() != null && !value.getText().equals(NONE))
		return value.getText();
	return null;
    }

    @JsonIgnore
    public List<Value> getValuesMatchingRule(IfcBenchmarkEnum operator, Value benchmarkValue) {
	List<Value> matchingValues = new ArrayList<>();
	for (Value value : this.values)
	    if (value.matches(operator, benchmarkValue))
		matchingValues.add(value);

	return matchingValues;
    }

    public void setValues(List<Value> list) {
	this.values = list;
    }

    @Override
    public boolean equals(Object property) {
	Property anotherProperty = (Property) property;
	if (!sameName(anotherProperty))
	    return false;
	if (!sameValues(anotherProperty))
	    return false;

	if (!sameProductName(anotherProperty))
	    return false;

	return true;
    }

    private boolean sameProductName(Property anotherProperty) {
	return product.getName().equals(anotherProperty.getProduct().getName());
    }

    private boolean sameValues(Property anotherProperty) {
	if (values.size() != anotherProperty.getValues().size())
	    return false;

	for (Value value : values)
	    if (!anotherProperty.hasValue(value))
		return false;
	return true;
    }

    private boolean hasValue(Value theValue) {
	for (Value value : values)
	    if (value.equals(theValue))
		return true;
	return false;
    }

    private boolean sameName(Property anotherProperty) {
	return name.equals(anotherProperty.getName());
    }

}
