package de.fernunihagen.ifcbooleancatalogue.boolean_to_catalogue.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.objectives.LogicalOperator;
import de.fernunihagen.ifcbooleancatalogue.model.Value;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.ListOfExpressions;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.catalogue.Dependent;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.catalogue.Trigger;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractBoolExpression;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractCompoundBoolExpression;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractSimpleBoolExpression;

public class Conjunction extends CompoundBoolExpression {

    public Conjunction(LogicalOperator operator) {
	super(operator);
	// TODO Auto-generated constructor stub
    }

    /**
     * conjunction AND can connect either // 1. simple expressions: in this case all
     * values are components of one trigger // Example: (A = 100) AND (B = 30) // 2.
     * or disjunctions of simple expressions: in this case these should be //
     * combined into several triggers // Example: (A = 100) AND (B = 30 OR B = 40)
     * should be transformed into: // Trigger_1 (A = 100) AND (B = 30) // Trigger_2
     * (A = 100) AND (B = 40)
     **/
//    @Override
//    public List<Trigger> generateTriggers() {
//	// replace any not-Equal operator in simple components by disjunction
//	// (A > 10) should be transformed (depending on property values) to (A = 20) OR
//	// (A = 30)
//	components = normalizeSimpleComponents();
//
//	// case_1: conjunction of simple expressions
//	if (allComponentsSimpleExpressions())
//	    return new ArrayList<Trigger>(Arrays.asList(new Trigger(getValues())));
//
//	// case_2: conjunction of disjunctions of simple expressions
//
//	// case2_1. Check if all the disjunctions have the same property. If so -
//	// combine
//	// them all in one disjunction: ((a=1 OR a=2) AND (a=3 OR a=4)) is equivalent to
//	// ((a=1 OR a=2 OR a=3 OR a=4))
//	ListOfExpressions expressionsList = new ListOfExpressions(components);
//	if (expressionsList.allExpressionsHaveSameProperty())
//	    return new Disjunction(expressionsList.getCommonSimpleComponents()).generateTriggers();
//
//	// case2_2. The properties are different
//	// Example: (F = 45) AND (A = 100 OR D = 50) AND (B = 30 OR B = 40)
//
//	// STEP 1. Isolate all disjunctions and calculate for each possible combination
//	// of values a trigger
//	// Example: (A = 100 OR D = 50) AND (B = 30 OR B = 40) should be transformed
//	// into:
//	// Trigger_1: (A = 100) AND (B = 30)
//	// Trigger_2: (A = 100) AND (B = 40)
//	// Trigger_4: (A = 50) AND (B = 30)
//	// Trigger_4: (A = 50) AND (B = 40)
//	List<Trigger> triggers = getTriggersByCombiningComponentsOfDisjunctions(isolateDisjunctions());
//
//	// STEP 2. If there are also simple expressions (besides disjunctions) - just
//	// add each of them to each trigger (in our example - (F = 45))
//	// Trigger_1: (A = 100) AND (B = 30) AND (F = 45)
//	// Trigger_2: (A = 100) AND (B = 40) AND (F = 45)
//	// Trigger_4: (A = 50) AND (B = 30) AND (F = 45)
//	// Trigger_4: (A = 50) AND (B = 40) AND (F = 45)
//	List<Value> valuesFromSimpleExpressions = getValuesFromSimples(isolateSimpleExpressions());
//	for (Trigger trigger : triggers)
//	    trigger.addSelectedValues(valuesFromSimpleExpressions);
//
//	return triggers;
//
//    }
//
//    private List<Disjunction> isolateDisjunctions() {
//	List<Disjunction> disjunctions = new ArrayList<>();
//
//	for (AbstractBoolExpression component : components)
//	    if (component instanceof Disjunction)
//		disjunctions.add((Disjunction) component);
//	return disjunctions;
//    }
//
//    private List<Trigger> getTriggersByCombiningComponentsOfDisjunctions(List<Disjunction> disjunctions) {
//	List<Trigger> triggers = new ArrayList<>();
//
//	if (disjunctions.size() == 1)
//	    for (AbstractBoolExpression component : disjunctions.get(0).getComponents())
//		triggers.add(new Trigger(component.getValues()));
//	else
//	    for (Value value : disjunctions.get(0).getValues()) {
//		List<Disjunction> copyWithoutFirst = disjunctions.stream().skip(1).collect(Collectors.toList());
//		for (Trigger trigger : getTriggersByCombiningComponentsOfDisjunctions(copyWithoutFirst)) {
//		    trigger.addSelectedValue(value);
//		    triggers.add(trigger);
//		}
//	    }
//	return triggers;
//    }

    private List<Value> getValuesFromSimples(List<AbstractSimpleBoolExpression> simples) {
	List<Value> values = new ArrayList<>();
	for (AbstractSimpleBoolExpression simple : simples)
	    values.add(simple.getValue());
	return values;
    }

//    private List<AbstractSimpleBoolExpression> isolateSimpleExpressions() {
//	List<AbstractSimpleBoolExpression> simples = new ArrayList<>();
//
//	for (AbstractBoolExpression component : components)
//	    if (component instanceof AbstractSimpleBoolExpression)
//		simples.add((AbstractSimpleBoolExpression) component);
//	return simples;
//    }
//
//    private List<AbstractBoolExpression> normalizeSimpleComponents() {
//	List<AbstractBoolExpression> newExpressions = new ArrayList<>();
//	for (AbstractBoolExpression component : components)
//	    newExpressions.add(normalizeSimpleComponent(component));
//	return newExpressions;
//    }

    private AbstractBoolExpression normalizeSimpleComponent(AbstractBoolExpression expression) {
	// simple expression
//	if (expression instanceof SimpleBoolExpression)
//	    return ((SimpleBoolExpression) expression).convertOperatorToEqual();

	// compound expression
	List<AbstractBoolExpression> newComponents = new ArrayList<AbstractBoolExpression>();
	for (AbstractBoolExpression component : expression.getComponents())
	    newComponents.add(normalizeSimpleComponent(component));

	((AbstractCompoundBoolExpression)expression).setComponents(newComponents);
	return expression;
    }

    @Override
    public boolean disjunctiveNormalForm() {
	// TODO Auto-generated method stub
	return false;
    }

    @Override
    public List<Dependent> generateDependents() {
	// TODO Auto-generated method stub
	return null;
    }

}
