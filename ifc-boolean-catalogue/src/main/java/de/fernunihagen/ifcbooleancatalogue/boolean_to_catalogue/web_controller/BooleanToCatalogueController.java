package de.fernunihagen.ifcbooleancatalogue.boolean_to_catalogue.web_controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_catalogue.web_service.BooleanToCatalogueService;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.MalformedBoolExpressionException;
import de.fernunihagen.ifcbooleancatalogue.shared.model.AbstractProperty;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class BooleanToCatalogueController {

//    private final BooleanToCatalogueService fileService;

//    @Autowired
//    public BooleanToCatalogueController(BooleanToCatalogueService fileService) {
//	this.fileService = fileService;
//    }
//
//    @PostMapping(value = "/uploadBooleanToCatalogue")
//    @ResponseStatus(HttpStatus.OK)
//    public ResponseEntity<String> handleFileUpload(@RequestParam("file") MultipartFile file) {
//	String message = "";
//	try {
//	    fileService.generateCatalogue(file);
//
//	    return ResponseEntity.status(HttpStatus.OK).body("OK");
//	} catch (Exception e) {
//	    message = "FAIL to upload " + file.getOriginalFilename() + "!";
//	    e.printStackTrace();
//	    return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
//	}
//    }
//
//    @RequestMapping(value = "/booleanCatalogue", method = RequestMethod.GET)
//    @ResponseBody
//    public AbstractCatalogue getCatalogue() {
//	return fileService.getCatalogue();
//    }
//    
//    @RequestMapping(value = "/searchRequest", method = RequestMethod.POST)
//    @ResponseBody
//    public List<Map<String, String>> getSearchResults(@RequestBody List <Object> dataFromUi) throws MalformedBoolExpressionException {
//	
//	
//	return fileService.getSearchResults(dataFromUi);
//    }

}
