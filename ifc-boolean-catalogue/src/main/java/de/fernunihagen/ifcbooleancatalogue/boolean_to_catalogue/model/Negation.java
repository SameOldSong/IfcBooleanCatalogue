package de.fernunihagen.ifcbooleancatalogue.boolean_to_catalogue.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.metrics.IfcBenchmarkEnum;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.objectives.LogicalOperator;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.catalogue.Dependent;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.catalogue.Trigger;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractBoolExpression;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractNegation;

public class Negation extends AbstractNegation {
    

  
    public Negation(AbstractBoolExpression positivePart) {
	super(positivePart);
	// TODO Auto-generated constructor stub
    }

//    @Override
//    public List<Trigger> generateTriggers() {
//	return invertPositivePart().generateTriggers();
//    }
//
//    @Override
//    public List<Dependent> generateDependents() {
//	return invertPositivePart().generateDependents();
//    }
//
//    private AbstractBoolExpression invertPositivePart() {
//
//	if (positivePart instanceof AbstractSimpleBoolExpression)
//	    return invertSimpleExpression((AbstractSimpleBoolExpression) positivePart);
//
//	if (positivePart instanceof AbstractConjunction)
//	    return invertConjunction((AbstractConjunction) positivePart);
//
//	if (positivePart instanceof AbstractDisjunction)
//	    return invertDisjunction((AbstractDisjunction) positivePart);
//
//	return null;
//    }
//
//    // NOT(A OR B) is transformed into NOT A AND NOT B
//    private AbstractBoolExpression invertDisjunction(AbstractDisjunction negatedDisjunction) {
//
//	return new AbstractConjunction(createNegatedComponents(negatedDisjunction));
//    }
//
//    // NOT(A AND B) is transformed into NOT A OR NOT B
//    private AbstractBoolExpression invertConjunction(AbstractCompoundBoolExpression negatedConjunction) {
//	return new AbstractDisjunction(createNegatedComponents(negatedConjunction));
//    }
//
//    private List<AbstractBoolExpression> createNegatedComponents(AbstractCompoundBoolExpression negatedCompoundExpression) {
//	List<AbstractBoolExpression> negatedComponents = new ArrayList<>();
//	for (AbstractBoolExpression component : negatedCompoundExpression.getComponents())
//	    negatedComponents.add(new Negation(component));
//
//	return negatedComponents;
//    }
//
//    private AbstractSimpleBoolExpression invertSimpleExpression(AbstractSimpleBoolExpression negatedExpr) {
//	RelationalOperator newOperator = invertOperator(negatedExpr.getOperator());
//
//	return new AbstractSimpleBoolExpression(negatedExpr.getProperty(), newOperator, negatedExpr.getValue());
//    }
//
//    private RelationalOperator invertOperator(RelationalOperator operator) {
//	switch (operator) {
//	case GREATERTHAN:
//	    return RelationalOperator.LESSTHAN;
//
//	case GREATERTHANOREQUALTO:
//	    return RelationalOperator.LESSTHANOREQUALTO;
//
//	case LESSTHANOREQUALTO:
//	    return RelationalOperator.GREATERTHANOREQUALTO;
//
//	case LESSTHAN:
//	    return RelationalOperator.GREATERTHAN;
//
//	case EQUALTO:
//	    return RelationalOperator.NOTEQUALTO;
//
//	case NOTEQUALTO:
//	    return RelationalOperator.EQUALTO;
//
//	}
//
//	return null;
//    }
}
