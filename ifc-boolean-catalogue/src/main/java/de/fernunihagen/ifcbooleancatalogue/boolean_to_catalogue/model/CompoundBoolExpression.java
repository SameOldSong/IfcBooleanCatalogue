package de.fernunihagen.ifcbooleancatalogue.boolean_to_catalogue.model;

import java.util.ArrayList;
import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.objectives.LogicalOperator;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.catalogue.Dependent;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.catalogue.Trigger;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractBoolExpression;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractCompoundBoolExpression;

public class CompoundBoolExpression extends AbstractCompoundBoolExpression implements BoolExpression {
    private List<BoolExpression> components;

    @Override
    public List<? extends AbstractBoolExpression> getComponents() {
	return components;
    }

    public CompoundBoolExpression(LogicalOperator operator) {
	new CompoundBoolExpression(operator, new ArrayList<BoolExpression>());
    }
    
    public CompoundBoolExpression(LogicalOperator operator, List<BoolExpression> components) {
   	this.operator = operator;
   	this.components = components;
       }

    // TODO
//    @Override
//    public boolean disjunctiveNormalForm() {
//	if (this instanceof Conjunction && allComponentsSimpleExpressions())
//	    return true;
//
////	if (this instanceof Conjunction) {
////	    for (BoolExpression component : components)
////		if (!component.conjunctionOfSimpleExpressions() && !(component instanceof SimpleBoolExpression))
////		    return false;
////	    return true;
////	}
//
//	return false;
//
//    }

    @Override
    public List<Trigger> generateTriggers() {
	List<Trigger> triggers = new ArrayList<Trigger>();
	for (BoolExpression component : components)
	    triggers.addAll(component.generateTriggers());

	return triggers;
    }

    @Override
    public List<Dependent> generateDependents() {
	List<Dependent> dependents = new ArrayList<Dependent>();
	for (BoolExpression component : components)
	    dependents.addAll(component.generateDependents());

	return dependents;
    }

//    @Override
//    public void addComponent(AbstractBoolExpression component) {
//	List<? extends AbstractBoolExpression> expressions = getComponents();
//	expressions.add(component);
//	
//    }

    @Override
    public void setComponents(List<AbstractBoolExpression> components) {
	// TODO Auto-generated method stub
	
    }

    @Override
    public void addComponent(AbstractBoolExpression component) {
	// TODO Auto-generated method stub
	
    }

    @Override
    public boolean disjunctiveNormalForm() {
	// TODO Auto-generated method stub
	return false;
    }
}
