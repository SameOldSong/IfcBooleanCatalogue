package de.fernunihagen.ifcbooleancatalogue.boolean_to_catalogue.model;

import java.util.ArrayList;
import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.objectives.LogicalOperator;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.catalogue.Dependent;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.catalogue.Trigger;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractBoolExpression;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractSimpleBoolExpression;

public class Disjunction extends CompoundBoolExpression implements BoolExpression {

    
    
    public Disjunction(LogicalOperator operator) {
	super(operator);
	// TODO Auto-generated constructor stub
    }

//    @Override
//   public List<Dependent> generateDependents(){
//	List<Dependent> dependents = new ArrayList<Dependent>();
//	
//	if(allComponentsSimpleExpressions() && allComponentsHaveSameProperty()) {
//	    String property = ((AbstractSimpleBoolExpression)components.get(0)).getPropertyName();
//	    dependents.add(new Dependent(property, getValuesAsListOfStrings()));
//	}
//	return dependents;
//    }

    @Override
    public boolean disjunctiveNormalForm() {
	// TODO Auto-generated method stub
	return false;
    }

    @Override
    public List<Trigger> generateTriggers() {
	// TODO Auto-generated method stub
	return null;
    }
    

}
