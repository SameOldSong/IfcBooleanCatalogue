package de.fernunihagen.ifcbooleancatalogue.boolean_to_catalogue.web_service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.metrics.IfcBenchmarkEnum;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.objectives.LogicalOperator;
import de.fernunihagen.ifcbooleancatalogue.model.MissingProductException;
import de.fernunihagen.ifcbooleancatalogue.model.MissingPropertyException;
import de.fernunihagen.ifcbooleancatalogue.model.Product;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.FailureWhenParsingExpressionException;
import de.fernunihagen.ifcbooleancatalogue.shared.WebService;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.MalformedBoolExpressionException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers.StringHelper;
import de.fernunihagen.ifcbooleancatalogue.shared.model.AbstractProperty;
import de.fernunihagen.ifcbooleancatalogue.shared.model.Constants;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractBoolExpression;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractCompoundBoolExpression;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractSimpleBoolExpression;

@Component
public class BooleanToCatalogueService extends WebService implements Constants {
//    private AbstractCatalogue products;
//    private CatalogueGenerator catalogueGenerator;
//
//    public void generateCatalogue(MultipartFile file)
//	    throws IOException, MalformedBoolExpressionException, MissingPropertyException, FailureWhenParsingExpressionException, MissingProductException {
//	//catalogueGenerator = new CatalogueGenerator(convertMultiPartToFile(file));
//	//products = catalogueGenerator.generateCatalogue();
//
//    }
//
//    public AbstractCatalogue getCatalogue() {
//	return products;
//    }
//
//    

    public List<Map<String, String>> getSearchResults(List<Object> dataFromUi) throws MalformedBoolExpressionException {
//        Product product = products.getProduct((String) dataFromUi.get(0));
//	List<List<String>> selectedValuesList = (List<List<String>>) dataFromUi.get(1);
//	Map<String, String> selectedValues = new HashMap<String, String>();
//	for (List<String> map : selectedValuesList)
//	    selectedValues.put(map.get(0), map.get(1));
//
//	List<AbstractProperty> propertiesWithoutSelectedValue = product.getMissingProperties(new ArrayList<>(selectedValues.keySet()));
//	// list of maps
//	List<Map<String, String>> valueCombinations = StringHelper.generateValueCombinations(selectedValues,
//		propertiesWithoutSelectedValue);
//
//	List<Map<String, String>> validCombinations = catalogueGenerator.getBoolExpManager()
//		.getValidValueCombinations(valueCombinations, product);
//	return validCombinations;
	
	return null;

    }

   

}
