package de.fernunihagen.ifcbooleancatalogue.boolean_to_catalogue.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.metrics.IfcBenchmarkEnum;
import de.fernunihagen.ifcbooleancatalogue.model.Value;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.catalogue.Dependent;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.catalogue.Trigger;
import de.fernunihagen.ifcbooleancatalogue.shared.model.AbstractProperty;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractBoolExpression;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractSimpleBoolExpression;

public class SimpleBoolExpression extends AbstractSimpleBoolExpression implements BoolExpression{

    public SimpleBoolExpression(Property property, IfcBenchmarkEnum operator, String value) {
	super(property, operator, value);

    }
    
    public SimpleBoolExpression(Property property, IfcBenchmarkEnum operator, Value value) {
	super(property, operator, value);

    }

    /**
     * if operator is equal, returns list consisting of only the value of this
     * expression if operator is not equal, returns list of property values matching
     * the operator of this expression Example: for (Length > 80) returns all values
     * of property Length that are greater than 80
     */
    public List<Value> getValuesAfterConvertingToEqual() {
	if (operator.equals(IfcBenchmarkEnum.EQUALTO))
	    return new ArrayList<Value>(Arrays.asList(value));

	return ((Property) property).getValuesMatchingRule(operator, value);

    }
//    @Override
//    public List<Trigger> generateTriggers() {
//	List<Trigger> triggers = new ArrayList<Trigger>();
//	if (operator.equals(RelationalOperator.EQUALTO)) {
//	    triggers.add(new Trigger(new ArrayList<Value>(Arrays.asList(getValue()))));
//	    return triggers;
//	}
//
//	return convertOperatorToEqual().generateTriggers();
//
//    }

//    public AbstractBoolExpression convertOperatorToEqual() {
//	if (operator.equals(RelationalOperator.EQUALTO))
//	    return this;
//
//	List<AbstractBoolExpression> simples = new ArrayList<AbstractBoolExpression>();
//	for (Value value : getValuesAfterConvertingToEqual())
//	    simples.add(new SimpleBoolExpression(((Property)value.getProperty()), RelationalOperator.EQUALTO, value));
//	return new Disjunction(simples);
//    }

    @Override
    public List<Dependent> generateDependents() {
	List<String> values = new ArrayList<String>();

	for (Value value : getValuesAfterConvertingToEqual())
	    values.add(value.getText());

	List<Dependent> dependents = new ArrayList<>();
	dependents.add(new Dependent(property.getName(), values));
	return dependents;
    }

    @Override
    public boolean disjunctiveNormalForm() {
	// TODO Auto-generated method stub
	return false;
    }

    @Override
    public List<Trigger> generateTriggers() {
	// TODO Auto-generated method stub
	return null;
    }

}
