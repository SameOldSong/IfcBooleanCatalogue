package de.fernunihagen.ifcbooleancatalogue.boolean_to_catalogue.model;

import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.catalogue.Dependent;
import de.fernunihagen.ifcbooleancatalogue.model.boolean_expressions.catalogue.Trigger;
import de.fernunihagen.ifcbooleancatalogue.shared.model.boolean_file.bool_expressions.AbstractBoolExpression;

public interface BoolExpression extends AbstractBoolExpression {
   

    public abstract boolean disjunctiveNormalForm();

    public abstract List<Trigger> generateTriggers();

    public abstract List<Dependent> generateDependents();

}
