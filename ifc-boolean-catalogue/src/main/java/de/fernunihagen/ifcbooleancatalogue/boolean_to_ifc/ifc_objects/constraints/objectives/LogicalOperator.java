package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.objectives;

import java.util.Arrays;

public enum LogicalOperator {
    AND(".LOGICALAND."), OR(".LOGICALOR."), NOT(".LOGICALNOTAND.");

    String ifcString;

    private LogicalOperator(String ifcString) {
	this.ifcString = ifcString;
    }

    public String toIfcString() {
	return ifcString;
    }

    public static String[] getOperators(Class<? extends Enum<?>> e) {
	return Arrays.stream(e.getEnumConstants()).map(Enum::name).toArray(String[]::new);
    }
}
