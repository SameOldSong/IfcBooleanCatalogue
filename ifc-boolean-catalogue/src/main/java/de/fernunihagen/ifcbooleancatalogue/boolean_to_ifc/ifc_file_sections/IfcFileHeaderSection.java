package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class IfcFileHeaderSection implements IfcFileConstants {
    private String catalogueName;
    private String timestamp = getTimestamp();

    public IfcFileHeaderSection(String booleanFileName) {
	this.catalogueName = getCatalogueName(booleanFileName);
    }

    private String getCatalogueName(String booleanFileName) {
	return "'" + booleanFileName + "'";
    }

    private String getTimestamp() {
	return "'" + DateTimeFormatter.ofPattern(DATETIME_FORMAT).format(LocalDateTime.now()) + "'";
    }

    public String getLines() {

	String ifcBlock = STEPFILE_STANDARD;

	ifcBlock += STEPFILE_HEADER;
	ifcBlock += STEPFILE_DESC;

	ifcBlock += "FILE_NAME(" + catalogueName + COMMA + timestamp + COMMA + DEFAULT_FILE_INFO + ");" + NEW_LINE;

	ifcBlock += IFC_FILE_SCHEMA;

	ifcBlock += END_SECTION;

	return ifcBlock;
    }

}
