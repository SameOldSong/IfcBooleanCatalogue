package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.helpers;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.boolean_file.BooleanFileConstants;

public class StringHelper implements BooleanFileConstants {
    public static boolean isManufacturerLine(String line) {
	return line.contains(MANUFACTURER);
    }

    public static boolean isProductLine(String line) {
	return line.contains(PRODUCT);
    }
    
    public static String removeLastSymbol(String line) {
	return line.trim().substring(0, line.trim().length() - 1);
    }
}
