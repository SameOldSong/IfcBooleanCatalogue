package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.relations;

import java.util.Arrays;
import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.helpers.IfcLineFormatter;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.IfcLine;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.top_hierarchy_interfaces.IfcRoot;

public class IfcRelDeclares extends IfcLine implements IfcRoot {
    // #2 = IFCRELDECLARES('Z7vWlTPVtsX0lGwFa5wbC4', $, 'myRelDeclares_2', $,
    // #1,(#3));
    private String name;
    private Integer referenceToRelatingLine;
    private List<Integer> referenceToRelatedGroupOfLines;

    public IfcRelDeclares(int lineIndex, Integer referenceToRelatingLine,
	    List<Integer> referenceToRelatedGroupOfLines) {

	super(lineIndex);
	this.referenceToRelatingLine = referenceToRelatingLine;
	this.referenceToRelatedGroupOfLines = referenceToRelatedGroupOfLines;
    }

    @Override
    public void initLineComponents() {
	addAttributesInheritedFromIfcRoot();
	addRelatingContext();
	addRelatedDefinitions();
    }

    private void addRelatedDefinitions() {
	lineComponents.add(IfcLineFormatter.getListOfReferencesFromLineIndexes(referenceToRelatedGroupOfLines));

    }

    private void addRelatingContext() {
	lineComponents.add(IfcLineFormatter.getLineReference(referenceToRelatingLine));

    }

    @Override
    public void addName() {
	lineComponents.add(name);
    }

    @Override
    public String getIfcClassName() {
	return "IFCRELDECLARES";
    }

    public void setName(String name) {
        this.name = name;
    }

}
