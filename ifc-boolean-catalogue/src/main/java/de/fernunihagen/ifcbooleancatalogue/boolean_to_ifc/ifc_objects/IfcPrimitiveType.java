package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects;

public enum IfcPrimitiveType {
	REAL,
	STRING,
	BOOLEAN,
	INTEGER,
	NUMBER,
	BINARY,
	ARRAY_OF_REAL,
	LIST_OF_INTEGER
}
