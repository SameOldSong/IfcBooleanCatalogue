package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.objectives;

import java.util.ArrayList;
import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.helpers.IfcLineFormatter;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.helpers.PropertyHelper;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.IfcConstraint;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.metrics.IfcMetric;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.metrics.MetricData;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.MissingPropertyException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.ParsingException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.expressions.BoolExpressionInFile;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.expressions.CompoundBoolExpressionInFile;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.expressions.PropertyInBoolExpression;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.expressions.SimpleBoolExpressionInFile;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.products.ProductInFile;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.properties.Property;

public class IfcObjective extends IfcConstraint {
    // #301 = IFCOBJECTIVE($, $, .HARD., $, $, $, $, (#302, #353, #355, #357),
    // .LOGICALOR., .PARAMETER., $);

    protected LogicalOperator operator;
    protected List<IfcConstraint> components = new ArrayList<IfcConstraint>();
    private ProductInFile product;

    public IfcObjective(int lineIndex, BoolExpressionInFile expression, ProductInFile product) throws ParsingException {
	super(lineIndex);
	this.operator = getOperator(expression);
	this.product = product;

	initComponents(expression.getComponents());
    }

    private LogicalOperator getOperator(BoolExpressionInFile expression) {
	return LogicalOperator.valueOf(((CompoundBoolExpressionInFile) expression).toString());
    }

    private void initComponents(List<BoolExpressionInFile> components) throws ParsingException {
	for (BoolExpressionInFile expression : components)
	    addComponent(generateIfcConstraintFromExpressionInFile(expression, this.getNextLineIndex()));
    }

    private IfcConstraint generateIfcConstraintFromExpressionInFile(BoolExpressionInFile expression, int lineIndex)
	    throws ParsingException {
	if (expression instanceof SimpleBoolExpressionInFile)
	    return new IfcMetric(getNextLineIndex(), generateMetricData((SimpleBoolExpressionInFile)expression)); 	
	return new IfcObjective(lineIndex, expression, product);
    }

    private MetricData generateMetricData(SimpleBoolExpressionInFile expression) throws MissingPropertyException {
	Property property = findPropertyInHeader(expression.getProperty());
	String operator = expression.getOperator().toString();

	return new MetricData(property, expression.getValue(), operator);
    }

    private Property findPropertyInHeader(PropertyInBoolExpression propertyInBoolExpression)
	    throws MissingPropertyException {
	return PropertyHelper.findExistingProperty(propertyInBoolExpression, product.getProperties());
    }

    public void addComponent(IfcConstraint component) {
	component.joinIfcBlock(this);
	components.add(component);
    }

    @Override
    public String getIfcClassName() {
	return "IFCOBJECTIVE";
    }

    @Override
    public void initLineComponents() {
	addAttributesInheritedFromIfcConstraint();

	addBenchmarkValues();
	addLogicalAggregator();
	addObjectiveQualifier();
	addUserDefinedQualifier();

    }

    private void addUserDefinedQualifier() {
	lineComponents.add(null);

    }

    private void addObjectiveQualifier() {
	lineComponents.add(IfcLineFormatter.getFormattedEnum(IfcObjectiveEnum.USERDEFINED));

    }

    private void addLogicalAggregator() {
	lineComponents.add(operator.toIfcString());

    }

    private void addBenchmarkValues() {
	lineComponents.add(IfcLineFormatter.getListOfReferencesFromObjects(components));

    }

}
