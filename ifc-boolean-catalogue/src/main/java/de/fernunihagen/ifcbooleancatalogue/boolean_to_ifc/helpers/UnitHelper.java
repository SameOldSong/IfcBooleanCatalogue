package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.helpers;

import java.util.ArrayList;
import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.IfcLine;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.properties.IfcValueType;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units.IfcConversionBasedUnit;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units.IfcDerivedUnit;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units.IfcDerivedUnitElement;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units.IfcDerivedUnitEnum;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units.IfcSiPrefix;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units.IfcSiUnit;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units.IfcSiUnitName;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units.IfcUnit;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units.IfcUnitEnum;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.InvalidUnitException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers.BracketsHelper;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers.EnumHelper;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers.StringHelper;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.units.ConversionBasedUnitInBoolFile;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.units.DerivedUnitInBoolFile;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.units.SiUnitInBoolFile;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.units.UnitInBoolFile;
import de.fernunihagen.ifcbooleancatalogue.shared.model.Constants;

public class UnitHelper implements Constants {

    /**
     * creates IfcUnit object from the information about property in the file header
     * 
     * @param valTypeAndUnit: value type is for example IFCLABEL or
     *                        IFCLENGTHMEASURE; value unit is
     * @return corresponding IfcUnit object
     * @throws InvalidUnitException
     */
    public static IfcUnit createUnit(int lineIndex, UnitInBoolFile unitInFile) throws InvalidUnitException {
	if (unitInFile instanceof DerivedUnitInBoolFile)
	    return createDerivedUnit(unitInFile);
	if (unitInFile instanceof ConversionBasedUnitInBoolFile)
	    return new IfcConversionBasedUnit(lineIndex, (ConversionBasedUnitInBoolFile) unitInFile);

	if (unitInFile instanceof SiUnitInBoolFile)
	    return new IfcSiUnit(lineIndex, (SiUnitInBoolFile) unitInFile);

	// for non-measure types like IFCLABEL there is no unit
	return null;
    }

    // gets IfcDerivedUnitEnum like AREADENSITYUNIT from IfcValueType like
    // IFCAREADENSITYMEASURE
    private static IfcDerivedUnitEnum getDerivedUnitFromMeasure(String measureType) {
	for (IfcDerivedUnitEnum ifcUnit : IfcDerivedUnitEnum.values())
	    if (ifcUnit.getMeasure().equals(IfcValueType.valueOf(measureType)))
		return ifcUnit;

	return null;
    }

    // creates IfcConversionBasedUnit from String like
    // IFCAREADENSITYMEASURE(IFCDERIVEDDUNIT(KILOGRAM, 1, METRE, 2))
    private static IfcDerivedUnit createDerivedUnit(UnitInBoolFile unitInFile) throws InvalidUnitException {

//	// IFCDERIVEDDUNIT(KILOGRAM, 1, METRE, 2)
//	String unitString = BracketsHelper.getSubstringInBrackets(valTypeAndUnit);
//	ArrayList<IfcDerivedUnitElement> derivedElements = getDerivedElements(
//		BracketsHelper.getSubstringInBrackets(unitString));
//	// IFCAREADENSITYMEASURE
//	String measureType = valTypeAndUnit.replace(OPEN_BRACKET + unitString + CLOSE_BRACKET, "");
//
//	// AREADENSITYUNIT mapped from IFCAREADENSITYMEASURE
//	IfcDerivedUnitEnum type = getDerivedUnitFromMeasure(measureType);
//
//	IfcDerivedUnit unit = new IfcDerivedUnit(derivedElements, type);
//	return unit;

	return null;
    }

    // get derived unit elements from a string (KILOGRAM, 1, METRE, 2)
    private static ArrayList<IfcDerivedUnitElement> getDerivedElements(String elementsString)
	    throws InvalidUnitException {
//	if (elementsString.contains(IFCCONVERSIONBASEDUNIT))
//	    return getDerivedElementsWithConversionBasedUnit(elementsString);
//
//	ArrayList<IfcDerivedUnitElement> elements = new ArrayList<IfcDerivedUnitElement>();
//
//	String[] elementStrings = elementsString.split(COMMA);
//
//	// check there is an even number of elements
//	if (elementStrings.length % 2 != 0)
//	    throw new InvalidUnitException(
//		    "This definition of derived unit elements is not well-formed: " + elementsString);
//
//	for (int index = 0; index < elementStrings.length; index += 2) {
//	    // KILOGRAM
//	    IfcUnit unit = getUnitOfDerivedSiBasedElement(elementStrings[index].trim());
//
//	    // 1
//	    int exponent = Integer.parseInt(elementStrings[index + 1].trim());
//	    elements.add(new IfcDerivedUnitElement(unit, exponent));
//	}
//	return elements;
	return null;
    }

    private static ArrayList<IfcDerivedUnitElement> getDerivedElementsWithConversionBasedUnit(String elementsString)
	    throws InvalidUnitException {
	ArrayList<IfcDerivedUnitElement> elements = new ArrayList<IfcDerivedUnitElement>();

//	int index = 0;
//	String value = "";
//	while (index < elementsString.length()) {
//	    while (index < elementsString.length() && !StringHelper.isCurrentSymbolComma(elementsString, index)) {
//		value += elementsString.charAt(index);
//		index++;
//	    }
//	    IfcUnit unit;
//	    // get CONVERSIONBASEDUNIT
//	    if (value.contains(IFCCONVERSIONBASEDUNIT)) {
//		while (!StringHelper.isCurrentSymbolCloseBracket(elementsString, index)) {
//		    value += elementsString.charAt(index);
//		    index++;
//		}
//		value += elementsString.charAt(index);
//		index += 2;
//		// IFCCONVERSIONBASEDUNIT(BAR, PASCAL, 100000)
//		// derive IFCPRESSUREMEASURE from PASCAL
//		String baseUnitString = BracketsHelper.getSubstringInBrackets(value).trim().split(COMMA)[1].trim();
//		IfcValueType baseUnitType = IfcSiUnitName.valueOf(baseUnitString).getMeasure().getMeasure();
//
//		// add IFCPRESSUREMEASURE
//		unit = new IfcConversionBasedUnit(baseUnitType.toString() + OPEN_BRACKET + value + CLOSE_BRACKET);
//
//	    } else
//		// get SI UNIT
//		unit = getUnitOfDerivedSiBasedElement(value.trim());
//
//	    index++;
//
//	    String exponent = "";
//	    while (index < elementsString.length() && !StringHelper.isCurrentSymbolComma(elementsString, index)) {
//		exponent += elementsString.charAt(index);
//		index++;
//	    }
//	    elements.add(new IfcDerivedUnitElement(unit, Integer.parseInt(exponent.trim())));
//	    index++;
//	    value = "";
//
//	}

	return elements;
    }

    private static IfcUnit getUnitOfDerivedSiBasedElement(String unitString) throws InvalidUnitException {

//	// KILLO
//	String prefix = SiUnitHelper.getPrefixSubstring(unitString);

	// GRAM
	// IfcSiUnitName unitName = SiUnitHelper.getSiUnitName(unitString, prefix);
	// IfcValueType baseUnitType = unitName.getMeasure().getMeasure();
	// return new IfcSiUnit(baseUnitType + String.valueOf(OPEN_BRACKET) + unitString
	// + CLOSE_BRACKET);
	return null;
    }

    public static IfcUnit findMatchingGlobalUnit(List<IfcUnit> globalUnits, IfcUnit unit) {
	for (IfcUnit globalUnit : globalUnits)
	    if (globalUnit.equals(unit))
		return globalUnit;
	return null;
    }

}
