package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.helpers.SiUnitHelper;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.InvalidUnitException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.units.SiUnitInBoolFile;

/**
 * represents standard (SI) measure units
 * 
 * @author Tatiana Nikolaeva
 *
 */
public class IfcSiUnit extends IfcUnit {
    // #6 = IFCSIUNIT(*, .LENGTHUNIT., .CENTI., .METRE.);
    IfcUnitEnum type;
    IfcSiPrefix prefix;
    IfcSiUnitName name;

    public IfcSiUnit(int lineIndex, SiUnitInBoolFile unitInFile) throws InvalidUnitException {
	super(lineIndex);
	// MILLIMETRE
	String unitString = unitInFile.getUnitName();

	// MILLI
	prefix = SiUnitHelper.getSiPrefix(unitString);

	// METRE
	name = SiUnitHelper.getSiUnitName(unitString);

	// IFCLENGTHUNIT has to be mapped from IFCLENGTHMEASURE
	type = SiUnitHelper.getSiUnitType(unitInFile);
    }

    /**
     * components of IFCSIUNIT record in IFC file
     */
    @Override
    public void initLineComponents() {

	lineComponents.add(ASTERISK);
	lineComponents.add(PERIOD + type + PERIOD);
	if (prefix == null)
	    lineComponents.add(null);
	else
	    lineComponents.add(PERIOD + prefix + PERIOD);

	lineComponents.add(PERIOD + name + PERIOD);

    }

    /**
     * for ex., LENGTHUNIT
     * 
     * @return
     */
    public IfcUnitEnum getUnitType() {
	return type;
    }

    /**
     * unit prefix, unit name without prefix, for ex., MILLI in MILLIMETRE
     * 
     * @return
     */
    public IfcSiPrefix getPrefix() {
	return prefix;
    }

    /**
     * unit name without prefix, for ex., METRE in MILLIMETRE
     * 
     * @return
     */
    public IfcSiUnitName getName() {
	return name;
    }

    /**
     * for ex., LENGTHUNIT
     */
    @Override
    public String getTypeAsString() {
	return type.name();
    }

    /**
     * for ex., MILLIMETRE
     */
    @Override
    public String getCompleteName() {
	String unitPrefix;
	if (prefix == null)
	    unitPrefix = "";
	else
	    unitPrefix = prefix.name();
	return unitPrefix + name.name();
    }

    /**
     * for creating a line in IFC file
     */
    @Override
    public String getIfcClassName() {
	return "IFCSIUNIT";
    }

    @Override
    public boolean equals(Object anotherUnit) {
	if (notIfcSiUnit(anotherUnit))
	    return false;
	if (differentType((IfcSiUnit) anotherUnit))
	    return false;
	if (differentPrefix((IfcSiUnit) anotherUnit))
	    return false;
	if (differentName((IfcSiUnit) anotherUnit))
	    return false;
	return true;
    }

    private boolean differentName(IfcSiUnit anotherUnit) {
	return !anotherUnit.getName().toString().equals(this.getName().toString());
    }

    private boolean differentPrefix(IfcSiUnit anotherUnit) {
	if (anotherUnit.getPrefix() == null && this.getPrefix() != null)
	    return true;
	if (anotherUnit.getPrefix() != null && this.getPrefix() == null)
	    return true;
	return !anotherUnit.getPrefix().toString().equals(this.getPrefix().toString());
    }

    private boolean differentType(IfcSiUnit anotherUnit) {
	return !anotherUnit.getTypeAsString().equals(this.getTypeAsString());
    }

    private boolean notIfcSiUnit(Object anotherUnit) {
	return !(anotherUnit instanceof IfcSiUnit);
    }

}
