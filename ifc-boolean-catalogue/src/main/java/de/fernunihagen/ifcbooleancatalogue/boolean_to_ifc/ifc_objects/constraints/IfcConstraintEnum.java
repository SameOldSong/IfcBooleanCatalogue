package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.helpers.IfcLineFormatter;

public enum IfcConstraintEnum {
    HARD, SOFT, ADVISORY, USERDEFINED, NOTDEFINED;
    
    @Override
    public String toString() {
	return IfcLineFormatter.getFormattedEnum(this);
    }
}
