package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.boolean_file;

import de.fernunihagen.ifcbooleancatalogue.shared.model.Constants;

public interface BooleanFileConstants extends Constants {
    final String MANUFACTURER = "Hersteller";
    final String PRODUCT = "Produkt";
    
}