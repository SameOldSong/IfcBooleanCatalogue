package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.IfcFileDataSection;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.IfcFileHeaderSection;
import de.fernunihagen.ifcbooleancatalogue.model.MissingProductException;
import de.fernunihagen.ifcbooleancatalogue.model.MissingPropertyException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.BoolFileParser;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.InvalidUnitException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.LineMissingInBooleanFileException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.MalformedBoolExpressionException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.ParsingException;
import de.fernunihagen.ifcbooleancatalogue.shared.model.Constants;

public class IfcFileGenerator implements Constants {
    private File inputFile;

    public IfcFileGenerator(File inputFile) throws IOException {
	this.inputFile = inputFile;
	// catalogueGenerator = new CatalogueGenerator(inputFile);
    }

    public void generateIFC(String ifcFilePath) throws IOException, LineMissingInBooleanFileException, ParsingException {

	//Catalogue catalogue = new Catalogue(inputFile.getName(), BooleanFileParser.parse(inputFile));

	
	IfcFileHeaderSection header = new IfcFileHeaderSection(inputFile.getName());
	IfcFileDataSection data = new IfcFileDataSection(inputFile);
//	 String ifcConstraints = new
//	IfcObjectiveGenerator(catalogue).convertCatalogueToIfc();
//
//	   private String createRelationToIfcConstraints() {
//		// IFCRELASSOCIATESCONSTRAINT('ipbxOZdEo6idAFU6CAbiry', $, 'radiator', $,
//		// (#100), $, #200);
//
//		return new IfcRelAssociatesConstraint(getNextLineIndex(), product.getName(), getMyLineIndex(),
//			product.getMainExpression().getLineIndex()).toIfcLine();
//	    }

	writeIfcFile(ifcFilePath, header.getLines(), data.print());
    }

   

    private void writeIfcFile(String outputFilePath, String ifcHeader, String ifcBlocksForConstraints)
	    throws FileNotFoundException, IOException {
	FileWriter fileWriter = new FileWriter(outputFilePath, false);
	Writer writer = new BufferedWriter(fileWriter);

	writer.write(ifcHeader);
	writer.write(ifcBlocksForConstraints);
	writer.write(IFC_FILE_END);

	writer.close();

    }

}
