package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.metrics;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.helpers.IfcLineFormatter;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.IfcConstraint;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.properties.IfcValueType;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units.IfcMeasureWithUnit;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.InvalidUnitException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.ParsingException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.expressions.RelationalOperatorInFile;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.expressions.SimpleBoolExpressionInFile;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.properties.Property;

public class IfcMetric extends IfcConstraint {
    // #424 = IFCMETRIC('Speicherserie', $, .HARD., $, $, $, $, .EQUALTO., $,
    // IFCLABEL('Heizwasser/Elektro'), #364);

    private Property property;
    private IfcBenchmarkEnum operator;
    private String value;
    private IfcReference reference;
    private MetricData metricData;

    public IfcMetric(int lineIndex, MetricData metricData) {
	super(lineIndex);
	this.metricData = metricData;
	this.property = metricData.getProperty();
	this.value = metricData.getValue();
	this.operator = getOperator(metricData.getOperator());
	createReferencePath();
    }

    private void createReferencePath() {
	if (referencePathToPropertyDoesNotExist())
	    property.setFirstReferenceInPathToProperty(createNewReferencePath());
	reference = property.getFirstReferenceInPathToProperty();
    }

    private IfcReference createNewReferencePath() {
	IfcReference reference = new IfcReference(getNextLineIndex(), "IfcProxy", "IsDefinedBy", null, null,
		getNextLineIndex() + 1);
	reference.joinIfcBlock(this);

	IfcReference relDefinesByPropertiesRef = new IfcReference(getNextLineIndex(), "IfcRelDefinesByProperties",
		"RelatingPropertyDefinition", null, null, getNextLineIndex() + 1);
	relDefinesByPropertiesRef.joinIfcBlock(this);

	IfcReference propertySetRef = new IfcReference(getNextLineIndex(), "IfcPropertySet", "HasProperties",
		property.getName(), null, getNextLineIndex() + 1);
	propertySetRef.joinIfcBlock(this);

	IfcReference propertySingleValueRef = new IfcReference(getNextLineIndex(), "IfcPropertySingleValue",
		"NominalValue", property.getName(), null, null);
	propertySingleValueRef.joinIfcBlock(this);

	return reference;
    }

    private IfcBenchmarkEnum getOperator(String operator) {
	return IfcBenchmarkEnum.valueOf(operator);
    }

    @Override
    public void initLineComponents() throws ParsingException{
	addAttributesInheritedFromIfcConstraint();

	addBenchmark(); // this is the comparison operator
	addValueSource();

	addDataValue();// this is value to be compared to
	addReferencePath();

    }

    private void addReferencePath() {
	lineComponents.add(reference.getReference());
    }

    private boolean referencePathToPropertyDoesNotExist() {
	return property.getFirstReferenceInPathToProperty() == null;
    }

    // this is value to be compared to
    private void addDataValue() throws InvalidUnitException {
	if (property.hasNoUnit() || property.hasGlobalUnit())
	    lineComponents.add(createFormattedValueWithMeasureType());
	else
	    createMeasureWithUnitAndAddToIfcBlock();

    }

    private void createMeasureWithUnitAndAddToIfcBlock() throws InvalidUnitException {

	IfcMeasureWithUnit measureWithUnit = new IfcMeasureWithUnit(getNextLineIndex(), metricData);
	measureWithUnit.joinIfcBlock(this);
	lineComponents.add(measureWithUnit.getReference());

    }

    private String createFormattedValueWithMeasureType() {
	String formattedValue = IfcLineFormatter.getFormattedValue(IfcValueType.valueOf(property.getMeasureType()),
		value);
	return property.getMeasureType() + OPEN_BRACKET + formattedValue + CLOSE_BRACKET;
    }

    private void addValueSource() {
	lineComponents.add(null);

    }

    // this is the comparison operator
    private void addBenchmark() {
	lineComponents.add(operator.toIfcString());

    }

    @Override
    public String getIfcClassName() {
	return "IFCMETRIC";
    }

}
