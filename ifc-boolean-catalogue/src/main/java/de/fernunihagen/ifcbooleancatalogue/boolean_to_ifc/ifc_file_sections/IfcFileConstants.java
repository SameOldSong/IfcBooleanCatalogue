package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections;

import de.fernunihagen.ifcbooleancatalogue.shared.model.Constants;

public interface IfcFileConstants extends Constants {
    final String STEPFILE_STANDARD = "ISO-10303-21;" + NEW_LINE;
    final String STEPFILE_HEADER = "HEADER;" + NEW_LINE;

    final String STEPFILE_DESC = "FILE_DESCRIPTION($,'2;1');" + NEW_LINE;

    final String DATETIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    final String DEFAULT_FILE_INFO = "(''),(''),'','manual',''"; // does not change from file to file

    final String IFC_FILE_SCHEMA = "FILE_SCHEMA(('IFC4'));" + NEW_LINE;

    final String END_SECTION = "ENDSEC;" + DOUBLE_NEW_LINE;

    final int FIRST_IFC_LINE_INDEX = 1;

    final int GLOBAL_ID_LENGTH = 22;
}
