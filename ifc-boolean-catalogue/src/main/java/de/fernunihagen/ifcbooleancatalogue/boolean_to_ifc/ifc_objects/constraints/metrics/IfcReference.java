package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.metrics;

import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.helpers.IfcLineFormatter;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.IfcLine;

public class IfcReference extends IfcLine {

    private String typeIdentifier;
    private String attributeIdentifier;
    private String instanceName;
    private Integer listPositions;
    private Integer innerReference;

    public IfcReference(int lineIndex, String typeIdentifier, String attributeIdentifier, String instanceName,
	    Integer listPositions, Integer innerReference) {
	super(lineIndex);
	this.typeIdentifier = typeIdentifier;
	this.attributeIdentifier = attributeIdentifier;
	this.instanceName = instanceName;
	this.listPositions = listPositions;
	this.innerReference = innerReference;
    }

    @Override
    public String getIfcClassName() {
	return getClass().getSimpleName().toUpperCase();
    }

    @Override
    public List<String> getLineComponents() {
	return lineComponents;
    }

    @Override
    public void initLineComponents() {
	addTypeIdentifier();
	addAttributeIdentifier();
	addInstanceName();
	addListPositions();
	addInnerReference();

    }

    private void addInnerReference() {
	lineComponents.add(getInnerReference());

    }

    private void addListPositions() {
	lineComponents.add(listPositions.toString());
    }

    private void addInstanceName() {
	lineComponents.add(instanceName);
    }

    private void addAttributeIdentifier() {
	lineComponents.add(attributeIdentifier);
    }

    private void addTypeIdentifier() {
	lineComponents.add(typeIdentifier);
    }

    private String getInnerReference() {
	if (innerReference != null)
	    return (OPEN_BRACKET + innerReference + CLOSE_BRACKET);
	return null;
    }

}
