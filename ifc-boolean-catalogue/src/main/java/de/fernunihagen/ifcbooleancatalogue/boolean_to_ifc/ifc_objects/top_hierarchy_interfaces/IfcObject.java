package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.top_hierarchy_interfaces;

import java.util.List;

public interface IfcObject extends IfcRoot {

    public List<String> getLineComponents();

    public default void addAttributesInheritedFromIfcObject() {
	addAttributesInheritedFromIfcRoot();
	addObjectType();
    }

    public default void addObjectType() {
	getLineComponents().add(null);
    }

}
