package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects;

import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.helpers.IfcLineFormatter;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.IfcLine;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.IfcLineBlock;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.top_hierarchy_interfaces.IfcContext;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units.IfcUnit;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units.IfcUnitAssignment;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.BoolFileContent;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.InvalidUnitException;

public class IfcProject extends IfcLineBlock implements IfcContext {
    private String projectName;
    private IfcUnitAssignment unitAssignment;

    public IfcProject(int lineIndex, BoolFileContent fileContent) throws InvalidUnitException {
	super(lineIndex);
	projectName = fileContent.getProjectName();

	if (fileContent.unitsExist())
	    createIfcUnitAssignmentWithGlobalUnitsAndAddToIfcBlock(fileContent);
    }

    private void createIfcUnitAssignmentWithGlobalUnitsAndAddToIfcBlock(BoolFileContent fileContent)
	    throws InvalidUnitException {

	unitAssignment = new IfcUnitAssignment(getNextLineIndex(), fileContent.getGlobalUnits());
	unitAssignment.joinIfcBlock(this);
    }

    @Override
    public void initLineComponents() {
	// #1 = IFCPROJECT('Gfzwmps02Kd4igYmbjaDXa', $, 'radiatorProject', $, $, $, $,
	// $, #5);
	addAttributesInheritedFromIfcContext();
    }

    @Override
    public void addName() {
	lineComponents.add(projectName);

    }

    @Override
    public void addUnitAssignment() {
	if (unitAssignment != null)
	    lineComponents.add(IfcLineFormatter.getLineReference(unitAssignment.getMyLineIndex()));
	else
	    lineComponents.add(null);

    }

    @Override
    public String getIfcClassName() {
	return "IFCPROJECT";
    }

    public List<IfcUnit> getGlobalUnits() {
	return unitAssignment.getGlobalUnits();
	
    }

}
