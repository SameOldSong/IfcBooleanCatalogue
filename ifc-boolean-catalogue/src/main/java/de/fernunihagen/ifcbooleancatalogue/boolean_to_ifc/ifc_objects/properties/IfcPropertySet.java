package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.properties;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.helpers.IfcLineFormatter;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.helpers.UnitHelper;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.IfcLineBlock;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.top_hierarchy_interfaces.IfcRoot;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units.IfcUnit;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.InvalidUnitException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.products.ProductInFile;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.properties.Property;

public class IfcPropertySet extends IfcLineBlock implements IfcRoot {
    // #1120 = IFCPROPERTYSET('BFmcZ527gnvedqZLFbqRpl', $, 'myPropertySet', $,
    // (#1121));
    private final String PROPERTY_SET_OF = "property set of ";
    private ProductInFile productInFile;
    private List<IfcProperty> properties = new ArrayList<>();

    public IfcPropertySet(int lineIndex, ProductInFile productInFile) throws InvalidUnitException {
	super(lineIndex);
	this.productInFile = productInFile;
	createPropertiesAndAddToIfcBlock();
    }

    private void createPropertiesAndAddToIfcBlock() throws InvalidUnitException {
	for (Property propertyInFile : productInFile.getProperties())
	    createPropertyAddToIfcBlockAndToProperties(propertyInFile);
    }

    private void createPropertyAddToIfcBlockAndToProperties(Property propertyInFile) throws InvalidUnitException {
	if (propertyInFile.isUsedInBoolExpressions()) {
	    IfcProperty property = new IfcPropertySingleValue(getNextLineIndex(), propertyInFile);
	    properties.add(property);
	    property.joinIfcBlock(this);
	}
    }

    @Override
    public void initLineComponents() {
	addAttributesInheritedFromIfcRoot();
	addReferencesToProperties();
    }

    private void addReferencesToProperties() {
	lineComponents.add(IfcLineFormatter.getListOfReferencesFromObjects(properties));

    }

    @Override
    public void addName() {
	lineComponents.add(PROPERTY_SET_OF + productInFile.getProductName());
    }

    @Override
    public String getIfcClassName() {
	return "IFCPROPERTYSET";
    }

    public List<IfcProperty> getPropertiesWithGlobalUnit() {
	return properties.stream().filter(property -> property.hasGlobalUnit()).collect(Collectors.toList());
    }

    public void addReferencesOfGlobalUnitsToProperties(List<IfcUnit> globalUnits) {
	for (IfcProperty property : getPropertiesWithGlobalUnit()) {
	    PropertyWithUnitReference propertyWithRef = ((PropertyWithUnitReference) property);
	    propertyWithRef.setIfcUnit(UnitHelper.findMatchingGlobalUnit(globalUnits, propertyWithRef.getIfcUnit()));

	}
    }
}
