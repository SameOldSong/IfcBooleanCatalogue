package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.top_hierarchy_interfaces;

import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.IfcFileConstants;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers.StringHelper;

public interface IfcRoot extends IfcFileConstants {

    public List<String> getLineComponents();

    public default void addAttributesInheritedFromIfcRoot() {
	addGlobalId();
	addOwnerHistory();
	addName();
	addDescription();
    }

    public default void addGlobalId() {
	getLineComponents().add(StringHelper.randomAlphaNumeric(GLOBAL_ID_LENGTH));
    }

    public default void addOwnerHistory() {
	getLineComponents().add(null);
    }

    public default void addName() {
	getLineComponents().add(null);
    }

    public default void addDescription() {
	getLineComponents().add(null);
    }

}
