package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.properties;

import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.IfcLine;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.IfcLineBlock;
import de.fernunihagen.ifcbooleancatalogue.shared.model.AbstractProperty;

/**
 * in the context of this work the class is only used to represent components of
 * Article number
 * 
 * @author Tatiana Nikolaeva
 *
 */
public class IfcPropertyListValue extends IfcProperty {

    String propertyName;
    String unitAssignment;
    AbstractProperty property;
    List<String> values;

    public IfcPropertyListValue(int lineIndex, List<String> values) {
	super(lineIndex);
	this.propertyName = ARTICLE_NUM;
	this.values = values;
	this.lineIndex = lineIndex;

    }

    @Override
    public void initLineComponents() {
	// IFCPROPERTYLISTVALUE('ArticleNum', $, (), $);

	lineComponents.add(propertyName);
	lineComponents.add(null);
	lineComponents.add(buildListValue());
	lineComponents.add(null);

    }

    private String buildListValue() {
	String listValue = OPEN_BRACKET;
	for (String val : this.values)
	    listValue += "IFCLABEL" + OPEN_BRACKET + SINGLE_QOUTE + val + SINGLE_QOUTE + CLOSE_BRACKET + COMMA;
	return listValue += CLOSE_BRACKET;
    }

    @Override
    public String getIfcClassName() {
	return "IFCPROPERTYLISTVALUE";
    }

    @Override
    public boolean hasGlobalUnit() {
	// TODO Auto-generated method stub
	return false;
    }
}
