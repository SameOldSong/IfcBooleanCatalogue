package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.IfcProject;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.IfcProjectLibrary;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.objectives.IfcObjective;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.relations.IfcRelDeclares;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units.IfcUnit;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.BoolFileContent;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.InvalidUnitException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.ParsingException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.products.ProductInFile;

public class IfcFileDataSection extends IfcLineBlock implements IfcFileConstants {

    private BoolFileContent productsInFile;
    private IfcProject project;
    private IfcProjectLibrary library;
    private List<IfcObjective> mainObjectives;

    public IfcFileDataSection(File inputFile) throws IOException, ParsingException {
	super(0);
	productsInFile = new BoolFileContent(inputFile);
	addIfcProjectAndLibraryToIfcBlock();
	addIfcObjectivesToIfcBlock();
	addRelationsToLinkIfcObjectivesWithIfcProxyProducts();

    }

    private void addRelationsToLinkIfcObjectivesWithIfcProxyProducts() {
	// TODO
//	new IfcRelAssociatesConstraint(getNextLineIndex(), library.getReferencesToProxyProducts(),
//		mainObjectives.getMyLineIndex());
    }

    private void addIfcObjectivesToIfcBlock() throws ParsingException {
	for (ProductInFile product : productsInFile.getProducts()) {
	    IfcObjective mainObjective = new IfcObjective(getNextLineIndex(), product.getMainExpression(), product);
	    mainObjectives.add(mainObjective);
	    mainObjective.joinIfcBlock(this);
	}
    }

    private void addIfcProjectAndLibraryToIfcBlock() throws InvalidUnitException {
	createIfcProjectAndAddToIfcBlock();
	createIfcProjectLibraryAndAddToIfcBlock();
	createProjectToLibraryRelationAndAddToIfcBlock();
    }

    private void createIfcProjectLibraryAndAddToIfcBlock() throws InvalidUnitException {
	library = new IfcProjectLibrary(getNextLineIndex(), productsInFile);
	library.joinIfcBlock(this);

    }

    private void createIfcProjectAndAddToIfcBlock() throws InvalidUnitException {
	project = new IfcProject(getNextLineIndex(), productsInFile);
	project.joinIfcBlock(this);

    }

    private void createProjectToLibraryRelationAndAddToIfcBlock() {
	IfcRelDeclares relation = new IfcRelDeclares(getNextLineIndex(), project.getMyLineIndex(),
		Arrays.asList(library.getMyLineIndex()));
	relation.setName("project-to-library relation");
	relation.joinIfcBlock(this);

    }

    @Override
    public String getIfcClassName() {
	return "";
    }

    public List<IfcUnit> getGlobalUnits() {
	return project.getGlobalUnits();
    }

    @Override
    public void initLineComponents() {
	// TODO Auto-generated method stub

    }

}
