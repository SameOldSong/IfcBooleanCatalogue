package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units;

public class IfcDerivedUnitElement {
	
	IfcUnit unit;
	int exponent; 
	
	public IfcDerivedUnitElement(IfcUnit unit, int exponent) {
		this.unit = unit;
		this.exponent = exponent;
	}

	public IfcUnit getUnit() {
		return unit;
	}

	public int getExponent() {
		return exponent;
	}
	
	

}
