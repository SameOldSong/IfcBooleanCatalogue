package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.top_hierarchy_interfaces;

import java.util.List;

public interface IfcProduct extends IfcObject {

    public List<String> getLineComponents();

    public default void addAttributesInheritedFromIfcProduct() {
	addAttributesInheritedFromIfcObject();
	addObjectPlacement();
	addRepresentation();
    }

    public default void addRepresentation() {
	getLineComponents().add(null);
    }

    public default void addObjectPlacement() {
	getLineComponents().add(null);
    }

}
