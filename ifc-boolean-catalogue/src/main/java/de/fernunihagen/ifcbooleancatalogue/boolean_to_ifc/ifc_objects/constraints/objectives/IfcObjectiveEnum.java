package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.objectives;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.helpers.IfcLineFormatter;

public enum IfcObjectiveEnum {
    CODECOMPLIANCE, CODEWAIVER, DESIGNINTENT, EXTERNAL, HEALTHANDSAFETY, MERGECONFLICT, MODELVIEW, PARAMETER,
    REQUIREMENT, SPECIFICATION, TRIGGERCONDITION, USERDEFINED, NOTDEFINED;
    
    @Override
    public String toString() {
	return IfcLineFormatter.getFormattedEnum(this);
    }
}
