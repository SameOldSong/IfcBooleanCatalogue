package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.relations;

import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.helpers.IfcLineFormatter;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.IfcLine;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.top_hierarchy_interfaces.IfcRoot;

public class IfcRelDefinesByProperties extends IfcLine implements IfcRoot {
    // IFCRELDEFINESBYPROPERTIES('6db38is4XOVJGeh1aFs5f4', $,
    // 'property set to product', $, (#100), #1120);
    private final String DESCRIPTION = "links property set to products";
    private List<Integer> lineIndexesOfProducts;
    private int propertySetLineIndex;

    public IfcRelDefinesByProperties(int lineIndex, List<Integer> lineIndexesOfProducts, int propertySetLineIndex) {
	super(lineIndex);

	this.propertySetLineIndex = propertySetLineIndex;
	this.lineIndexesOfProducts = lineIndexesOfProducts;
    }

    @Override
    public void initLineComponents() {

	addAttributesInheritedFromIfcRoot();
	addLineIndexesOfProducts();
	addPropertySetLineIndex();
    }

    @Override
    public void addDescription() {
	lineComponents.add(DESCRIPTION);

    }

    private void addPropertySetLineIndex() {
	lineComponents.add(IfcLineFormatter.getLineReference(propertySetLineIndex));
    }

    private void addLineIndexesOfProducts() {
	lineComponents.add(IfcLineFormatter.getListOfReferencesFromLineIndexes(lineIndexesOfProducts));
    }

    @Override
    public String getIfcClassName() {
	return "IFCRELDEFINESBYPROPERTIES";
    }

}
