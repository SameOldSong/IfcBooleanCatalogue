package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.properties;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.IfcLineBlock;

public abstract class IfcProperty extends IfcLineBlock {
   

    public IfcProperty(int lineIndex) {
	super(lineIndex);
    }
    
    public void addNameToAttributes(String name) {
	lineComponents.add(name);
    }

    public void addDescriptionToAttributes(String description) {
	lineComponents.add(description);
    }
    
    public abstract boolean hasGlobalUnit();
    
    

}
