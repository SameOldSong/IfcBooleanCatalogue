package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units;

public enum IfcSiPrefix {
	EXA,
	 PETA,
	 TERA,
	 GIGA,
	 MEGA,
	 KILO,
	 HECTO,
	 DECA,
	 DECI,
	 CENTI,
	 MILLI,
	 MICRO,
	 NANO,
	 PICO,
	 FEMTO,
	 ATTO
	 
	 
	
}
