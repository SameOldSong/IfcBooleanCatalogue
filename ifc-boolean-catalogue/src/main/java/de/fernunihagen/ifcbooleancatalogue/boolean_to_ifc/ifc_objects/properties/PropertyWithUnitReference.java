package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.properties;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units.IfcUnit;

public interface PropertyWithUnitReference {
    public void setIfcUnit(IfcUnit unit);
    public IfcUnit getIfcUnit();
}
