package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects;

import java.util.Arrays;
import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.IfcLineBlock;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.properties.IfcPropertySet;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.relations.IfcRelDefinesByProperties;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.top_hierarchy_interfaces.IfcProduct;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units.IfcUnit;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.InvalidUnitException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.products.ProductInFile;

public class IfcProxy extends IfcLineBlock implements IfcProduct {
    // #100 = IFCPROXY('ogfa5hMOV83k25LchdrHev', $, 'IFCSPACEHEATER', $, $, $, $, $,
    // $);
    private final String PROXY_FOR = "proxy object for ";
    private IfcPropertySet propertySet;
    private ProductInFile productInFile;

    public IfcProxy(int lineIndex, ProductInFile productInFile) throws InvalidUnitException {
	super(lineIndex);
	this.productInFile = productInFile;
	createIfcPropertySetAndAddToIfcBlock();
	addRelationToLinkIfcProxyToIfcPropertySet();
    }

    private void addRelationToLinkIfcProxyToIfcPropertySet() {
	// IFCRELDEFINESBYPROPERTIES('6db38is4XOVJGeh1aFs5f4', $,
	// 'myRelDefinesByProperties_1', $, (#100), #1120);
	IfcRelDefinesByProperties relation = new IfcRelDefinesByProperties(getNextLineIndex(),
		Arrays.asList(this.getMyLineIndex()), propertySet.getMyLineIndex());
	relation.joinIfcBlock(this);
    }

    private void createIfcPropertySetAndAddToIfcBlock() throws InvalidUnitException {
	propertySet = new IfcPropertySet(getNextLineIndex(), productInFile);
	propertySet.joinIfcBlock(this);
    }

    @Override
    public void initLineComponents() {

	addAttributesInheritedFromIfcProduct();
    }

    @Override
    public void addName() {
	lineComponents.add(PROXY_FOR + productInFile.getProductName());
    }

    @Override
    public String getIfcClassName() {
	return "IFCPROXY";
    }

    

    public boolean hasPropertiesWithGlobalUnits() {
	return propertySet.getPropertiesWithGlobalUnit().size() > 0;
    }
    
    public void addReferencesOfGlobalUnitsToProperties(List<IfcUnit> globalUnits) {
	propertySet.addReferencesOfGlobalUnitsToProperties(globalUnits);
    }

}
