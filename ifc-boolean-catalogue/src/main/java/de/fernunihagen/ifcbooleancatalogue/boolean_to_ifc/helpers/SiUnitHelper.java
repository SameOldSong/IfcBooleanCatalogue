package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.helpers;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.properties.IfcValueType;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units.IfcSiPrefix;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units.IfcSiUnitName;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units.IfcUnitEnum;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.InvalidUnitException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers.BoolLineHelper;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.helpers.EnumHelper;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.units.SiUnitInBoolFile;
import de.fernunihagen.ifcbooleancatalogue.shared.model.Constants;

public class SiUnitHelper implements Constants {
    // gets IfcSiPrefix from String like MILLIMETRE, if it exists
    private static String getPrefixSubstring(String unitString) {
	for (String ifcSiPrefix : EnumHelper.getEnumNames(IfcSiPrefix.class))
	    if (unitString.contains(ifcSiPrefix))
		return ifcSiPrefix;

	return null;
    }

    public static IfcSiUnitName getSiUnitName(String unitString) {
	String prefix = getPrefixSubstring(unitString);
	if (prefix != null)
	    return IfcSiUnitName.valueOf(unitString.replace(prefix, ""));
	else
	    return IfcSiUnitName.valueOf(unitString);
    }

    // gets prefix (MILLI), if exists
    public static IfcSiPrefix getSiPrefix(String unitString) {
	String prefixString = getPrefixSubstring(unitString);
	if (prefixString == null)
	    return null;
	if (prefixString.trim().length() == 0)
	    return null;

	return IfcSiPrefix.valueOf(prefixString);
    }

    // gets IfcUnitEnum like IFCLENGTHUNIT from IfcValueType like IFCLENGTHMEASURE
    // and checks that it matches IfcUnitEnum mapped from IfcSiUnitName
    public static IfcUnitEnum getSiUnitType(SiUnitInBoolFile unitInFile) throws InvalidUnitException {
	return getSiUnitType(unitInFile.getMeasureType(), unitInFile.getUnitName());
    }

    public static IfcUnitEnum getSiUnitType(String measureType, String unitNameString) throws InvalidUnitException {
	IfcSiUnitName unitName = getSiUnitName(unitNameString);
	// IFCLENGTHUNIT mapped from unit name METRE
	// matches the one derived from measure
	IfcUnitEnum unitMeasureFromUnitName = unitName.getMeasure();

	// IFCLENGTHUNIT mapped from IFCLENGTHMEASURE
	IfcUnitEnum unitType = getIfcUnitEnum(measureType);
	if (unitType.equals(unitMeasureFromUnitName))
	    return unitType;
	else
	    throw new InvalidUnitException("Specified unit type " + measureType + " does not match the unit " + unitName
		    + ". Expected type for this unit is " + unitMeasureFromUnitName);

    }

    /**
     * get MILLIMETRE from IFCLENGTHMEASURE(MILLIMETRE)
     * 
     * @param valTypeAndUnit
     * @return
     */
    public static String getUnitString(String valTypeAndUnit) {
	int firstBracket = BoolLineHelper.getFirstOpenBracketIndex(valTypeAndUnit);
	return valTypeAndUnit.substring(firstBracket + 1, valTypeAndUnit.length() - 1);
    }

    // gets IfcUnitEnum like IFCLENGTHUNIT from IfcValueType like IFCLENGTHMEASURE
    private static IfcUnitEnum getIfcUnitEnum(String measureType) throws InvalidUnitException {

	for (IfcUnitEnum ifcUnit : IfcUnitEnum.values())

	    if (ifcUnit.getMeasure().equals(IfcValueType.valueOf(measureType)))
		return ifcUnit;

	return null;
    }

}
