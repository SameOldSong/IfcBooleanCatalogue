package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.IfcLine;

public class IfcAppliedValue extends IfcLine {

    private String appliedValue;

    public IfcAppliedValue(int lineIndex, String appliedValue) {
	super(lineIndex);
	this.appliedValue = appliedValue;
    }

    @Override
    public void initLineComponents() {
	// #115 = IFCAPPLIEDVALUE($, $, #120, $, $, $, $, $, $, $);

	for (int counter = 0; counter < 2; counter++)
	    lineComponents.add(null);

	lineComponents.add(appliedValue);

	for (int counter = 0; counter < 7; counter++)
	    lineComponents.add(null);
    }

    @Override
    public String getIfcClassName() {
	return getClass().getSimpleName().toUpperCase();
    }

}
