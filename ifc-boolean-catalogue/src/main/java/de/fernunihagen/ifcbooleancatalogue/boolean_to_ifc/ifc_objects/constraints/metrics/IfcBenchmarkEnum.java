package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.metrics;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.helpers.IfcLineFormatter;

public enum IfcBenchmarkEnum {

    GREATERTHAN(">"), GREATERTHANOREQUALTO(">="), LESSTHAN("<"), LESSTHANOREQUALTO("<="), EQUALTO("="),
    NOTEQUALTO("!=");

    private String symbol;

    IfcBenchmarkEnum(String symbol) {
	this.symbol = symbol;
    }

    public String getSymbol() {
	return symbol;
    }

    public String toIfcString() {
	return IfcLineFormatter.getFormattedEnum(this);
    }

}
