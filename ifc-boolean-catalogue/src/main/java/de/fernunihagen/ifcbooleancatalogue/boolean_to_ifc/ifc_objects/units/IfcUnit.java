package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.IfcLineBlock;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.ParsingException;

public abstract class IfcUnit extends IfcLineBlock {
    private boolean global;

    protected IfcUnit(int lineIndex) {
	super(lineIndex);
    }

    public abstract String getTypeAsString();

    public abstract String getCompleteName();

    public boolean isGlobal() {
	return global;
    }

    public void setGlobal(boolean global) {
	this.global = global;
    }

    @Override
    public abstract boolean equals(Object anotherUnit);

    @Override
    public String print() throws ParsingException {
	return super.print().trim() + NEW_LINE;
    }
}
