package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units;

import java.util.ArrayList;
import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.ParsingException;

public class IfcDerivedUnit extends IfcUnit {
    List<IfcDerivedUnitElement> derivedUnitElements;

    IfcDerivedUnitEnum unitType; // ex.: AREADENSITYUNIT;
    // for printing into file
    int numLinesInIfcBlock;
    ArrayList<String> referencedLines;

    public IfcDerivedUnit(List<IfcDerivedUnitElement> derivedUnitElements, IfcDerivedUnitEnum unitType) {
	super(1);
	this.derivedUnitElements = derivedUnitElements;
	this.unitType = unitType;
    }

    public List<IfcDerivedUnitElement> getDerivedUnitElements() {
	return derivedUnitElements;
    }

    public IfcDerivedUnitEnum getUnitType() {
	return unitType;
    }

    @Override
    public String getTypeAsString() {
	return unitType.name();
    }

    @Override
    public String getCompleteName() {
	String name = "";
	for (IfcDerivedUnitElement element : derivedUnitElements)
	    name += element.getUnit().getCompleteName() + ASTERISK + element.getExponent() + FORWARD_SLASH;
	return name.substring(0, name.length() - 1);
    }

    @Override
    public void initLineComponents() {
	// #10=IFCDERIVEDUNIT((#11, #12, #13), .SPECIFICHEATCAPACITYUNIT., $);

	// references to elements
	String elements = OPEN_BRACKET;
	for (String ref : referencedLines)
	    elements += HASH + ref + COMMA + BLANK;

	elements = elements.trim().substring(0, elements.length() - 1);
	elements += CLOSE_BRACKET;

	lineComponents.add(elements);
	lineComponents.add(PERIOD + unitType + PERIOD);
	lineComponents.add(null);

    }

    @Override
    public String getIfcClassName() {
	return "IFCDERIVEDUNIT";
    }

    @Override
    public String print() throws ParsingException {
//		#10=IFCDERIVEDUNIT((#11, #12, #13), .SPECIFICHEATCAPACITYUNIT., $);
//		#11=IFCDERIVEDUNITELEMENT(#14, 1);
//		#12=IFCDERIVEDUNITELEMENT(#15, -1);
//		#13=IFCDERIVEDUNITELEMENT(#16, -1);
//		#14=IFCSIUNIT(*, .ENERGYUNIT., $, .JOULE.);
//		#15=IFCSIUNIT(*, .MASSUNIT., .KILO., .GRAM.);
//		#16=IFCSIUNIT(*, .THERMODYNAMICTEMPERATUREUNIT., $, .KELVIN.);

	String ifcBlock = "";
	numLinesInIfcBlock = 1;
	int index = lineIndex + 1;
	referencedLines = new ArrayList<String>();
	for (IfcDerivedUnitElement element : derivedUnitElements) {
	    referencedLines.add(String.valueOf(index));
	    if (element.getUnit() instanceof IfcSiUnit) {
		IfcSiUnit unit = (IfcSiUnit) element.getUnit();
		unit.setNextLineIndex(index);
		ifcBlock += unit.toIfcLine() + LINE_SEPARATOR;
		index++;
		numLinesInIfcBlock++;
	    } else {
		IfcConversionBasedUnit unit = (IfcConversionBasedUnit) element.getUnit();
		unit.setNextLineIndex(index);
		ifcBlock += unit.print() + LINE_SEPARATOR;
		index += 3;
		numLinesInIfcBlock += 3;
	    }

	}
	ifcBlock = this.toIfcLine() + LINE_SEPARATOR + ifcBlock;

	return ifcBlock;
    }

    @Override
    public boolean equals(Object anotherUnit) {
	// TODO Auto-generated method stub
	return false;
    }

}
