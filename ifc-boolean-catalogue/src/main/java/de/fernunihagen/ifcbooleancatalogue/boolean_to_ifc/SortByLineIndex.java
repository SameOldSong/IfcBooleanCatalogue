package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc;

import java.util.Comparator;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.IfcConstraint;


public class SortByLineIndex implements Comparator<IfcConstraint>{

	@Override
	public int compare(IfcConstraint con1, IfcConstraint con2) {
		return con1.getMyLineIndex() - con2.getMyLineIndex();
	}

}
