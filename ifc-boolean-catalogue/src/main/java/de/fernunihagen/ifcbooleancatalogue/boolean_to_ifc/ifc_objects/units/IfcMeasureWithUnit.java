package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.helpers.IfcLineFormatter;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.IfcLineBlock;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.metrics.MetricData;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.properties.IfcValueType;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.InvalidUnitException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.units.ConversionBasedUnitInBoolFile;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.units.SiUnitInBoolFile;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.units.UnitInBoolFile;

public class IfcMeasureWithUnit extends IfcLineBlock {

    ////////////// ATTRIBUTES FOR CONVERSION BASED UNIT //////////
    private IfcValueType measureType;// IFCLENGTHMEASURE
    private Double conversionFactor;// 25.4 (inch - mm) if class is used for conversion based unit to give the
    // conversion rate and its base.
    private IfcUnit baseUnit;
    ///////////////////////////////////////////////////////////////

    //////// COMMON ATTRIBUTES ///////////////
    String value; // if class is used for representing measure value together with its unit on the
    // entity type attribute level; thus overriding the IFC model global unit
    // assignments.

    public IfcMeasureWithUnit(int lineNumber, MetricData metricData) throws InvalidUnitException {
	super(lineNumber);

	// #115 = IFCMEASUREWITHUNIT(IFCLENGTHMEASURE(100.), #116);
	this.measureType = metricData.getMeasureType();
	this.value = metricData.getValue();

	// #116 = IFCSIUNIT(*, .LENGTHUNIT., .MILLI., .METRE.);
	// or conversion based unit or derived unit
	createUnitAndAddToIfcBlock(metricData.getUnitInBoolFile());

    }

    private void createUnitAndAddToIfcBlock(UnitInBoolFile unitInBoolFile) throws InvalidUnitException {
	if (unitInBoolFile instanceof SiUnitInBoolFile)
	    baseUnit = new IfcSiUnit(getNextLineIndex(), (SiUnitInBoolFile) unitInBoolFile);
	else if (unitInBoolFile instanceof ConversionBasedUnitInBoolFile)
	    baseUnit = new IfcConversionBasedUnit(getNextLineIndex(), (ConversionBasedUnitInBoolFile) unitInBoolFile);

	baseUnit.joinIfcBlock(this);
    }

    public IfcMeasureWithUnit(int lineNumber, ConversionBasedUnitInBoolFile unitInFile) throws InvalidUnitException {
	super(lineNumber);
	measureType = IfcValueType.valueOf(unitInFile.getMeasureType());

	// Conversion factor 100000
	conversionFactor = Double.parseDouble(unitInFile.getConversionFactor());

	createUnitAndAddToIfcBlock(unitInFile.getSiUnit());
    }

    @Override
    public void initLineComponents() {
	// #8=IFCMEASUREWITHUNIT(IFCLENGTHMEASURE(25.4), #9);
	if (conversionFactor != null)

	    lineComponents.add(measureType + OPEN_BRACKET + conversionFactor + CLOSE_BRACKET);
	else
	    lineComponents.add(measureType + OPEN_BRACKET + IfcLineFormatter.getFormattedValue(measureType, value)
		    + CLOSE_BRACKET);

	lineComponents.add(baseUnit.getReference());
    }

    @Override
    public String getIfcClassName() {
	return "IFCMEASUREWITHUNIT";
    }

    @Override
    public boolean equals(Object anotherMeasure) {

	if (differentMeasureType((IfcMeasureWithUnit) anotherMeasure))
	    return false;
	if (differentBaseUnit((IfcMeasureWithUnit) anotherMeasure))
	    return false;
	if (differentConversionFactor((IfcMeasureWithUnit) anotherMeasure))
	    return false;
	return true;
    }

    private boolean differentConversionFactor(IfcMeasureWithUnit anotherMeasure) {
	return !this.getConversionFactor().equals(anotherMeasure.getConversionFactor());
    }

    private boolean differentBaseUnit(IfcMeasureWithUnit anotherMeasure) {
	return !this.getBaseUnit().equals(anotherMeasure.getBaseUnit());
    }

    private boolean differentMeasureType(IfcMeasureWithUnit anotherMeasure) {
	return !this.getMeasureType().toString().equals(anotherMeasure.getMeasureType().toString());
    }

    public IfcValueType getMeasureType() {
	return measureType;
    }

    public Double getConversionFactor() {
	return conversionFactor;
    }

    public IfcUnit getBaseUnit() {
	return baseUnit;
    }

}
