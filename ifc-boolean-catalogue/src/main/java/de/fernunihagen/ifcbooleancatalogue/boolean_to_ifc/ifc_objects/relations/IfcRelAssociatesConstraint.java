package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.relations;

import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.helpers.IfcLineFormatter;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.IfcLine;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.top_hierarchy_interfaces.IfcRoot;

public class IfcRelAssociatesConstraint extends IfcLine implements IfcRoot {
    // #118 = IFCRELASSOCIATESCONSTRAINT('5w8cv5urqA7rcADfMX1TZv', $, 'IfcTank
    // Constraints', $, (#117), $, #1535);
    private final String RELATION_NAME = "connects proxy products with constraints";
    private List<Integer> lineIndexesOfProducts;
    private Integer lineIndexOfMainConstraint;

    public IfcRelAssociatesConstraint(int lineIndex, List<Integer> lineIndexesOfProducts,
	    Integer mainObjectiveLineIndex) {
	super(lineIndex);
	this.lineIndexesOfProducts = lineIndexesOfProducts;
	this.lineIndexOfMainConstraint = mainObjectiveLineIndex;
    }

    @Override
    public void initLineComponents() {
	addAttributesInheritedFromIfcRoot();

	addRelatedObjects();
	addIntent();
	addRelatingConstraint();

    }

    private void addRelatingConstraint() {
	lineComponents.add(IfcLineFormatter.getLineReference(lineIndexOfMainConstraint));
    }

    private void addIntent() {
	lineComponents.add(null);

    }

    private void addRelatedObjects() {
	lineComponents.add(IfcLineFormatter.getListOfReferencesFromLineIndexes(lineIndexesOfProducts));

    }

    @Override
    public void addName() {
	lineComponents.add(RELATION_NAME);
    }

    @Override
    public String getIfcClassName() {
	return "IFCRELASSOCIATESCONSTRAINT";
    }

}
