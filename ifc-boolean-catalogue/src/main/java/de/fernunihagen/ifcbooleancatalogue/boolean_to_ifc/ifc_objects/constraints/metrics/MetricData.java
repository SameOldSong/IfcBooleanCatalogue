package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints.metrics;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.properties.IfcValueType;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.properties.Property;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.units.UnitInBoolFile;

public class MetricData {
    private Property property;
    private String value;
    private String operator;
   
    
    public MetricData(Property property, String value, String operator) {
	super();
	this.property = property;
	this.value = value;
	this.operator = operator;
    }

    public Property getProperty() {
        return property;
    }

    public String getValue() {
        return value;
    }

    public String getOperator() {
        return operator;
    }

    public UnitInBoolFile getUnitInBoolFile() {
        return property.getUnit();
    }
    
    //IFCLENGTHMEASURE
    public IfcValueType getMeasureType() {
	return IfcValueType.valueOf(property.getUnit().getMeasureType());
    }
    
    
}
