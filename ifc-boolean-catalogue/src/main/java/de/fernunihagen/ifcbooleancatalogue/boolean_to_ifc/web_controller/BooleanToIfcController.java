package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.web_controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.web_service.BooleanToIfcService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class BooleanToIfcController {

    private final BooleanToIfcService fileService;

    @Autowired
    public BooleanToIfcController(BooleanToIfcService fileService) {
	this.fileService = fileService;
    }

    @PostMapping(value = "/uploadBooleanToIfc")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> handleFileUpload(@RequestParam("file") MultipartFile file) {
	String message = "";
	try {
	    String fileId = fileService.convertToIfc(file);

	    return ResponseEntity.status(HttpStatus.OK).body(fileId);
	} catch (Exception e) {
	    message = "FAIL to upload " + file.getOriginalFilename() + "!";
	    e.printStackTrace();
	    return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
	}
    }

    @RequestMapping(value = "/ifcfiles/{file_name}", method = RequestMethod.GET)
    @ResponseBody
    public FileSystemResource getFile(@PathVariable("file_name") String fileName) {
	return new FileSystemResource(fileService.getFileFor(fileName));
    }

}
