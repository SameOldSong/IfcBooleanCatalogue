package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.properties;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.helpers.IfcLineFormatter;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.helpers.UnitHelper;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units.IfcUnit;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.InvalidUnitException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.ParsingException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.properties.Property;

public class IfcPropertySingleValue extends IfcProperty implements PropertyWithUnitReference {
    // IFCPROPERTYSINGLEVALUE('Heizkoerperart', $, IFCLABEL('Flachheizkoerper'), $);
    private Property propertyInFile;
    // can be 1. global unit (in IfcUnitAssignment) or 2. to
    // local unit (in the next line) 3. or null if property is for ex. IFCLABEL
    private IfcUnit ifcUnit;

    public IfcPropertySingleValue(int lineIndex, Property propertyInFile) throws InvalidUnitException {
	super(lineIndex);
	this.propertyInFile = propertyInFile;
	if (propertyInFile.hasLocalUnit())
	    createLocalIfcUnitAndAddToIfcBlock();
    }

    private void createLocalIfcUnitAndAddToIfcBlock() throws InvalidUnitException {
	ifcUnit = UnitHelper.createUnit(getNextLineIndex(), propertyInFile.getUnit());
	ifcUnit.joinIfcBlock(this);
    }

    @Override
    public void initLineComponents() throws InvalidUnitException {
	super.addNameToAttributes(propertyInFile.getName());
	super.addDescriptionToAttributes(null);
	addNominalValue();
	addUnit();
    }

    private void addUnit() throws InvalidUnitException {
	if (ifcUnit == null)
	    lineComponents.add(null);
	else
	    lineComponents.add(ifcUnit.getReference());
    }

    private void addNominalValue() {
	lineComponents.add(buildValue());

    }

    private String buildValue() {

	String value = propertyInFile.getMeasureType() + OPEN_BRACKET;
	value += IfcLineFormatter.getFormattedValue(IfcValueType.valueOf(propertyInFile.getMeasureType()),
		propertyInFile.getRandomValue());
	return value += CLOSE_BRACKET;
    }

    @Override
    public String getIfcClassName() {
	return "IFCPROPERTYSINGLEVALUE";
    }

    public void setIfcUnit(IfcUnit unit) {
	this.ifcUnit = unit;

    }

    @Override
    public boolean hasGlobalUnit() {
	return ifcUnit != null && ifcUnit.isGlobal();
    }

    @Override
    public String print() throws ParsingException {
	return super.print().trim() + NEW_LINE;
    }

    @Override
    public IfcUnit getIfcUnit() {
	return ifcUnit;
    }

}
