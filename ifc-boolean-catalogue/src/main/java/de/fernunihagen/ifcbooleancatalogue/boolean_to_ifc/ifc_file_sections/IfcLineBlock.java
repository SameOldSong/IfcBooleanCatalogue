package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.InvalidUnitException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.ParsingException;

public abstract class IfcLineBlock extends IfcLine {
    protected int firstLineIndex;
    protected int nextLineIndex;
    protected List<IfcLine> ifcLinesOfComponents = new ArrayList<IfcLine>();

    public IfcLineBlock(int firstLineIndex) {
	super(firstLineIndex);
	setFirstLineIndex(firstLineIndex);
	setNextLineIndex(firstLineIndex + 1);
    }

    public void add(IfcLine component) {
	ifcLinesOfComponents.add(component);

    }

    @Override
    public int getMyLineIndex() {
	return firstLineIndex;
    }

    public void setFirstLineIndex(int firstLineIndex) {

	this.firstLineIndex = firstLineIndex;
    }

    public void setNextLineIndex(int updatedLineIndex) {

	this.nextLineIndex = updatedLineIndex;
    }

    public int getNextLineIndex() {
	return nextLineIndex;
    }

    @Override
    public String print() throws ParsingException {
	String block = this.toIfcLine();
	for (IfcLine component : ifcLinesOfComponents)
	    block += component.print();
	return block + DOUBLE_NEW_LINE;
    }

    @Override
    public void updateNextLineIndexOfIfcBlock(IfcLineBlock ifcBlock) {
	ifcBlock.setNextLineIndex(this.getNextLineIndex());
    }

}
