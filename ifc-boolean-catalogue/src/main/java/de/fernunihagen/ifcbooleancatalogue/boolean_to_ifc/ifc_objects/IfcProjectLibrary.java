package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.IfcFileDataSection;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.IfcLine;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.IfcLineBlock;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.relations.IfcRelDeclares;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.top_hierarchy_interfaces.IfcContext;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units.IfcUnit;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.BoolFileContent;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.InvalidUnitException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.products.ProductInFile;

public class IfcProjectLibrary extends IfcLineBlock implements IfcContext {
    // #3 = IFCPROJECTLIBRARY('k2tanFnCs5uvUqTP3gBlLw', $, $, $, $, $, $, $, $);
    private List<IfcLine> proxyProducts = new ArrayList<IfcLine>();

    public IfcProjectLibrary(int lineIndex, BoolFileContent productsInFile) throws InvalidUnitException {
	super(lineIndex);
	addIfcProxyProductsToIfcBlock(productsInFile);
	addRelationToLinkIfcProxyProductsToLibrary();
    }

    private void addIfcProxyProductsToIfcBlock(BoolFileContent productsInFile) throws InvalidUnitException {
	for (ProductInFile product : productsInFile.getProducts())
	    createIfcProxyProductAndAddToIfcBlock(product);
    }

    private void addRelationToLinkIfcProxyProductsToLibrary() {
	IfcRelDeclares relDeclares = new IfcRelDeclares(getNextLineIndex(), this.getMyLineIndex(),
		getLineNumbersOfProxies());
	relDeclares.setName("proxyProducts-to-library relation");
	relDeclares.joinIfcBlock(this);
    }

    private List<Integer> getLineNumbersOfProxies() {
	return proxyProducts.stream().map(product -> product.getMyLineIndex()).collect(Collectors.toList());
    }

    private void createIfcProxyProductAndAddToIfcBlock(ProductInFile productInFile) throws InvalidUnitException {
	IfcProxy proxy = new IfcProxy(getNextLineIndex(), productInFile);
	proxyProducts.add(proxy);
	proxy.joinIfcBlock(this);
    }

    @Override
    public void initLineComponents() {
	addAttributesInheritedFromIfcContext();
    }

    @Override
    public String getIfcClassName() {
	return "IFCPROJECTLIBRARY";
    }

    public List<IfcLine> getProxyProducts() {
	return proxyProducts;
    }

    @Override
    public void joinIfcBlock(IfcLineBlock block) {
	addReferencesOfGlobalUnitsToProperties(block);
	super.joinIfcBlock(block);
    }

    private void addReferencesOfGlobalUnitsToProperties(IfcLineBlock block) {
	List<IfcUnit> globalUnits = ((IfcFileDataSection) block).getGlobalUnits();
	for (IfcLine proxyProduct : proxyProducts) {
	    IfcProxy proxy = (IfcProxy) proxyProduct;
	    if (proxy.hasPropertiesWithGlobalUnits())
		proxy.addReferencesOfGlobalUnitsToProperties(globalUnits);
	}

    }

    public List<Integer> getReferencesToProxyProducts() {
	return proxyProducts.stream().map(product -> product.getMyLineIndex()).collect(Collectors.toList());
    }

}
