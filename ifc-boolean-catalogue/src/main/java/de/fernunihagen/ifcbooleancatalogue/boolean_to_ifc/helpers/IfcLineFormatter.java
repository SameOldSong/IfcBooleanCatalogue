package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.helpers;

import java.util.List;
import java.util.stream.Collectors;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.boolean_file.BooleanFileConstants;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.IfcLine;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.IfcPrimitiveType;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.properties.IfcProperty;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.properties.IfcValueType;

public class IfcLineFormatter implements BooleanFileConstants {

    public static String createIfcLine(List<String> printables, int lineIndex, String ifcClassName) {
	String objectString = "";

	objectString += HASH + lineIndex + "  =  " + ifcClassName + OPEN_BRACKET;

	for (int index = 0; index < printables.size(); index++) {
	    String printable = printables.get(index);
	    // add comma to separate from the previous parameter
	    if (index > 0)
		objectString += COMMA + BLANK;

	    // add parameter to the list
	    if (printables.get(index) == null)
		objectString += DOLLAR;
	    else if (isIfcObject(printable))
		objectString += printable;
	    else
		objectString += SINGLE_QOUTE + printable + SINGLE_QOUTE;

	    // if this is the last parameter - close the list
	    if (index == printables.size() - 1)
		objectString += CLOSE_BRACKET + SEMICOLON + LINE_SEPARATOR;
	}

	return objectString;
    }

    private static boolean isIfcObject(String printable) {
	if (printable.startsWith("IFC"))
	    return true;
	if (printable.startsWith(PERIOD))
	    return true;

	if (printable.startsWith(OPEN_BRACKET))
	    return true;

	if (printable.startsWith(HASH))
	    return true;
	if (printable.equals(ASTERISK))
	    return true;
	return false;
    }

    public static String getFormattedValue(IfcValueType type, String value) {
	if (value == null)
	    return "";
	if (type.getPrimType().equals(IfcPrimitiveType.STRING))
	    return SINGLE_QOUTE + value + SINGLE_QOUTE;
	if (type.getPrimType().equals(IfcPrimitiveType.BOOLEAN))
	    return createIfcBoolean(value);

	if (type.getPrimType().equals(IfcPrimitiveType.REAL))
	    if (!value.contains(PERIOD))
		return value + PERIOD;
	return value;
    }

    private static String createIfcBoolean(String value) {
	if (value.equalsIgnoreCase("TRUE"))
	    return ".T.";
	return ".F.";
    }

    public static String getLineReference(Integer number) {

	if (number != null)
	    return HASH + number;
	return DOLLAR;
    }

    public static String getListOfReferencesFromObjects(List<? extends IfcLine> ifcObjects) {
	// this string will have a redundant comma at the end
	String references = ifcObjects.stream().map(object -> HASH + object.getMyLineIndex() + COMMA + BLANK)
		.collect(Collectors.joining());

	return OPEN_BRACKET + StringHelper.removeLastSymbol(references) + CLOSE_BRACKET;
    }

    public static String getListOfReferencesFromLineIndexes(List<Integer> lineIndexes) {
	// this string will have a redundant comma at the end
	String references = lineIndexes.stream().map(index -> HASH + index + COMMA + BLANK)
		.collect(Collectors.joining());

	return OPEN_BRACKET + StringHelper.removeLastSymbol(references) + CLOSE_BRACKET;
    }
    
    public static String getFormattedEnum(Enum enumObject) {
	if(enumObject == null)
	    return DOLLAR;
	return PERIOD + enumObject.name() + PERIOD;
    }

}
