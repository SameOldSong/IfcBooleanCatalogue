package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.constraints;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.IfcLineBlock;

public abstract class IfcConstraint extends IfcLineBlock {

    public IfcConstraint(int firstLineIndex) {
	super(firstLineIndex);
    }

    public void addAttributesInheritedFromIfcConstraint() {
	addName();
	addDescription();
	addConstraintGrade(IfcConstraintEnum.HARD);
	addConstraintSource();
	addCreatingActor();
	addCreationTime();
	addUserDefinedGrade();
    }

    private void addUserDefinedGrade() {
	lineComponents.add(null);
	
    }

    private void addCreationTime() {
	lineComponents.add(null);
	
    }

    private void addCreatingActor() {
	lineComponents.add(null);
	
    }

    private void addConstraintSource() {
	lineComponents.add(null);
	
    }

    private void addConstraintGrade(IfcConstraintEnum grade) {
	lineComponents.add(grade.toString());
	
    }

    private void addDescription() {
	lineComponents.add(null);

    }

    private void addName() {
	lineComponents.add(null);
    }

}
