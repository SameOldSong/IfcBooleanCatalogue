package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.helpers;

import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.MissingPropertyException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.expressions.PropertyInBoolExpression;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.properties.Property;

public class PropertyHelper {
    
    public static boolean propertyExistsInHeader(String propName, List<Property> properties) {
	for (Property property : properties)
	    if (property.getName().equals(propName))
		return true;
	return false;
    }
    
    public static Property findExistingProperty(PropertyInBoolExpression prop, List<Property> properties) throws MissingPropertyException {
   	for (Property existingProp : properties)
   	    if (existingProp.getName().equals(prop.getName()))
   		return existingProp;
   	throw new MissingPropertyException(prop.getName());
       }

}
