package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.top_hierarchy_interfaces;

import java.util.List;

public interface IfcContext extends IfcRoot {

    public List<String> getLineComponents();

    public default void addAttributesInheritedFromIfcContext() {
	addAttributesInheritedFromIfcRoot();
	addObjectType();
	addLongName();
	addPhase();
	addRepresentationContexts();
	addUnitAssignment();

    }

    public default void addLongName() {
	getLineComponents().add(null);

    }

    public default void addObjectType() {
	getLineComponents().add(null);

    }

    public default void addRepresentationContexts() {
	getLineComponents().add(null);

    }

    public default void addPhase() {
	getLineComponents().add(null);

    }

    public default void addUnitAssignment() {
	getLineComponents().add(null);

    }

}
