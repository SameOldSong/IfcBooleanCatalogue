package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections;

import java.util.ArrayList;
import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.helpers.IfcLineFormatter;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.InvalidUnitException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.ParsingException;
import de.fernunihagen.ifcbooleancatalogue.shared.model.Constants;

public abstract class IfcLine implements Constants {
    protected List<String> lineComponents = new ArrayList<>();
    protected int lineIndex;

    public IfcLine(int lineIndex) {
	this.lineIndex = lineIndex;
    }

    public List<String> getLineComponents() {
	return lineComponents;
    }

    public abstract void initLineComponents() throws ParsingException;

    public String print() throws ParsingException {
	return toIfcLine();
    }

    public int getMyLineIndex() {
	return lineIndex;
    }

    public String toIfcLine() throws ParsingException {
	if (lineComponents.isEmpty())
	    initLineComponents();
	return IfcLineFormatter.createIfcLine(getLineComponents(), getMyLineIndex(), getIfcClassName());
    }

    public abstract String getIfcClassName();

    public void updateNextLineIndexOfIfcBlock(IfcLineBlock ifcBlock) {
	ifcBlock.setNextLineIndex(getMyLineIndex() + 1);
    }

    public void joinIfcBlock(IfcLineBlock block) {
	block.add(this);
	updateNextLineIndexOfIfcBlock(block);
    }

    public String getReference() {
	return IfcLineFormatter.getLineReference(getMyLineIndex());
    }

}
