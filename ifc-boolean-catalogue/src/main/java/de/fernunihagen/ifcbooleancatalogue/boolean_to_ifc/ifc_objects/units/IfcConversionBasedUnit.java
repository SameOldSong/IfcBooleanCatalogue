package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.helpers.IfcLineFormatter;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.helpers.SiUnitHelper;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.properties.IfcValueType;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.InvalidUnitException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.units.ConversionBasedUnitInBoolFile;

public class IfcConversionBasedUnit extends IfcUnit {

    // #7=IFCCONVERSIONBASEDUNIT(*, .LENGTHUNIT., INCH, #8);
    // #8=IFCMEASUREWITHUNIT(IFCLENGTHMEASURE(25.4), #9);
    // #9=IFCSIUNIT (*, .LENGTHUNIT., .MILLI., .METRE.);

    private IfcUnitEnum unitType; // ex.:PRESSUREUNIT
    private String conversionUnit;// ex.: BAR
    private IfcMeasureWithUnit measureWithUnit;

    // IFCPRESSUREMEASURE, IFCCONVERSIONBASEDUNIT(BAR, PASCAL, 100000)
    public IfcConversionBasedUnit(int lineIndex, ConversionBasedUnitInBoolFile unitInFile) throws InvalidUnitException {
	super(lineIndex);

	initAttributes(unitInFile);

	createIfcMeasureWithUnitAndAddToIfcBlock(unitInFile);

    }

    private void createIfcMeasureWithUnitAndAddToIfcBlock(ConversionBasedUnitInBoolFile unitInFile)
	    throws InvalidUnitException {
	measureWithUnit = new IfcMeasureWithUnit(getNextLineIndex(), unitInFile);
	measureWithUnit.joinIfcBlock(this);
    }

    private void initAttributes(ConversionBasedUnitInBoolFile unitInFile) throws InvalidUnitException {
	// BAR
	conversionUnit = unitInFile.getConversionUnit();

	// PRESSUREUNIT mapped from IFCPRESSUREMEASURE
	unitType = getUnitType(unitInFile);
    }

    // get PRESSUREUNIT from
    // IFCPRESSUREMEASURE(PASCAL)
    // extracted from IFCPRESSUREMEASURE(IFCCONVERSIONBASEDUNIT(BAR, PASCAL,
    // 100000))
    public static IfcUnitEnum getUnitType(ConversionBasedUnitInBoolFile unitInFile) throws InvalidUnitException {
	return SiUnitHelper.getSiUnitType(unitInFile.getMeasureType(), unitInFile.getBaseUnit());

    }

    public IfcUnitEnum getUnitType() {
	return unitType;
    }

    public String getConversionUnit() {
	return conversionUnit;
    }

    @Override
    public String getTypeAsString() {
	return unitType.toString();
    }

    @Override
    public String getCompleteName() {
	return conversionUnit;
    }

    @Override
    public void initLineComponents() {

	// #7=IFCCONVERSIONBASEDUNIT(*, .LENGTHUNIT., INCH, #8);

	lineComponents.add(ASTERISK);
	lineComponents.add(PERIOD + unitType + PERIOD);

	lineComponents.add(conversionUnit);
	lineComponents.add(measureWithUnit.getReference());

    }

    @Override
    public String getIfcClassName() {
	return "IFCCONVERSIONBASEDUNIT";
    }

    @Override
    public boolean equals(Object anotherUnit) {
	if (notIfcConversionBasedUnit(anotherUnit))
	    return false;
	if (differentType((IfcConversionBasedUnit) anotherUnit))
	    return false;
	if (differentConversionUnit((IfcConversionBasedUnit) anotherUnit))
	    return false;
	if (differentMeasureWithUnit((IfcConversionBasedUnit) anotherUnit))
	    return false;
	return true;
    }

    private boolean differentMeasureWithUnit(IfcConversionBasedUnit anotherUnit) {
	return !this.getMeasureWithUnit().equals(anotherUnit.getMeasureWithUnit());
    }

    private boolean differentConversionUnit(IfcConversionBasedUnit anotherUnit) {
	return !this.getConversionUnit().toString().equals(anotherUnit.getConversionUnit().toString());
    }

    private boolean differentType(IfcConversionBasedUnit anotherUnit) {
	return !anotherUnit.getTypeAsString().equals(this.getTypeAsString());
    }

    private boolean notIfcConversionBasedUnit(Object anotherUnit) {
	return !(anotherUnit instanceof IfcConversionBasedUnit);
    }

    public IfcMeasureWithUnit getMeasureWithUnit() {
	return measureWithUnit;
    }

}
