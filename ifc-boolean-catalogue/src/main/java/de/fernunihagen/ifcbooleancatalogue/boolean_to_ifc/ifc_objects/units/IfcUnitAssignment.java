package de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_objects.units;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.helpers.IfcLineFormatter;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.helpers.UnitHelper;
import de.fernunihagen.ifcbooleancatalogue.boolean_to_ifc.ifc_file_sections.IfcLineBlock;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.exceptions.InvalidUnitException;
import de.fernunihagen.ifcbooleancatalogue.shared.boolean_file.units.UnitInBoolFile;

public class IfcUnitAssignment extends IfcLineBlock {
    // #5 = IFCUNITASSIGNMENT((#6, #7));
    private List<IfcUnit> globalUnits = new ArrayList<IfcUnit>();

    public IfcUnitAssignment(int lineIndex, List<UnitInBoolFile> globalUnitsInFile) throws InvalidUnitException {
	super(lineIndex);
	createGlobalUnitsAndAddToIfcBlock(globalUnitsInFile);
    }

    private void createGlobalUnitsAndAddToIfcBlock(List<UnitInBoolFile> globalUnitsInFile) throws InvalidUnitException {
	for (UnitInBoolFile unitInFile : globalUnitsInFile)
	    createGlobalUnitAndAddToIfcBlock(unitInFile);
    }

    private void createGlobalUnitAndAddToIfcBlock(UnitInBoolFile unitInFile) throws InvalidUnitException {
	IfcUnit unit = UnitHelper.createUnit(getNextLineIndex(), unitInFile);
	globalUnits.add(unit);
	unit.joinIfcBlock(this);
    }

    @Override
    public void initLineComponents() {

	lineComponents.addAll(Arrays.asList(IfcLineFormatter.getListOfReferencesFromObjects(globalUnits)));
    }

    @Override
    public String getIfcClassName() {
	return "IFCUNITASSIGNMENT";
    }

    public List<IfcUnit> getGlobalUnits() {
	return globalUnits;
    }

}
